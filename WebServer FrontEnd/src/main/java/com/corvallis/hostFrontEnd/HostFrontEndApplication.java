package com.corvallis.hostFrontEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HostFrontEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(HostFrontEndApplication.class, args);
	}

}
