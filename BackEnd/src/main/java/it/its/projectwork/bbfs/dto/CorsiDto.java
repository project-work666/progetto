package it.its.projectwork.bbfs.dto;
import it.its.projectwork.bbfs.dao.CorsiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CorsiDto {
	private Long corsi_id;
	private Long tipo;
	private Long tipologia;
	private String descrizione;
	private String edizione;
	private Long previsto;
	private Long erogato;
	private Long durata;
	private Long misura;
	private String note;
	private String luogo;
	private String ente;
	private String data_erogazione;
	private String data_chiusura;
	private String data_censimento;
	private Long interno;
	public void parseDto(CorsiDao d) {
		if(d.getCorsoId() != null)
			this.corsi_id= d.getCorsoId();
		if(d.getTipo() != null)
			this.tipo= d.getTipo();
		if(d.getTipologia() != null)
			this.tipologia= d.getTipologia();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();
		if(d.getEdizione() != null)
			this.edizione= d.getEdizione();
		if(d.getPrevisto() != null)
			this.previsto= d.getPrevisto();
		if(d.getErogato() != null)
			this.erogato= d.getErogato();
		if(d.getDurata() != null)
			this.durata= d.getDurata();
		if(d.getMisura() != null)
			this.misura= d.getMisura();
		if(d.getNote() != null)
			this.note= d.getNote();
		if(d.getLuogo() != null)
			this.luogo= d.getLuogo();
		if(d.getEnte() != null)
			this.ente= d.getEnte();
		if(d.getData_erogazione() != null)
			this.data_erogazione= d.getData_erogazione();
		if(d.getData_chiusura() != null)
			this.data_chiusura= d.getData_chiusura();
		if(d.getData_censimento() != null)
			this.data_censimento= d.getData_censimento();
		if(d.getInterno() != null)
			this.interno= d.getInterno();
		
	}
}
