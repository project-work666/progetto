package it.its.projectwork.bbfs.service;

import java.util.List;

import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;
import it.its.projectwork.bbfs.dao.ValutazioniUtentiDao;

public interface ValutazioneUtentiService {
	public List<ValutazioniUtentiDao> selTutti();
	public void insert(ValutazioniUtentiDao d);
	public void delete(Long id) throws Exception;
	public void edit(ValutazioniUtentiDao d) throws Exception;
	public Double media(Long d, Long c);
	public List<ValutazioniUtentiDao> trovaByDipAndCorso(Long d, Long c);
}
