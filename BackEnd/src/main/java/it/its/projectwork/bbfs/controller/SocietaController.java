package it.its.projectwork.bbfs.controller;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.its.projectwork.bbfs.dao.SocietaDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.service.SocietaService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/societa")

public class SocietaController {
	@Autowired
	private SocietaService societaService;
	private static final Logger LOGGER = LoggerFactory.getLogger(DipendentiController.class);
	@GetMapping(value = "/id/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il soc con Id: " + id + "*******");

		Optional<SocietaDao> doc = societaService.selById(id);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_societa");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;


	}
}
