package it.its.projectwork.bbfs.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.ValutazioneDocentiDto;
import it.its.projectwork.bbfs.service.ValutazioneDocentiService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/valutazioni/docenti")
public class ValutazioneDocenteController {
	@Autowired
	private ValutazioneDocentiService service;

	private static final Logger LOGGER = LoggerFactory.getLogger(ValutazioneDocenteController.class);

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<ValutazioneDocentiDto>> fetchAll() {

		BaseResponseDto<List<ValutazioneDocentiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<ValutazioniDocentiDao> docentiDao = service.selTutti();
		ArrayList<ValutazioneDocentiDto> docenti =  new ArrayList<ValutazioneDocentiDto>();
		for(ValutazioniDocentiDao d: docentiDao) {
			ValutazioneDocentiDto doc = new ValutazioneDocentiDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;
	}
	
	@GetMapping(value = "/trova/{d}_{c}")
	public BaseResponseDto<?> trovaByDocAndCor(@PathVariable("d") Long d, @PathVariable("c") Long c) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		
		List<ValutazioniDocentiDao> listaDao = service.trovaByDocAndCorso(d, c);
		ArrayList<ValutazioneDocentiDto> lista =  new ArrayList<ValutazioneDocentiDto>();
		for(ValutazioniDocentiDao b: listaDao) {
			ValutazioneDocentiDto doc = new ValutazioneDocentiDto();
			doc.parseDto(b);
			lista.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (lista.isEmpty()) {
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setResponse(null);
			return response;
		}

		response.setResponse(lista);
		return response;
	}
	

	@GetMapping(value = "/media/{d}_{c}")
	public BaseResponseDto<?> media(@PathVariable("d") Long d, @PathVariable("c") Long c) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		
		Double out = service.media(d, c);

		if (out > 0) {
			String s = out.toString();
			boolean b = true;
			for (int i = 0; i < s.length(); i++) {
				if(s.charAt(i) == '.' && s.charAt((i + 1)) == '0' )
					b = false;
			}
			DecimalFormat df = new DecimalFormat("#.00");
			if (b)
				response.setResponse(df.format(out));
			else
				response.setResponse(out);
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			return response;
		}
		
		response.setStatus(HttpStatus.NOT_FOUND.value());
		response.setMessage("NON_TROVO_IL_DOCENTE");
		response.setResponse(null);
		return response;
	}
	
	@DeleteMapping(value = "/elimina/{id}")
	public BaseResponseDto<?> deleteDoc(@PathVariable("id") Long id) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("Eliminiamo il docente con id " + id);
		try {
			service.delete(id);
			response.setResponse("{}");
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		} catch (Exception e) {
			response.setResponse(null);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.setMessage("NON_TROVO_IL_DOCENTE");
			
		}
		response.setTimestamp(new Date());
		return response;
	}
	
	@PostMapping(produces = "application/json")
	public BaseResponseDto<?> createDoc(@RequestBody ValutazioniDocentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			service.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
	
	
	@PutMapping(produces = "application/json")
	public BaseResponseDto<?> edit(@RequestBody ValutazioniDocentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			service.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
}
