package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.CorsiUtentiDao;
import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;
import it.its.projectwork.bbfs.dao.ValutazioniUtentiDao;
import it.its.projectwork.bbfs.repository.ValutazioneDocentiRepository;
import it.its.projectwork.bbfs.repository.ValutazioneUtentiRepository;

@Service
@Transactional
public class ValutazioneUtentiServiceImpl implements ValutazioneUtentiService {
	@Autowired
	ValutazioneUtentiRepository rep;

	@Override
	public List<ValutazioniUtentiDao> selTutti() {
		return rep.findAll();
	}

	@Override
	public Double media(Long d, Long c) {
		List<ValutazioniUtentiDao> t = rep.findByDipendenteAndCorso(d, c);
		double l = (double) t.size();
		double s = (double) 0;
		for (ValutazioniUtentiDao valutazioniUtentiDao : t) {
			s = s + valutazioniUtentiDao.getValore();
		}
		return s / l;
	}
	
	

	@Override
	public void delete(Long id) throws Exception {
		Optional<ValutazioniUtentiDao> d = rep.findById(id);
		if (d.isPresent()) {
			rep.deleteById(id);
		} else {
			throw new Exception();
		}
	}
	
	@Override
	public List<ValutazioniUtentiDao> trovaByDipAndCorso(Long d, Long c) {
		return rep.findByDipendenteAndCorso(d, c);
	}

	
	
	@Override
	public void edit(ValutazioniUtentiDao dip) throws Exception {
		Optional<ValutazioniUtentiDao> d = rep.findById(dip.getValutazioni_utenti_id());
		if(d.isPresent()) {
			rep.save(dip);
		}else {
			throw new Exception();
		}
	}

	@Override
	public void insert(ValutazioniUtentiDao d) {
		rep.saveAndFlush(d);
	}
}
