package it.its.projectwork.bbfs.dto;


import it.its.projectwork.bbfs.dao.ValutazioniUtentiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ValutazioniUtentiDto {
	private Long valutazioni_utenti_id;
	private String criterio;
	private Long valore;
	private Long corso;
	private Long dipendente;
	public void parseDto(ValutazioniUtentiDao d) {
		if(d.getValutazioni_utenti_id() != null)
			this.valutazioni_utenti_id= d.getValutazioni_utenti_id();
		if(d.getCriterio() != null)
			this.criterio= d.getCriterio();
		if(d.getValore() != null)
			this.valore= d.getValore();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
		if(d.getDipendente() != null)
			this.dipendente= d.getDipendente();
	
	}
}
