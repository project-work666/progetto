package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.TipologieDao;
import it.its.projectwork.bbfs.repository.TipologieRepository;

@Service
@Transactional

public class TipologieServiceImpl implements TipologieService{
	@Autowired
	TipologieRepository tipologieRepository;
	@Override
	public Optional<TipologieDao> selById(Long id) {
		return tipologieRepository.findById(id);
	}
	@Override
	public List<TipologieDao> selTutti() {
		return tipologieRepository.findAll();
	}
}
