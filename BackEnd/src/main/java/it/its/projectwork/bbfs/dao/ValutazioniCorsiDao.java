package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import it.its.projectwork.bbfs.dto.ValutazioneCorsiDto;
import lombok.Data;

@Entity
@Table(name = "valutazioni_corsi")
@Data
public class ValutazioniCorsiDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="valutazione_corsi_id", unique=true, nullable=false) 
	private Long valutazione_corsi_id;
	@Column(name="criterio") 
	private String criterio;
	@Column(name="valore") 
	private Double valore;
	@Column(name="corso") 
	private Long corso;
	public void parseDto(ValutazioneCorsiDto d) {
		if(d.getValutazione_corso_id() != null)
			this.valutazione_corsi_id= d.getValutazione_corso_id();
		if(d.getCriterio() != null)
			this.criterio= d.getCriterio();
		if(d.getValore() != null)
			this.valore= d.getValore();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
	
	}
	
}
