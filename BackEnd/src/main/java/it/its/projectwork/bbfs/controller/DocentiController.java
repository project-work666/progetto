package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.DipendentiDto;
import it.its.projectwork.bbfs.dto.DocentiDto;
import it.its.projectwork.bbfs.service.DocentiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/docenti")

public class DocentiController {

	@Autowired
	private DocentiService docentiService;

	private static final Logger LOGGER = LoggerFactory.getLogger(DocentiController.class);

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<DocentiDto>> fetchAll() {

		BaseResponseDto<List<DocentiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DocentiDao> docentiDao = docentiService.selTutti();
		ArrayList<DocentiDto> docenti =  new ArrayList<DocentiDto>();
		for(DocentiDao d: docentiDao) {
			DocentiDto doc = new DocentiDto();
			doc.parseDao(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}
	
	@GetMapping(value="/interni/{page}_{quantita}",produces = "application/json")
	public BaseResponseDto<List<DipendentiDto>> fetchAllEsterni(@PathVariable("page") int page,
			@PathVariable("quantita") int quantita,@RequestParam(required = false)  String sortField,
			@RequestParam(required = false)  Integer sortOrder, @RequestParam(required = false)  String filters) {
		
		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		
		
		String sortF ="DipendenteId";
		Integer sortD = 1;
		if(sortField != null) {
			if(sortField.equals("ragione_sociale")) {
				sortF = "ragioneSociale";
				}
			else {
				if(sortField.equals("docente_id")) {
					sortF = "DocenteId";
				}else {
				sortF = sortField;
				}
			}
			sortD = sortOrder;
		}
		ArrayList<DipendentiDto> docenti =  new ArrayList<DipendentiDto>();	
		
		JSONParser parser = new JSONParser();
		System.err.println(filters);
		String matricola = "";
		String nome = "";
		String cognome = "";
		String dataNascita ="";
		String tipoDip ="";
		try {
			JSONObject json = (JSONObject) parser.parse(filters);
			
			JSONObject val = (JSONObject) json.get("matricola");
			if(val != null)
				matricola = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) json.get("nome");
			if(val != null)
				nome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) json.get("cognome");
			if(val != null)
				cognome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) json.get("data_nascita");
			if(val != null) {
				dataNascita = (String) val.get("value") == null ? "" : (String) val.get("value");
				String[] temp = dataNascita.split("/");
				if(temp.length > 0 ) {
					String tempStr = "";
					if(temp.length == 3) {
						if( temp[2].length() < 4)
							while(temp[2].length() < 4) {
								System.err.println(temp[2]);
								temp[2] += "%";
							}
					}
					for(int i = temp.length; i > 0; i--) {
						if(temp[i-1].length() ==  1) {
							temp[i-1] = "0"+temp[i-1];
						}
						tempStr += temp[i-1]+"-";
					}
					
					if(tempStr.charAt(tempStr.length()-1) == '-') tempStr = tempStr.substring(0, tempStr.length()-1);
					
					dataNascita = tempStr;
				}
			}
			val = (JSONObject) json.get("tipo_dipendente");
			if(val != null)
				tipoDip = (String) val.get("value") == null ? "" : (String) val.get("value");
		 } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch ( NullPointerException e){
			
		}
		finally {
			
			List<DipendentiDao> dipendentiDao;
			dipendentiDao = docentiService.selTuttiInterni(page, quantita, sortF, sortD, matricola, nome, cognome, dataNascita, tipoDip);
			for(DipendentiDao d: dipendentiDao) {
				DipendentiDto dip = new DipendentiDto();
				dip.parseDao(d);
				docenti.add(dip);
			}
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}
	
	@GetMapping(value = "/total", produces = "application/json")
	public BaseResponseDto<Integer> howMany() {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DocentiDao> docentiDao = docentiService.selTutti();
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docentiDao.size());
		return response;
	}
	
	
	@GetMapping(value = "/interni/total", produces = "application/json")
	public BaseResponseDto<Integer> howManyInterni() {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DipendentiDao> docentiDao = docentiService.selTuttiInterni();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docentiDao.size());
		return response;
	}
	
	@GetMapping(value = "/{page}_{quantita}",produces = "application/json")
	public BaseResponseDto<List<DocentiDto>> fetchPagina(@PathVariable("page") int page, @PathVariable("quantita") int quantita,@RequestParam(required = false)  String sortField, @RequestParam(required = false)  Integer sortOrder, @RequestParam(required = false)  String filters) {
		BaseResponseDto<List<DocentiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo paginato******");		
		ArrayList<DocentiDto> docenti =  new ArrayList<DocentiDto>();	
		String sortF ="DocenteId";
		Integer sortD = 1;
		if(sortField != null) {
			if(sortField.equals("ragione_sociale")) {
				sortF = "ragioneSociale";
				}
			else {
				if(sortField.equals("docente_id")) {
					sortF = "DocenteId";
				}else {
				sortF = sortField;
				}
			}
			sortD = sortOrder;
		}
		
		JSONParser parser = new JSONParser();
		System.err.println(filters);
		String ragione = "";
		String telefono = "";
		String titolo = "";
		String referente ="";
		try {
			JSONObject json = (JSONObject) parser.parse(filters);
			
			JSONObject val = (JSONObject) json.get("ragione_sociale");
			if(val != null)
				ragione = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) json.get("telefono");
			if(val != null)
				telefono = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) json.get("titolo");
			if(val != null)
				titolo = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) json.get("referente");
			if(val != null)
				referente = (String) val.get("value") == null ? "" : (String) val.get("value");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch ( NullPointerException e){
			
		}
		finally {
			List<DocentiDao> docentiDao;
			System.err.println(ragione + telefono);
			docentiDao = docentiService.selAllOrdinato(page, quantita, sortF, sortD, ragione, telefono, titolo, referente);
			for(DocentiDao d: docentiDao) {
				DocentiDto doc = new DocentiDto();
				doc.parseDao(d);
				docenti.add(doc);
			}
		}
		
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}

	@GetMapping(value = "/id/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il docente con Id: " + id + "*******");

		Optional<DocentiDao> doc = docentiService.selById(id);
		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;
	}

	@DeleteMapping(value = "/elimina/{id}")
	public BaseResponseDto<?> deleteDoc(@PathVariable("id") Long id) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("Eliminiamo il docente con id " + id);
		try {
			docentiService.delete(id);
			response.setResponse("{}");
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		} catch (Exception e) {
			response.setResponse(null);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.setMessage("NON_TROVO_IL_DOCENTE");
			
		}
		response.setTimestamp(new Date());
		return response;
	}

	@PutMapping(produces = "application/json")
	public BaseResponseDto<?> updateDoc(@RequestBody DocentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("***** Modifichiamo il doc con id " + doc.getIdDocente() + " *****");
			try {

				docentiService.edit(doc);
				response.setResponse(doc);
				response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.OK.value());
			} catch (Exception e) {
				System.err.println(e);
				response.setTimestamp(new Date());
				response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				
			}
			response.setTimestamp(new Date());
			return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<?> createDoc(@RequestBody DocentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			docentiService.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
	
	

}
