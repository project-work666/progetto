package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.ValutazioniUtentiDto;
import lombok.Data;

@Entity
@Table(name = "valutazioni_utenti")
@Data
public class ValutazioniUtentiDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="valutazioni_utenti_id", unique=true, nullable=false) 
	private Long valutazioni_utenti_id;
	@Column(name="criterio") 
	private String criterio;
	@Column(name="valore") 
	private Long valore;
	@Column(name="corso") 
	private Long corso;
	@Column(name="dipendente") 
	private Long dipendente;
	public void parseDto(ValutazioniUtentiDto d) {
		if(d.getValutazioni_utenti_id() != null)
			this.valutazioni_utenti_id= d.getValutazioni_utenti_id();
		if(d.getCriterio() != null)
			this.criterio= d.getCriterio();
		if(d.getValore() != null)
			this.valore= d.getValore();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
		if(d.getDipendente() != null)
			this.dipendente= d.getDipendente();
	
	}
}
