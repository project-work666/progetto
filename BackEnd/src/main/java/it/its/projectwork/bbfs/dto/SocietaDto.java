package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.SocietaDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class SocietaDto {
	private Long societa_id;
	private String ragione_sociale;
	private String indirizzo;
	private String localita;
	private String provincia;
	private String nazione;
	private String telefono;
	private String fax;
	private String partita_iva;
	public void parseDao(SocietaDao d) {
		if(d.getSocieta_id() != null)
			this.societa_id= d.getSocieta_id();
		if(d.getRagione_sociale() != null)
			this.ragione_sociale= d.getRagione_sociale();
		if(d.getIndirizzo() != null)
			this.indirizzo= d.getIndirizzo();
		if(d.getLocalita() != null)
			this.localita= d.getLocalita();
		if(d.getProvincia() != null)
			this.provincia= d.getProvincia();
		if(d.getNazione() != null)
			this.nazione= d.getNazione();
		if(d.getTelefono() != null)
			this.telefono= d.getTelefono();	
		if(d.getFax() != null)
			this.fax= d.getFax();
		if(d.getPartita_iva() != null)
			this.partita_iva= d.getPartita_iva();
	}
}
