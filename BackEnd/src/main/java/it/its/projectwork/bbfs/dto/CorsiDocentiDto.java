package it.its.projectwork.bbfs.dto;


import it.its.projectwork.bbfs.dao.CorsiDocentiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CorsiDocentiDto {
	private Long corsi_docenti_id;	
	private Long corso;
	private Long docente;
	private Long interno;
	public void parseDto(CorsiDocentiDao d) {
		if(d.getCorsi_docenti_id() != null)
			this.corsi_docenti_id= d.getCorsi_docenti_id();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
		if(d.getDocente() != null)
			this.docente= d.getDocente();
		if(d.getInterno() != null)
			this.interno= d.getInterno();
		
	}
}
