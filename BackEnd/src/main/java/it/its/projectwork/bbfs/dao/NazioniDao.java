package it.its.projectwork.bbfs.dao;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import it.its.projectwork.bbfs.dto.NazioniDto;
import lombok.Data;

@Entity
@Table(name = "nazioni")
@Data
public class NazioniDao {
	@Id
	@Column(name = "iso")
	private String iso;
	
	@Basic(optional = false)
	@Column(name = "description")
	private String description;
	
	public void parseDto(NazioniDto d) {
		if(d.getIdNazione() != null)
			this.iso= d.getIdNazione();
		if(d.getName() != null)
			this.description= d.getName();

	}

}