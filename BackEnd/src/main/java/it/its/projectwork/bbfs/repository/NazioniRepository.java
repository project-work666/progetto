package it.its.projectwork.bbfs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.projectwork.bbfs.dao.NazioniDao;

@Repository
public interface NazioniRepository extends JpaRepository<NazioniDao, String> {}