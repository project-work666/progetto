package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.its.projectwork.bbfs.dao.TipiDao;
import it.its.projectwork.bbfs.repository.TipiRepository;

@Service
@Transactional

public class TipiServiceImpl implements TipiService{
	@Autowired
	TipiRepository tipoRepository;
	@Override
	public Optional<TipiDao> selById(Long id) {
		return tipoRepository.findById(id);
	}
	@Override
	public List<TipiDao> selTutti() {
		return tipoRepository.findAll();
	}

}
