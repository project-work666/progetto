package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import it.its.projectwork.bbfs.dao.DipendentiDao;


public interface DipendentiRepository extends PagingAndSortingRepository<DipendentiDao, Long>{
	List<DipendentiDao> findAllByOrderByCognome();
	Page<DipendentiDao> findAllByOrderByCognome(Pageable page);
	
	@Query("SELECT dip FROM DipendentiDao dip  where dip.matricola like ?1 and dip.nome like ?2 and dip.cognome like ?3 and dip.dataNascita like ?4 and dip.tipoDipendente like ?5")
	Page<DipendentiDao> findDipendenti(String matricola,String nome,String cognome, String dataNascita,String tipoDip, Pageable page);
}
