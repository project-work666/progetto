package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.DocentiDto;
import lombok.Data;

@Entity
@Table(name = "docenti")
@Data
public class DocentiDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="docente_id", unique=true, nullable=false) 
	private Long docenteId;

	@Column(name = "titolo")
	private String titolo;

	@Column(name = "ragione_sociale")
	private String ragioneSociale;

	@Column(name = "indirizzo")
	private String indirizzo;

	@Column(name = "localita")
	private String localita;

	@Column(name = "provincia")
	private String provincia;

	@Column(name = "nazione")
	private String nazione;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "fax")
	private String fax;

	@Column(name = "partita_iva")
	private String partita_iva;

	@Column(name = "referente")
	private String referente;

	public Long getIdDocente() {
		return docenteId;
	}
	
	public void parseDto(DocentiDto d) {
		if(d.getDocente_id() != null)
			this.docenteId= d.getDocente_id();
		if(d.getTitolo() != null)
			this.titolo = d.getTitolo();
		if(d.getRagione_sociale() != null)
			this.ragioneSociale = d.getRagione_sociale();
		if(d.getIndirizzo() != null)
			this.indirizzo = d.getIndirizzo();
		if(d.getLocalita() != null)
			this.localita = d.getLocalita();
		if(d.getNazione() != null)
			this.nazione = d.getNazione();
		if(d.getTelefono() != null)
			this.telefono = d.getTelefono();
		if(d.getReferente() != null)
			this.fax = d.getFax();
		if(d.getReferente() != null)
			this.partita_iva = d.getPartita_iva();
		if(d.getReferente() != null)
			this.referente = d.getReferente();
	}
}
