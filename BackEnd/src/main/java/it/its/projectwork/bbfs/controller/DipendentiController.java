package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.DipendentiDto;
import it.its.projectwork.bbfs.service.DipendentiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/dipendenti")
public class DipendentiController {
	@Autowired
	private DipendentiService dipendentiService;
	private static final Logger LOGGER = LoggerFactory.getLogger(DipendentiController.class);
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<DipendentiDto>> fetchAll() {

		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DipendentiDao> dipendentiDao = dipendentiService.selTutti();
		ArrayList<DipendentiDto> dipendenti =  new ArrayList<DipendentiDto>();
		for(DipendentiDao d: dipendentiDao) {
			DipendentiDto doc = new DipendentiDto();
			doc.parseDao(d);
			dipendenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + dipendenti.size());

		response.setResponse(dipendenti);
		return response;

	}
	@GetMapping(value = "/id/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il dip con Id: " + id + "*******");

		Optional<DipendentiDao> doc = dipendentiService.selById(id);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;


	}
	
//	@GetMapping(value = "/{page}_{quantita}",produces = "application/json")
//	public BaseResponseDto<List<DipendentiDto>> fetchPagina(@PathVariable("page") int page, @PathVariable("quantita") int quantita) {
//
//		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
//		LOGGER.info("******Otteniamo tutto******");
//
//		List<DipendentiDao> dipendentiDao = dipendentiService.selTutti(page, quantita);
//		ArrayList<DipendentiDto> dipendenti =  new ArrayList<DipendentiDto>();
//		for(DipendentiDao d: dipendentiDao) {
//			DipendentiDto doc = new DipendentiDto();
//			doc.parseDao(d);
//			dipendenti.add(doc);
//		}
//		response.setTimestamp(new Date());
//		response.setStatus(HttpStatus.OK.value());
//		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
//
//		if (dipendenti.isEmpty()) {
//			response.setResponse(null);
//			return response;
//		}
//
//		LOGGER.info("Numero dei record: " + dipendenti.size());
//
//		response.setResponse(dipendenti);
//		return response;
//	}

	
	@GetMapping(value ="/getPaginato", produces ="application/json")
	public BaseResponseDto<List<DipendentiDto>> fetchPagina(@RequestParam(required = false)  String filters){

		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
		
		
		LOGGER.info("******Otteniamo tutto******");
		
		JSONParser parser = new JSONParser();
		System.err.println(filters);
		String matricola = "";
		String nome = "";
		String cognome = "";
		String dataNascita ="";
		String tipoDip ="";
		Long page = (long) 0;
		Long quant = (long) 10;
		String sortF ="dipendenteId";

		String sortField ="dipendenteId";
		Integer sortDir = 1;
		Integer sortD = 1;
		
		try {
			JSONObject json = (JSONObject) parser.parse(filters);
			
			JSONObject jsonFiltri = (JSONObject) json.get("filters");
			JSONObject val = (JSONObject) jsonFiltri.get("matricola");
			if(val != null)
				matricola = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("nome");
			if(val != null)
				nome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("cognome");
			if(val != null)
				cognome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("data_nascita");
			if(val != null)
				dataNascita = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("tipo_dipendente");
			if(val != null)
				tipoDip = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			quant = (Long)json.get("rows");
			page = (Long)json.get("first")/quant;
			
			sortField = (String) json.get("sortField");
			Long sd = (Long) json.get("sortOrder");
			sortDir = sd.intValue();
			
			if(sortField != null) {
				if(sortField.equals("data_nascita")) {
					sortF = "dataNascita";
				}
				else {
					if(sortField.equals("tipo_dipendente")) {
						sortF = "tipoDipendente";
					}else {
						sortF = sortField;
					}
				}
				sortD = sortDir;
			}

			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch ( NullPointerException e){
			
		}
		
		
		
		List<DipendentiDao> dipendentiDao = dipendentiService.selDipendenti(page.intValue(),quant.intValue(),sortF,sortD,matricola, nome, cognome,dataNascita,tipoDip);
		ArrayList<DipendentiDto> dipendenti =  new ArrayList<DipendentiDto>();
		for(DipendentiDao d: dipendentiDao) {
			DipendentiDto doc = new DipendentiDto();
			doc.parseDao(d);
			dipendenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + dipendenti.size());

		response.setResponse(dipendenti);
		return response;

	}
	
	
	
	@GetMapping(value = "/total", produces = "application/json")
	public BaseResponseDto<Integer> howMany() {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DipendentiDao> docentiDao = dipendentiService.selTutti();
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docentiDao.size());
		return response;
	}
	
	
	
	
}
