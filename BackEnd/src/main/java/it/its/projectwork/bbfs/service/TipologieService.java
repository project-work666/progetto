package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.TipologieDao;

public interface TipologieService {
	public Optional<TipologieDao> selById(Long id); 
	public List <TipologieDao> selTutti();
}
