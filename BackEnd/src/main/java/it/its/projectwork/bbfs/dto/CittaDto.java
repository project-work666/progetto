package it.its.projectwork.bbfs.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import it.its.projectwork.bbfs.dao.CittaDao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CittaDto {
	
    @Column(name = "id_city")
    private Long idCity;

    @Column(name = "description")
    private String name;

    @Column(name = "id_prov")
    private String idProv;

    @NotNull
    private ProvinceDto province;
    public void parseDto(CittaDao d) {	
		if(d.getIdCity() != null)
			this.idCity= d.getIdCity();
		if(d.getName() != null)
			this.name= d.getName();
		if(d.getIdProv() != null)
			this.idProv= d.getIdProv();
	}
    
}
