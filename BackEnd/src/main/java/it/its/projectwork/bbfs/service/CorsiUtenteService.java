package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.CorsiUtentiDao;
import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;

public interface CorsiUtenteService {
	public List <DipendentiDao> selCorsisti();
	public List<CorsiDao> selCorsi(Long c);
	public Optional<CorsiUtentiDao> findCorsoAlunno(Long c, Long a);
	public void insert(CorsiUtentiDao d);
	public void delete(Long d) throws Exception;
	public void edit(CorsiUtentiDao dip) throws Exception;
	public ArrayList<DipendentiDao> selCorsistiOrdinato(int pagina, int quantita, String sortBy, int dir, String matricola, String nome, String cognome, String dataNascita,String tipoDip );
	public List<DipendentiDao> selCorsisti(int pagina, int quantita, String sortBy, int dir, String matricola, String nome, String cognome, String dataNascita,String tipoDip, Long corso);
	public List<DipendentiDao> selCorsisti(Long corso);
	public List<DipendentiDao> selCorsistiDist();
}
