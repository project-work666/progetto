package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.its.projectwork.bbfs.dao.TipiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.CorsiDto;
import it.its.projectwork.bbfs.dto.TipiDto;
import it.its.projectwork.bbfs.service.TipiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/tipo")

public class TipiController {
	@Autowired
	private TipiService service;
	private static final Logger LOGGER = LoggerFactory.getLogger(TipiController.class);
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchAll() {

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<TipiDao> docentiDao = service.selTutti();
		ArrayList<TipiDto> docenti =  new ArrayList<TipiDto>();
		for(TipiDao d: docentiDao) {
			TipiDto doc = new TipiDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}
	@GetMapping(value = "/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il tip con Id: " + id + "*******");

		Optional<TipiDao> doc = service.selById(id);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_tip");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;


	}
}
