package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.ValutazioniCorsiDao;
import it.its.projectwork.bbfs.repository.ValutazioneCorsiRepository;
@Service
@Transactional
public class ValutazioneCorsiServiceImpl implements ValutazioneCorsiService{
	@Autowired
    ValutazioneCorsiRepository rep;
	@Override
	public List<ValutazioniCorsiDao> selTutti() {
		return rep.findAll();
	}
	
	@Override
	public void insert(ValutazioniCorsiDao d) {
		rep.saveAndFlush(d);
	}

	@Override
	public void delete(Long id) throws Exception{
		Optional<ValutazioniCorsiDao> d = rep.findById(id);
		if(d.isPresent()) {
			rep.deleteById(id);
		}else {
			throw new Exception();
		}
	}

	@Override
	public List<ValutazioniCorsiDao> selByCorso(Long id) {
		List<ValutazioniCorsiDao> res = rep.findByCorso(id);
		System.err.println("\n\n\n\n\n "+id+" VOTI: "+res.size());
		return res;
	}
	
	
}
