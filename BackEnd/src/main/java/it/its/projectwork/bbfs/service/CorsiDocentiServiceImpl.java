package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.CorsiDocentiDao;
import it.its.projectwork.bbfs.repository.CorsiDocentiRepository;

@Service
@Transactional
public class CorsiDocentiServiceImpl implements CorsiDocentiService{
	@Autowired
	CorsiDocentiRepository rep;
	@Override
	public List<CorsiDocentiDao> selTut() {
		return rep.findAll();
	}
	@Override
	public void insert(CorsiDocentiDao dip){
		rep.saveAndFlush(dip);
	}
	@Override
	public void delete(Long id) throws Exception {
		
		Optional<CorsiDocentiDao> d = rep.findById(id);
		if(d.isPresent()) {
			rep.deleteById(id);
		}else {
			throw new Exception();
		}
	}
	@Override
	public Optional<CorsiDocentiDao> trovaByCorso(Long c) {
		Optional<CorsiDocentiDao> d = rep.findByCorso(c);
		return d;
	}
	@Override
	public Long trovaByCorAndDoc(Long c, Long d) {
		Long e = rep.findByDocAndCor(c, d);
		return e;
	}
	@Override
	public void edit(CorsiDocentiDao d) throws Exception {
		Optional<CorsiDocentiDao> d1 = rep.findById(d.getCorsi_docenti_id());
		if(d1.isPresent()) {
			rep.save(d);
		}else {
			throw new Exception();
		}
	}
	@Override
	public List<CorsiDocentiDao> trovaDocente(Long d) {
		List<CorsiDocentiDao> out = rep.findByDocente(d);
		return out;
	}
	@Override
	public List<CorsiDocentiDao> trovaDocenteInterno(Long d) {
		List<CorsiDocentiDao> out = rep.findByDocenteInterno(d);
		return out;
	}
	@Override
	public List<CorsiDocentiDao> trovaDocenteEsterno(Long d) {
		List<CorsiDocentiDao> out = rep.findByDocenteEsterno(d);
		return out;
	}
}
