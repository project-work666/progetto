package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.DipendentiDao;


public interface DipendentiService {
	public List <DipendentiDao> selTutti();
	public List <DipendentiDao> selTutti(int pagina, int quantita);
	public Optional<DipendentiDao> selById(Long id);
	
	public List<DipendentiDao> selDipendenti(int pagina, int quantita, String sortBy, int dir, String matricola, String nome, String cognome, String dataNascita,String tipoDip);
	
}
