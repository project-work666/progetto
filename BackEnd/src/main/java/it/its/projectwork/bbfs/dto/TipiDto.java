package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.TipiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class TipiDto {
	private Long tipo_id;
	private String descrizione;
	public void parseDto(TipiDao d) {
		if(d.getTipoId() != null)
			this.tipo_id= d.getTipoId();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();

	}
}
