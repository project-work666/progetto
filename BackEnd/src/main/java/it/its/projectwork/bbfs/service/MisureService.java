package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.MisureDao;

public interface MisureService {
	public Optional<MisureDao> selById(Long id);
	public List <MisureDao> selTutti();
}
