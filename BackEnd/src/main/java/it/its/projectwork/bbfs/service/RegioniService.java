package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;

import it.its.projectwork.bbfs.dao.RegioniDao;
import it.its.projectwork.bbfs.dto.RegioniDto;

public interface RegioniService {
	
	public ArrayList<RegioniDto> selTutti();
	public RegioniDto selRegioneById(Long id);
	public List<RegioniDao> selByIdIso(String iso_coun);
}
