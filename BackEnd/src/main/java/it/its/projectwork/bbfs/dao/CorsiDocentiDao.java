package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.CorsiDocentiDto;
import lombok.Data;

@Entity
@Table(name = "corsi_docenti")
@Data
public class CorsiDocentiDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="corsi_docenti_id", unique=true, nullable=false) 
	private Long corsi_docenti_id;
	@Column(name = "corso")	
	private Long corso;
	@Column(name = "docente")	
	private Long docente;
	@Column(name = "interno")	
	private Long interno;
	public void parseDto(CorsiDocentiDto d) {
		if(d.getCorsi_docenti_id() != null)
			this.corsi_docenti_id= d.getCorsi_docenti_id();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
		if(d.getDocente() != null)
			this.docente= d.getDocente();
		if(d.getInterno() != null)
			this.interno= d.getInterno();
		
	}
}
