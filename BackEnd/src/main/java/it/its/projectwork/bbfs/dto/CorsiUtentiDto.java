package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.CorsiUtentiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CorsiUtentiDto {
	private Long corsi_utenti_id;
	private Long corso;
	private Long dipendente;
	private Long durata;
	public void parseDto(CorsiUtentiDao d) {
		if(d.getCorsi_utenti_id() != null)
			this.corsi_utenti_id= d.getCorsi_utenti_id();
		if(d.getCorso() != null)
			this.corso = d.getCorso();
		if(d.getDipendente() != null)
			this.dipendente = d.getDipendente();
		if(d.getDurata() != null)
			this.durata = d.getDurata();	
	}
	
}
