package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.CorsiUtentiDao;
import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;
import it.its.projectwork.bbfs.repository.CorsiRepository;
import it.its.projectwork.bbfs.repository.CorsiUtentiReository;
import it.its.projectwork.bbfs.repository.DipendentiRepository;

@Service
@Transactional

public class CorsiUtentiServiceImpl implements CorsiUtenteService{
	@Autowired
	CorsiUtentiReository corsiutentireository;
	@Autowired
	DipendentiRepository dipendentiRepository;
	@Autowired
	CorsiRepository corsiRepository;
	
	@Override
	public List<DipendentiDao> selCorsisti() {
		List<DipendentiDao> out=corsiutentireository.findAllCorsisti();
		return out;
	}
	
	@Override
	public List<DipendentiDao> selCorsistiDist() {
		List<DipendentiDao> out=corsiutentireository.findAllCorsistiDist();
		return out;
	}
	
	public ArrayList<DipendentiDao> selCorsistiOrdinato(int pagina, int quantita, String sortBy, int dir, String matricola, String nome, String cognome, String dataNascita,String tipoDip ) {
		Pageable p;
		if(sortBy.length() <= 0)
			sortBy = "dipendenteId";
		if(dir > 0)
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).ascending());
		else{
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).descending());
		}
		Page<DipendentiDao> resP = corsiutentireository.findCorsisti("%"+matricola+"%","%"+nome+"%","%"+cognome+"%","%"+dataNascita+"%","%"+tipoDip+"%",p);
		ArrayList<DipendentiDao> res = new ArrayList<DipendentiDao>();
		for(DipendentiDao d : resP) {
			res.add(d);
		}
		return res;
	}
	
	
	@Override
	public List<DipendentiDao> selCorsisti(int pagina, int quantita, String sortBy, int dir, String matricola, String nome, String cognome, String dataNascita,String tipoDip, Long corso) {
		Pageable p;
		if(sortBy.length() <= 0)
			sortBy = "dipendenteId";
		if(dir > 0)
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).ascending());
		else{
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).descending());
		}
		Page<DipendentiDao> resP = corsiutentireository.findCorsisti("%"+matricola+"%","%"+nome+"%","%"+cognome+"%","%"+dataNascita+"%","%"+tipoDip+"%",corso,p);
		ArrayList<DipendentiDao> res = new ArrayList<DipendentiDao>();
		for(DipendentiDao d : resP) {
			res.add(d);
		}
		return res;
	}
	
	
	public List<DipendentiDao> selCorsisti(Long corso){
		List<DipendentiDao> out=corsiutentireository.findCorsisti(corso);
		return out;
	}
	@Override
	public List<CorsiDao> selCorsi(Long c) {
		List<CorsiDao> out=corsiRepository.findAll();
		List<CorsiUtentiDao> filtro=corsiutentireository.findAll();
		List<CorsiDao> finale=new ArrayList<CorsiDao>();
		for (CorsiDao i : out) {
			Boolean t=false;
	        for (CorsiUtentiDao x : filtro) {
				if(x.getCorso()==i.getCorsoId() && x.getDipendente()==c) {
					t=true;
				}
			}
	        if(t)finale.add(i);
		}
		return finale;
	}

	@Override
	public void insert(CorsiUtentiDao d) {
		corsiutentireository.saveAndFlush(d);
	}

	@Override
	public void delete(Long id) throws Exception {
		Optional<CorsiUtentiDao> d = corsiutentireository.findById(id);
		if(d.isPresent()) {
			corsiutentireository.deleteById(id);
		}else {
			throw new Exception();
		}
	}
	
	@Override
	public void edit(CorsiUtentiDao dip) throws Exception {
		Optional<CorsiUtentiDao> d = corsiutentireository.findById(dip.getCorsi_utenti_id());
		if(d.isPresent()) {
			corsiutentireository.save(dip);
		}else {
			throw new Exception();
		}
	}

	@Override
	public Optional<CorsiUtentiDao> findCorsoAlunno(Long c, Long a) {
		return corsiutentireository.findByCorDip(c, a);
	}
	
}
