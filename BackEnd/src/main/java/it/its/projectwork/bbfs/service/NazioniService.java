package it.its.projectwork.bbfs.service;

import java.util.ArrayList;

import it.its.projectwork.bbfs.dto.NazioniDto;

public interface NazioniService {
	
	public ArrayList<NazioniDto> selTutti();
	public NazioniDto selNazioneById(String id);
}