package it.its.projectwork.bbfs.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.ValutazioniCorsiDao;
import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;
import it.its.projectwork.bbfs.dao.ValutazioniUtentiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.CorsiDto;
import it.its.projectwork.bbfs.dto.ValutazioneCorsiDto;
import it.its.projectwork.bbfs.dto.ValutazioneDocentiDto;
import it.its.projectwork.bbfs.dto.ValutazioniUtentiDto;
import it.its.projectwork.bbfs.service.ValutazioneCorsiService;
import it.its.projectwork.bbfs.service.ValutazioneUtentiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/valutazioni/alunni")

public class ValutazioneUtenteController {
	
	@Autowired
	private ValutazioneUtentiService service;
	private static final Logger LOGGER = LoggerFactory.getLogger(ValutazioneUtenteController.class);
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<?> fetchAll() {

		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<ValutazioniUtentiDao> valUtentiDao = service.selTutti();
		ArrayList<ValutazioniUtentiDto> valutazioni =  new ArrayList<ValutazioniUtentiDto>();
		for(ValutazioniUtentiDao d: valUtentiDao) {
			ValutazioniUtentiDto doc = new ValutazioniUtentiDto();
			doc.parseDto(d);
			valutazioni.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (valutazioni.isEmpty()) {
			response.setResponse(null);
			return response;
		}
		LOGGER.info("Numero dei record: " + valutazioni.size());

		response.setResponse(valutazioni);
		return response;
	}
	
	@GetMapping(value = "/trova/{d}_{c}")
	public BaseResponseDto<?> trovaByDipAndCor(@PathVariable("d") Long d, @PathVariable("c") Long c) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		
		List<ValutazioniUtentiDao> listaDao = service.trovaByDipAndCorso(d, c);
		ArrayList<ValutazioniUtentiDto> lista =  new ArrayList<ValutazioniUtentiDto>();
		for(ValutazioniUtentiDao b: listaDao) {
			ValutazioniUtentiDto doc = new ValutazioniUtentiDto();
			doc.parseDto(b);
			lista.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (lista.isEmpty()) {
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setResponse(null);
			return response;
		}

		response.setResponse(lista);
		return response;
	}
	
	
	@GetMapping(value = "/media/{d}_{c}")
	public BaseResponseDto<?> media(@PathVariable("d") Long d, @PathVariable("c") Long c) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		
		Double out = service.media(d, c);

		if (out > 0) {
			String s = out.toString();
			boolean b = true;
			for (int i = 0; i < s.length(); i++) {
				if(s.charAt(i) == '.' && s.charAt((i + 1)) == '0' )
					b = false;
			}
			DecimalFormat df = new DecimalFormat("#.00");
			if (b)
				response.setResponse(df.format(out));
			else
				response.setResponse(out);
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			return response;
		}
		
		response.setStatus(HttpStatus.NOT_FOUND.value());
		response.setMessage("NON_TROVO_IL_DIPENDENTE");
		response.setResponse(null);
		return response;
	}
	
	@PostMapping(produces = "application/json")
	public BaseResponseDto<?> createValUtente(@RequestBody ValutazioniUtentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			service.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
	
	@PutMapping(produces = "application/json")
	public BaseResponseDto<?> updateVal(@RequestBody ValutazioniUtentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
			try {
				service.edit(doc);
				response.setResponse(doc);
				response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.OK.value());
			} catch (Exception e) {
				response.setTimestamp(new Date());
				response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				
			}
			response.setTimestamp(new Date());
			return response;
	}
}
