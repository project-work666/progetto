package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import it.its.projectwork.bbfs.dto.CorsiDto;
import lombok.Data;

@Entity
@Table(name = "corsi")
@Data
public class CorsiDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="corso_id", unique=true, nullable=false) 
	private Long corsoId;
	@Column(name="tipo") 
	private Long tipo;
	@Column(name="tipologia") 
	private Long tipologia;
	@Column(name="descrizione")
	private String descrizione;
	@Column(name="edizione")
	private String edizione;
	@Column(name="previsto") 
	private Long previsto;
	@Column(name="erogato") 
	private Long erogato;
	@Column(name="durata") 
	private Long durata;
	@Column(name="misura") 
	private Long misura;
	@Column(name="note") 
	private String note;
	@Column(name="luogo") 
	private String luogo;
	@Column(name="ente") 
	private String ente;
	@Column(name="data_erogazione") 
	private String data_erogazione;
	@Column(name="data_chiusura") 
	private String data_chiusura;
	@Column(name="data_censimento") 
	private String data_censimento;
	@Column(name="interno") 
	private Long interno;
	public Long getIdCorso() {
		return corsoId;
	}
	public void parseDto(CorsiDto d) {
		if(d.getCorsi_id() != null)
			this.corsoId= d.getCorsi_id();
		if(d.getTipo() != null)
			this.tipo= d.getTipo();
		if(d.getTipologia() != null)
			this.tipologia= d.getTipologia();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();
		if(d.getEdizione() != null)
			this.edizione= d.getEdizione();
		if(d.getPrevisto() != null)
			this.previsto= d.getPrevisto();
		if(d.getErogato() != null)
			this.erogato= d.getErogato();
		if(d.getDurata() != null)
			this.durata= d.getDurata();
		if(d.getMisura() != null)
			this.misura= d.getMisura();
		if(d.getNote() != null)
			this.note= d.getNote();
		if(d.getLuogo() != null)
			this.luogo= d.getLuogo();
		if(d.getEnte() != null)
			this.ente= d.getEnte();
		if(d.getData_erogazione() != null)
			this.data_erogazione= d.getData_erogazione();
		if(d.getData_chiusura() != null)
			this.data_chiusura= d.getData_chiusura();
		if(d.getData_censimento() != null)
			this.data_censimento= d.getData_censimento();
		if(d.getInterno() != null)
			this.interno= d.getInterno();
		
	}
}
