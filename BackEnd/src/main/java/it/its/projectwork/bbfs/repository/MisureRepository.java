package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.MisureDao;

public interface MisureRepository extends PagingAndSortingRepository<MisureDao,Long>{
	List<MisureDao> findAll();
}
