package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.ProvinceDao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProvinceDto {
  private String idProvincia;
  private String description;
  private String idRegione;
	public void parseDto(ProvinceDao d) {
		if(d.getIdProvincia() != null)
			this.idProvincia= d.getIdProvincia();
		if(d.getDescription() != null)
			this.description= d.getDescription();
		if(d.getIdRegione() != null)
			this.idRegione= d.getIdRegione();

	}
}
