package it.its.projectwork.bbfs.exception;

public class UserAlreadyExistsException extends Exception {

	private static final long serialVersionUID = -3686429974280389453L;

	public UserAlreadyExistsException() {}
}