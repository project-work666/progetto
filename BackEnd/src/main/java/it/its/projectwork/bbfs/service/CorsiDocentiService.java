package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.CorsiDocentiDao;

public interface CorsiDocentiService {
	public List<CorsiDocentiDao> selTut();
	public void insert(CorsiDocentiDao dip);
	public void delete(Long d) throws Exception;
	public void edit(CorsiDocentiDao d) throws Exception;
	public Optional<CorsiDocentiDao> trovaByCorso(Long c);
	public Long trovaByCorAndDoc(Long c, Long d);
	
	public List<CorsiDocentiDao> trovaDocente(Long d);

	public List<CorsiDocentiDao> trovaDocenteEsterno(Long d);
	public List<CorsiDocentiDao> trovaDocenteInterno(Long d);
}
