package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.CorsiDocentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;
import it.its.projectwork.bbfs.dao.TipiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.CorsiDocentiDto;
import it.its.projectwork.bbfs.dto.CorsiDto;
import it.its.projectwork.bbfs.dto.TipiDto;
import it.its.projectwork.bbfs.service.CorsiDocentiService;
import it.its.projectwork.bbfs.service.CorsiService;
import it.its.projectwork.bbfs.service.TipiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/corsidocenti")
public class CorsiDocentiControlloer {
	@Autowired
	private CorsiDocentiService service;
	
	@Autowired
	private CorsiService corsiService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TipiController.class);
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDocentiDao>> fetchAll() {

		BaseResponseDto<List<CorsiDocentiDao>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<CorsiDocentiDao> docentiDao = service.selTut();
		ArrayList<CorsiDocentiDto> docenti =  new ArrayList<CorsiDocentiDto>();
		for(CorsiDocentiDao d: docentiDao) {
			CorsiDocentiDto doc = new CorsiDocentiDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}
	
	@GetMapping(value = "/id/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il corso docente con IdCorso: " + id + "*******");

		Optional<CorsiDocentiDao> doc = service.trovaByCorso(id);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_CORSODOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;
	}
	
	@GetMapping(value = "/trovaByDoc/{id}", produces = "application/json")
	public BaseResponseDto<?> findByDoc(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il corso docente con IdDocente: " + id + "*******");

		List<CorsiDocentiDao> doc = service.trovaDocente(id);
		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_CORSODOCENTE_CON_QUESTO_DOCENTE");
			return response;
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;
	}
	
	@GetMapping(value = "/trovaByDocEsterno/{id}", produces = "application/json")
	public BaseResponseDto<?> findByDocEsterno(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il corso docente con IdDocente: " + id + "*******");

		List<CorsiDocentiDao> doc = service.trovaDocenteEsterno(id);
		ArrayList<CorsiDao> cor = new ArrayList<CorsiDao>();
		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_CORSODOCENTE_CON_QUESTO_DOCENTE");
			return response;
		}else {
			for(CorsiDocentiDao cd : doc) {
				CorsiDao c = corsiService.selById(cd.getCorso()).get();
				cor.add(c);
			}
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(cor);
		return response;
	}
	
	@GetMapping(value = "/trovaByDocInterno/{id}", produces = "application/json")
	public BaseResponseDto<?> findByDocInterno(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo i corsi docente interno con IdDocente: " + id + "*******");

		List<CorsiDocentiDao> doc = service.trovaDocenteInterno(id);
		ArrayList<CorsiDao> cor = new ArrayList<CorsiDao>();
		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_CORSODOCENTE_CON_QUESTO_DOCENTE");
			return response;
		}else {
			for(CorsiDocentiDao cd : doc) {
				CorsiDao c = corsiService.selById(cd.getCorso()).get();
				cor.add(c);
			}
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(cor);
		return response;
	}

	
	@GetMapping(value = "/trova/{cor}/{doc}", produces = "application/json")
	public BaseResponseDto<?> deleteByDocCor(@PathVariable("cor") Long cor, @PathVariable("doc") Long doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		
		Long id = service.trovaByCorAndDoc(cor, doc);
		LOGGER.info("Trovato il corso docente con id " + id);
		
		if (id == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_CORSODOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(id);
		return response;
	}
	
	@DeleteMapping(value = "/elimina/{id}")
	public BaseResponseDto<?> deleteDoc(@PathVariable("id") Long id) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("Eliminiamo il docente con id " + id);
		try {
			service.delete(id);
			response.setResponse("{}");
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		} catch (Exception e) {
			response.setResponse(null);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.setMessage("NON_TROVO_IL_DOCENTE");
			
		}
		response.setTimestamp(new Date());
		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<?> createDoc(@RequestBody CorsiDocentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			service.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
	
	@PutMapping(produces = "application/json")
	public BaseResponseDto<?> updateDoc(@RequestBody CorsiDocentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
			try {
				service.edit(doc);
				response.setResponse(doc);
				response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.OK.value());
			} catch (Exception e) {
				response.setTimestamp(new Date());
				response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				
			}
			response.setTimestamp(new Date());
			return response;
	}
}
