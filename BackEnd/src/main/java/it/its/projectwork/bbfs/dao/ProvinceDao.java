package it.its.projectwork.bbfs.dao;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import it.its.projectwork.bbfs.dto.ProvinceDto;
import lombok.Data;

@Entity
@Table(name = "province")
@Data
public class ProvinceDao{
	@Id
	@Column(name = "id_prov")
	private String idProvincia;

	@Basic(optional = false)
	@Column(name = "description")
	private String description;
	
	@Column(name = "id_region")
	private String idRegione;
	
	public void parseDto(ProvinceDto d) {
		if(d.getIdProvincia() != null)
			this.idProvincia= d.getIdProvincia();
		if(d.getDescription() != null)
			this.description= d.getDescription();
		if(d.getIdRegione() != null)
			this.idRegione= d.getIdRegione();

	}

}