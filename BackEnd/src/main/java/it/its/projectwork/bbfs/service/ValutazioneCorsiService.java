package it.its.projectwork.bbfs.service;

import java.util.List;

import it.its.projectwork.bbfs.dao.ValutazioniCorsiDao;

public interface ValutazioneCorsiService {
	public List<ValutazioniCorsiDao> selTutti();
	public void insert(ValutazioniCorsiDao d);
	public void delete(Long id) throws Exception;
	public List<ValutazioniCorsiDao> selByCorso(Long id);
	
}
