package it.its.projectwork.bbfs.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.ProvinceDao;
import it.its.projectwork.bbfs.dao.RegioniDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.ProvinceDto;
import it.its.projectwork.bbfs.dto.RegioniDto;
import it.its.projectwork.bbfs.service.ProvinceService;
import it.its.projectwork.bbfs.service.RegioniService;


@RestController
@RequestMapping(value = "api/province")
public class ProvinceController {

	@Autowired
	ProvinceService provinceService;
	@Autowired
	RegioniService regioniService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<ProvinceDao>> allCity(){

		List<ProvinceDao> province = provinceService.selTutti();

		BaseResponseDto<List<ProvinceDao>> res = new BaseResponseDto<List<ProvinceDao>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		res.setResponse(province);

		return res;
	}

	@GetMapping(produces = "application/json", value = "/regione/{idRegione}")
	public BaseResponseDto<ArrayList<ProvinceDto>> findByIdRegione(@PathVariable("idRegione") String id){

    ArrayList<ProvinceDto> province = provinceService.selByIdRegione(id);
    
		BaseResponseDto<ArrayList<ProvinceDto>> res = new BaseResponseDto<ArrayList<ProvinceDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		res.setResponse(province);

		return res;
	}
	
	@GetMapping(produces = "application/json", value = "/nazione/{iso}")
	public BaseResponseDto<ArrayList<ProvinceDto>> findByIso(@PathVariable("iso") String iso){
		
		List<RegioniDao> regioni = regioniService.selByIdIso(iso);
		ArrayList<ProvinceDto> resultData = new ArrayList<ProvinceDto>();
		for(RegioniDao regione : regioni) {
			ArrayList<ProvinceDto> province = provinceService.selByIdRegione(regione.getIdRegion()+"");
			resultData.addAll(province);
		}
		
		BaseResponseDto<ArrayList<ProvinceDto>> res = new BaseResponseDto<ArrayList<ProvinceDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		res.setResponse(resultData);

		return res;
	}
	
	@GetMapping(produces = "application/json", value = "/id/{idProvincia}")
	public BaseResponseDto<ArrayList<ProvinceDto>> findById(@PathVariable("idProvincia") String id){

		ProvinceDto province = provinceService.selById(id);
    
		BaseResponseDto<ArrayList<ProvinceDto>> res = new BaseResponseDto<ArrayList<ProvinceDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		res.setResponse(province);

		return res;
	}
	
}
