package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;
import it.its.projectwork.bbfs.dao.TipiDao;

public interface TipiService {
	public Optional<TipiDao> selById(Long id);
	public List <TipiDao> selTutti();
}
