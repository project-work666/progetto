package it.its.projectwork.bbfs.dto;


import it.its.projectwork.bbfs.dao.MisureDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class MisureDto {
	private Long misura_id;
	private String descrizione;
	public void parseDto(MisureDao d) {
		if(d.getMisura_id() != null)
			this.misura_id= d.getMisura_id();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();

	}
}
