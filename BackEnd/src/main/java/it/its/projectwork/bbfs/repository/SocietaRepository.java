package it.its.projectwork.bbfs.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.SocietaDao;

public interface SocietaRepository extends PagingAndSortingRepository<SocietaDao, Long>{

}
