package it.its.projectwork.bbfs.service;

import java.util.Optional;

import it.its.projectwork.bbfs.dao.SocietaDao;

public interface SocietaService {
	public Optional<SocietaDao> selById(Long id);
}
