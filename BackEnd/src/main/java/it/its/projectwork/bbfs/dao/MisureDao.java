package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.MisureDto;
import lombok.Data;

@Entity
@Table(name = "misure")
@Data
public class MisureDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="misura_id", unique=true, nullable=false) 
	private Long misura_id;
	@Column(name="descrizione") 
	private String descrizione;
	public void parseDto(MisureDto d) {
		if(d.getMisura_id() != null)
			this.misura_id= d.getMisura_id();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();

	}
}
