package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.CorsiUtentiDto;
import lombok.Data;

@Entity
@Table(name = "corsi_utenti")
@Data
public class CorsiUtentiDao {	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="corsi_utenti_id", unique=true, nullable=false) 
	private Long corsi_utenti_id;
	@Column(name = "corso")	
	private Long corso;
	@Column(name = "dipendente")	
	private Long dipendente;
	@Column(name = "durata")	
	private Long durata;
	public Long getIdCorsiUtenti() {
		return corsi_utenti_id;
	}
	public void parseDto(CorsiUtentiDto d) {	
		if(d.getCorsi_utenti_id() != null)
			this.corsi_utenti_id= d.getCorsi_utenti_id();
		if(d.getCorso() != null)
			this.corso = d.getCorso();
		if(d.getDipendente() != null)
			this.dipendente = d.getDipendente();
		if(d.getDurata() != null)
			this.durata = d.getDurata();	
	}
}
