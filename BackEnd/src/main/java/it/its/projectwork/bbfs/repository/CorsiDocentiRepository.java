package it.its.projectwork.bbfs.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.CorsiDocentiDao;
import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;

public interface CorsiDocentiRepository  extends PagingAndSortingRepository<CorsiDocentiDao,Long>{
	
	void saveAndFlush(CorsiDocentiDao d);
	
	List<CorsiDocentiDao> findAll();
	
	@Query("SELECT cor.corsi_docenti_id FROM CorsiDocentiDao cor WHERE cor.corso = ?1 AND cor.docente = ?2")
	Long findByDocAndCor(Long corso, Long docente);
	
	Optional<CorsiDocentiDao> findByCorso(Long corso);

	@Query("SELECT cor FROM CorsiDocentiDao cor WHERE cor.docente = ?1")
	List<CorsiDocentiDao> findByDocente(Long docente);
	

	@Query("SELECT cor FROM CorsiDocentiDao cor WHERE cor.interno = 1 AND cor.docente = ?1")
	List<CorsiDocentiDao> findByDocenteInterno(Long docente);
	
	@Query("SELECT cor FROM CorsiDocentiDao cor WHERE cor.interno = 0 AND cor.docente = ?1")
	List<CorsiDocentiDao> findByDocenteEsterno(Long docente);
	
	
}
