package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.TipologieDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class TipologieDto {
	private Long tipologia_id;
    private String descrizione;
	public void parseDto(TipologieDao d) {
		if(d.getTipologiaId() != null)
			this.tipologia_id= d.getTipologiaId();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();
	
	}
}
