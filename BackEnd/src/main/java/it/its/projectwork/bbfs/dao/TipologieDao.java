package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.TipologieDto;
import lombok.Data;

@Entity
@Table(name = "tipologie")
@Data
public class TipologieDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="tipologia_id", unique=true, nullable=false) 
	private Long tipologiaId;
	@Column(name="descrizione") 
	private String descrizione;
	public void parseDto(TipologieDto d) {
		if(d.getTipologia_id() != null)
			this.tipologiaId= d.getTipologia_id();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();
	
	}

}
