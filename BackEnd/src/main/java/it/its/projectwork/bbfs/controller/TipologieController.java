package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.TipologieDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.CorsiDto;
import it.its.projectwork.bbfs.dto.TipologieDto;
import it.its.projectwork.bbfs.service.TipologieService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/tipologie")

public class TipologieController {
	@Autowired
	private TipologieService service;
	private static final Logger LOGGER = LoggerFactory.getLogger(TipologieController.class);
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchAll() {

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<TipologieDao> docentiDao = service.selTutti();
		ArrayList<TipologieDto> docenti =  new ArrayList<TipologieDto>();
		for(TipologieDao d: docentiDao) {
			TipologieDto doc = new TipologieDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}
	@GetMapping(value = "/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il tip con Id: " + id + "*******");

		Optional<TipologieDao> doc = service.selById(id);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_tip");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;


	}
}
