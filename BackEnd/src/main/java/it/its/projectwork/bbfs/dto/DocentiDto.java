package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.DocentiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class DocentiDto {
	private Long docente_id;
	private String titolo;
	private String ragione_sociale;
	private String indirizzo;
	private String localita;
	private String provincia;
	private String nazione;
	private String telefono;
	private String fax;
	private String partita_iva;
	private String referente;
	
	public void parseDao(DocentiDao d) {
		if(d.getDocenteId() != null)
			this.docente_id= d.getDocenteId();
		if(d.getTitolo() != null)
			this.titolo = d.getTitolo();
		if(d.getRagioneSociale() != null)
			this.ragione_sociale = d.getRagioneSociale();
		if(d.getIndirizzo() != null)
			this.indirizzo = d.getIndirizzo();
		if(d.getLocalita() != null)
			this.localita = d.getLocalita();
		if(d.getNazione() != null)
			this.nazione = d.getNazione();
		if(d.getTelefono() != null)
			this.telefono = d.getTelefono();
		if(d.getReferente() != null)
			this.fax = d.getFax();
		if(d.getReferente() != null)
			this.partita_iva = d.getPartita_iva();
		if(d.getReferente() != null)
			this.referente = d.getReferente();
	}
}
