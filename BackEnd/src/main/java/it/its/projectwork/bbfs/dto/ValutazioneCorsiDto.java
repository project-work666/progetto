package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.ValutazioniCorsiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ValutazioneCorsiDto {
	private Long valutazione_corso_id;
	private String criterio;
	private Double valore;
	private Long corso;
	public void parseDto(ValutazioniCorsiDao d) {
		if(d.getValutazione_corsi_id() != null)
			this.valutazione_corso_id= d.getValutazione_corsi_id();
		if(d.getCriterio() != null)
			this.criterio= d.getCriterio();
		if(d.getValore() != null)
			this.valore= d.getValore();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
	
	}
}
