package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.ValutazioniCorsiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.CorsiDto;
import it.its.projectwork.bbfs.dto.ValutazioneCorsiDto;
import it.its.projectwork.bbfs.service.ValutazioneCorsiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/valutazioni/corso")

public class ValutazioneCorsoController {
	
	@Autowired
	private ValutazioneCorsiService service;
	private static final Logger LOGGER = LoggerFactory.getLogger(ValutazioneCorsoController.class);
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchAll() {

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<ValutazioniCorsiDao> docentiDao = service.selTutti();
		ArrayList<ValutazioneCorsiDto> docenti =  new ArrayList<ValutazioneCorsiDto>();
		for(ValutazioniCorsiDao d: docentiDao) {
			ValutazioneCorsiDto doc = new ValutazioneCorsiDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}
		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;
	}
	

	@GetMapping(value="/corso/{id}", produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchByCorso(@PathVariable("id") Long idCorso) {
		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		List<ValutazioniCorsiDao> docentiDao = service.selByCorso(idCorso);
		ArrayList<ValutazioneCorsiDto> docenti =  new ArrayList<ValutazioneCorsiDto>();
		LOGGER.info("******Otteniamo tutto SIZE:"+ docentiDao.size()+"******");
		for(ValutazioniCorsiDao d: docentiDao) {
			ValutazioneCorsiDto doc = new ValutazioneCorsiDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setResponse(docenti);
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		return response;
		
	}
	
	@PostMapping(value="/batchInsert", produces = "application/json")
	public BaseResponseDto<String> batchInsert(@RequestBody JSONObject test){
		BaseResponseDto<String> response = new BaseResponseDto<>();
		ArrayList<Object> listValutazione = new ArrayList<Object>();
		try {
			Integer temp = (Integer)test.get("corso");
			Long idCorso = Long.parseLong(temp.toString());
			listValutazione = (ArrayList<Object>) test.get("valutazioni");
			for(Object valutazione : listValutazione) {
				LinkedHashMap<String, String> val = (LinkedHashMap<String, String>) valutazione;
				String criterio = val.get("criterio");
				String votoStr = val.get("valutazione");
				Double voto = Double.parseDouble(votoStr);
				ValutazioniCorsiDao dao = new ValutazioniCorsiDao();
				dao.setCorso(idCorso);
				dao.setCriterio(criterio);
				dao.setValore(voto);
				service.insert(dao);
			}
			response.setStatus(HttpStatus.OK.value());
			}catch(Exception e) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			}finally {
				response.setTimestamp(new Date());
				response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
				response.setResponse("Inserite "+ listValutazione.size()+ " valutazione");
			}
		return response;
		
	}
	
	
	
	
}
