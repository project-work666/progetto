package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;

import it.its.projectwork.bbfs.dao.ProvinceDao;
import it.its.projectwork.bbfs.dto.ProvinceDto;

public interface ProvinceService {

	public List<ProvinceDao> selTutti();
	//public List<ProvinceDao> selByIdRegione(String idRegione);
  public ProvinceDto selById(String id);
  ArrayList<ProvinceDto> selByIdRegione(String id);
}
