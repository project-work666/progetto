package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ValutazioneDocentiDto {
	private Long valutazioni_docenti_id;
	private String criterio;
	private Double valore;
	private Long corso;
	private Long docente;
	public void parseDto(ValutazioniDocentiDao d) {
		if(d.getValutazioni_docenti_id() != null)
			this.valutazioni_docenti_id= d.getValutazioni_docenti_id();
		if(d.getCriterio() != null)
			this.criterio= d.getCriterio();
		if(d.getValore() != null)
			this.valore= d.getValore();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
		if(d.getDocente() != null)
			this.docente= d.getDocente();
	
	}
}
