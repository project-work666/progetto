package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.DipendentiDto;
import lombok.Data;

@Entity
@Table(name = "dipendenti")
@Data
public class DipendentiDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dipendente_id", unique=true, nullable=false) 
	private Long dipendenteId;
	
	@Column(name = "matricola")	
	private String matricola;
	
	@Column(name = "nome")
	private String nome;

	@Column(name = "cognome")
	private String cognome;

	@Column(name = "sesso")
	private String sesso;

	@Column(name = "luogo_nascita")
	private String luogoNascita;
	
	@Column(name = "stato_civile")
	private String statoCivile;
	
	@Column(name = "titolo_studio")
	private String titoloStudio;
	
	@Column(name = "conseguito_presso")
	private String conseguitoPresso;
	
	@Column(name = "anno_conseguimento")
	private String annoConseguimento;
	
	@Column(name = "tipo_dipendente")
	private String tipoDipendente;

	@Column(name = "qualifica")
	private String qualifica;
	
	@Column(name = "livello")
	private String livello;
	
	@Column(name = "data_assunzione")
	private String dataAssunzione;
	
	@Column(name = "data_nascita")
	private String dataNascita;
	
	@Column(name = "data_fine_rapporto")
	private String dataFineRapporto;
	
	@Column(name = "data_scadenza_contratto")
	private String dataScadenzaContratto;
	
	@Column(name = "societa")
	private Long societa;
	
	public Long getIdDipendente() {
		return dipendenteId;
	}
	
	public void parseDto(DipendentiDto d) {
		if(d.getDipendente_id() != null)
			this.dipendenteId = d.getDipendente_id();
		if(d.getMatricola() != null)
			this.matricola = d.getMatricola();
		if(d.getNome() != null)
			this.nome = d.getNome();
		if(d.getCognome() != null)
			this.cognome = d.getCognome();
		if(d.getSesso() != null)
			this.sesso = d.getSesso();
		if(d.getLuogo_nascita() != null)
			this.luogoNascita = d.getLuogo_nascita();
		if(d.getStato_civile() != null)
			this.statoCivile = d.getStato_civile();
		if(d.getTitolo_studio() != null)
			this.titoloStudio = d.getTitolo_studio();
		if(d.getConseguito_presso() != null)
			this.conseguitoPresso = d.getConseguito_presso();
		if(d.getAnno_conseguimento() != null)
			this.annoConseguimento = d.getAnno_conseguimento();
		if(d.getTipo_dipendente() != null)
			this.tipoDipendente = d.getTipo_dipendente();
		if(d.getQualifica() != null)
			this.qualifica = d.getQualifica();
		if(d.getLivello() != null)
			this.livello = d.getLivello();
		if(d.getSocieta() != null)
			this.societa = d.getSocieta();
		if(d.getData_assunzione() != null)
			this.dataAssunzione = d.getData_assunzione();
		if(d.getData_nascita() != null)
			this.dataNascita = d.getData_nascita();
		if(d.getData_fine_rapporto() != null)
			this.dataFineRapporto = d.getData_fine_rapporto();
		if(d.getData_scadenza_contratto() != null)
			this.dataScadenzaContratto = d.getData_scadenza_contratto();
	}
	
}
