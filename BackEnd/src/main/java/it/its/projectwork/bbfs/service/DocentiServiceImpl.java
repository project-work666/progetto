package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;
import it.its.projectwork.bbfs.repository.DocentiRepository;

@Service
@Transactional

public class DocentiServiceImpl implements DocentiService {

	@Autowired
	DocentiRepository docentiRepository;

	@Override
	public List<DocentiDao> selTutti() {
		return docentiRepository.findAll();
	}
	
	@Override
	public List<DocentiDao> selTutti(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<DocentiDao> resP = docentiRepository.findAll(p);
		ArrayList<DocentiDao> res = new ArrayList<DocentiDao>();
		for(DocentiDao d : resP) {
			res.add(d);
		}
		return res;
	}
	
	@Override
	public List<DocentiDao> selAllOrdinato(int pagina, int quantita, String sortBy, int dir, String ragioneSociale, String telefono, String titolo, String referente) {
		Pageable p;
		if(sortBy.length() <= 0)
			sortBy = "docenteId";
		if(dir > 0)
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).ascending());
		else{
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).descending());
		}
		Page<DocentiDao> resP = docentiRepository.findByRagioneSocialeLikeIgnoreCaseAndTelefonoLikeIgnoreCaseAndTitoloLikeIgnoreCaseAndReferenteLikeIgnoreCase("%"+ragioneSociale+"%", "%"+telefono+"%","%"+titolo+"%","%"+referente+"%", p);
		ArrayList<DocentiDao> res = new ArrayList<DocentiDao>();
		for(DocentiDao d : resP) {
			res.add(d);
		}
		return res;
	}
	
	
	
	
	@Override
	public Optional<DocentiDao> selById(Long id) {
		return docentiRepository.findById(id);
	}


	@Override
	public void delete(Long id) throws Exception{
		Optional<DocentiDao> d = docentiRepository.findById(id);
		if(d.isPresent()) {
			docentiRepository.deleteById(id);
		}else {
			throw new Exception();
		}
	}

	@Override
	public void insert(DocentiDao dip){
		docentiRepository.saveAndFlush(dip);
	}

	@Override
	public void edit(DocentiDao dip) throws Exception {
		Optional<DocentiDao> d = docentiRepository.findById(dip.getIdDocente());
		if(d.isPresent()) {
			docentiRepository.save(dip);
		}else {
			throw new Exception();
		}
	}

	@Override
	public List<DipendentiDao> selTuttiInterni(int pagina,
			int quantita, String sortBy, int dir,
			String matricola, String nome, String cognome,
			String dataNascita,String tipoDip) {
		// TODO Auto-generated method stub
		
		Pageable p;
		p = PageRequest.of(pagina, quantita);
	
		Page<DipendentiDao> resP = docentiRepository.findAllDocentiInterni("%"+matricola+"%","%"+nome+"%","%"+cognome+"%","%"+dataNascita+"%","%"+tipoDip+"%",p);
		
		ArrayList<DipendentiDao> res = new ArrayList<DipendentiDao>();
		for(DipendentiDao d : resP) {
			res.add(d);
		}
		return res;
	}
	
	
	@Override
	public List<DipendentiDao> selTuttiInterni() {
		// TODO Auto-generated method stub
		List<DipendentiDao> resP = docentiRepository.findAllDocentiInterni();
		
		
		return resP;
	}

}
