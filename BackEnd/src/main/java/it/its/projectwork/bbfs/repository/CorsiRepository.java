package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.DipendentiDao;

public interface CorsiRepository extends PagingAndSortingRepository<CorsiDao,Long>{
	void saveAndFlush(CorsiDao d);
	List<CorsiDao> findAll();
	Page<CorsiDao> findAll(Pageable page);
	List<CorsiDao> findByTipo(Long tipo);
	List<CorsiDao> findByTipologia(Long tipo);
	
	@Query("SELECT cor FROM CorsiDao cor, TipiDao tipi, TipologieDao tipologie where cor.tipo = tipi.tipoId and tipi.descrizione like ?1 and cor.tipologia = tipologie.tipologiaId and tipologie.descrizione like ?2 and cor.descrizione like ?3 and cor.luogo like ?4 and cor.ente like ?5")
	Page<CorsiDao> findCorsi(String tipo,String tipologia,String descrizione, String luogo,String ente, Pageable page);
}
