package it.its.projectwork.bbfs.dto;

import javax.persistence.Column;

import it.its.projectwork.bbfs.dao.RegioniDao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegioniDto {

	@Column(name = "id_region")
	private Long idRegion;
	
	@Column(name = "description")
	private String name;
	
	@Column(name = "iso_coun")
	private String isoCountry;
	public void parseDto(RegioniDao d) {
		if(d.getIsoCountry() != null)
			this.isoCountry= d.getIsoCountry();
		if(d.getDescription() != null)
			this.name= d.getDescription();
		if(d.getIdRegion() != null)
			this.idRegion= d.getIdRegion();

	}
}
