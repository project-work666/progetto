package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;
import it.its.projectwork.bbfs.repository.ValutazioneDocentiRepository;

@Service
@Transactional
public class ValutazioneDocentiServiceImpl implements ValutazioneDocentiService {
	@Autowired
	ValutazioneDocentiRepository rep;

	@Override
	public List<ValutazioniDocentiDao> selTutti() {
		return rep.findAll();
	}

	@Override
	public void insert(ValutazioniDocentiDao d) {
		rep.saveAndFlush(d);
	}

	@Override
	public Double media(Long d, Long c) {
		List<ValutazioniDocentiDao> t = rep.findByDocenteAndCorso(d, c);
		double l = (double) t.size();
		double s = (double) 0;
		for (ValutazioniDocentiDao valutazioniDocentiDao : t) {
			s = s + valutazioniDocentiDao.getValore();
		}
		return s / l;
	}

	@Override
	public void delete(Long id) throws Exception {
		Optional<ValutazioniDocentiDao> d = rep.findById(id);
		if (d.isPresent()) {
			rep.deleteById(id);
		} else {
			throw new Exception();
		}
	}

	@Override
	public List<ValutazioniDocentiDao> trovaByDocAndCorso(Long d, Long c) {
		return rep.findByDocenteAndCorso(d, c);
	}
}
