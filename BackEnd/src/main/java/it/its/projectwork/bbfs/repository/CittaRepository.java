package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.projectwork.bbfs.dao.CittaDao;

@Repository
public interface CittaRepository extends JpaRepository<CittaDao, Long> {

	List<CittaDao> findByIdProv(String id);
}