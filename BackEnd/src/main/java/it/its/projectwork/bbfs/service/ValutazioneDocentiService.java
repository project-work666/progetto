package it.its.projectwork.bbfs.service;

import java.util.List;

import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;

public interface ValutazioneDocentiService {
	public List<ValutazioniDocentiDao> selTutti();
	public List<ValutazioniDocentiDao> trovaByDocAndCorso(Long d, Long c);
	public void insert(ValutazioniDocentiDao d);
	public Double media(Long d, Long c);
	public void delete(Long id) throws Exception;
}
