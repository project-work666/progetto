package it.its.projectwork.bbfs.dto;

import javax.persistence.Column;

import it.its.projectwork.bbfs.dao.NazioniDao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NazioniDto {

	@Column(name = "iso")
	private String idNazione;
	
	@Column(name = "description")
	private String name;
	public void parseDto(NazioniDao d) {
		if(d.getIso() != null)
			this.idNazione= d.getIso();
		if(d.getDescription() != null)
			this.name= d.getDescription();

	}

}