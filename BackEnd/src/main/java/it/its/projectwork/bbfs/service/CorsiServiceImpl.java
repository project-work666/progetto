package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.TipiDao;
import it.its.projectwork.bbfs.dao.TipologieDao;
import it.its.projectwork.bbfs.repository.CorsiRepository;
import it.its.projectwork.bbfs.repository.TipiRepository;
import it.its.projectwork.bbfs.repository.TipologieRepository;
@Service
@Transactional
public class CorsiServiceImpl implements CorsiService{
	@Autowired
    CorsiRepository rep;
	
	@Autowired
	TipologieRepository tipologierepo;
	
	@Autowired
	TipiRepository tipirepo;
	
	@Override
	public List<CorsiDao> selTutti() {
		return rep.findAll();
	}
	@Override
	public List<CorsiDao> selTipo(Long c) {
		return rep.findByTipo(c);
	}
	
	@Override
	public List<CorsiDao> selTutti(int pagina, int quantita,String sortBy, int dir, String tipo, String tipologia, String descrizione, String luogo, String ente) {
		Pageable p;
		if(sortBy.length() <= 0)
			sortBy = "corsoId";
		if(dir > 0)
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).ascending());
		else{
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).descending());
		}

		

		Page<CorsiDao> resP = rep.findCorsi("%"+tipo+"%","%"+tipologia+"%", "%"+descrizione+"%", "%"+luogo+"%","%"+ente+"%", p);
		ArrayList<CorsiDao> res = new ArrayList<CorsiDao>();
		for(CorsiDao d : resP) {
			res.add(d);
		}
		return res;
	}

	@Override
	public Optional<CorsiDao> selById(Long d) {
		return rep.findById(d);
	}

	@Override
	public void delete(Long id) throws Exception {
		
		Optional<CorsiDao> d = rep.findById(id);
		if(d.isPresent()) {
			rep.deleteById(id);
		}else {
			throw new Exception();
		}
	}

	@Override
	public void insert(CorsiDao d) {
		rep.saveAndFlush(d);
	}

	@Override
	public void edit(CorsiDao d1) throws Exception {
		Optional<CorsiDao> d = rep.findById(d1.getCorsoId());
		if(d.isPresent()) {
			rep.save(d1);
		}else {
			throw new Exception();
		}
	}
	@Override
	public List<CorsiDao> selTipologia(Long c) {
		return rep.findByTipologia(c);
	}

}
