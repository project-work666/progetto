package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.TipologieDao;

public interface TipologieRepository extends PagingAndSortingRepository<TipologieDao,Long>{
	List<TipologieDao> findAll();
	TipologieDao findByDescrizione(String descrizione);
	
}
