package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dto.BaseResponseDto;

@RestController
@RequestMapping(value = "ping")
public class test {
	@GetMapping(produces = "application/json")
	public BaseResponseDto<ArrayList<String>> testGet(){

		BaseResponseDto<ArrayList<String>> res = new BaseResponseDto<ArrayList<String>>();
		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		res.setResponse("get funziona");
		return res;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<ArrayList<String>> findByProvinceId(@RequestBody String test){
		BaseResponseDto<ArrayList<String>> res = new BaseResponseDto<ArrayList<String>>();
		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		res.setResponse(test);
		return res;
	}
}