package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.projectwork.bbfs.dao.RegioniDao;

@Repository
public interface RegioniRepository  extends JpaRepository<RegioniDao, Long> {
	List<RegioniDao> findByIsoCountry(String iso_coun);
}