package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.ValutazioniDocentiDao;

public interface ValutazioneDocentiRepository extends PagingAndSortingRepository <ValutazioniDocentiDao,Long>{
	void saveAndFlush(ValutazioniDocentiDao d);
	List<ValutazioniDocentiDao> findAll();
	@Query("SELECT val FROM ValutazioniDocentiDao val where val.docente = ?1 and val.corso = ?2")
    List<ValutazioniDocentiDao> findByDocenteAndCorso(Long docente, Long corso);
}