package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.ValutazioniCorsiDao;

public interface ValutazioneCorsiRepository extends PagingAndSortingRepository< ValutazioniCorsiDao,Long>{
	void saveAndFlush(ValutazioniCorsiDao d);
	List<ValutazioniCorsiDao> findAll();
	List<ValutazioniCorsiDao> findByCorso(Long corso);
}
