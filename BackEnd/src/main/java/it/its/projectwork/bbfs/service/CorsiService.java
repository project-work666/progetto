package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.CorsiDao;

public interface CorsiService {
	public List <CorsiDao> selTutti();
	public List <CorsiDao> selTutti(int pagina, int quantita,String sortBy, int dir, String tipo, String tipologia, String descrizione, String luogo, String ente);
	
	public Optional<CorsiDao> selById(Long d);
	
	public void delete(Long d) throws Exception;

	public void insert(CorsiDao d);
	
	public void edit(CorsiDao d) throws Exception;
	
	public List<CorsiDao> selTipo(Long c);
	
	public List<CorsiDao> selTipologia(Long c);
}
