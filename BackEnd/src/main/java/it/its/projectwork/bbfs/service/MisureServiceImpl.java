package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.MisureDao;
import it.its.projectwork.bbfs.repository.MisureRepository;
@Service
@Transactional
public class MisureServiceImpl implements MisureService{
	@Autowired
	MisureRepository misureRepository;
	@Override
	public Optional<MisureDao> selById(Long id) {
		
		return misureRepository.findById(id);
	}
	@Override
	public List<MisureDao> selTutti() {
		return misureRepository.findAll();
	}

}
