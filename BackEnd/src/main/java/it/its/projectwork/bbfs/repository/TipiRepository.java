package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import it.its.projectwork.bbfs.dao.TipiDao;

public interface TipiRepository extends PagingAndSortingRepository<TipiDao,Long>{
	List<TipiDao> findAll();
	
	TipiDao findByDescrizione(String descrizione);
}
