package it.its.projectwork.bbfs.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.SocietaDao;
import it.its.projectwork.bbfs.repository.SocietaRepository;

@Service
@Transactional

public class SocietaServiceImpl implements SocietaService{
	@Autowired
	SocietaRepository societaRepository;
	@Override
	public Optional<SocietaDao> selById(Long id) {
		return societaRepository.findById(id);
	}

}
