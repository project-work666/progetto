package it.its.projectwork.bbfs.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import it.its.projectwork.bbfs.dao.CorsiUtentiDao;
import it.its.projectwork.bbfs.dao.DipendentiDao;

public interface CorsiUtentiReository extends PagingAndSortingRepository<CorsiUtentiDao, Long>{
	List<CorsiUtentiDao> findAll();
	List<CorsiUtentiDao> findByCorso(Long corso);
	List<CorsiUtentiDao> findByDipendente(Long dipendente);
	void saveAndFlush(CorsiUtentiDao d);
	
	@Query("SELECT DISTINCT dip FROM DipendentiDao dip join CorsiUtentiDao cor on dip.dipendenteId = cor.dipendente where dip.matricola like ?1 and dip.nome like ?2 and dip.cognome like ?3 and dip.dataNascita like ?4 and dip.tipoDipendente like ?5")
	Page<DipendentiDao> findCorsisti(String matricola,String nome,String cognome, String dataNascita,String tipoDip, Pageable page);
	
	@Query("SELECT dip FROM DipendentiDao dip join CorsiUtentiDao cor on dip.dipendenteId = cor.dipendente")
	List<DipendentiDao> findAllCorsisti();
	
	@Query("SELECT DISTINCT dip FROM DipendentiDao dip join CorsiUtentiDao cor on dip.dipendenteId = cor.dipendente")
	List<DipendentiDao> findAllCorsistiDist();

	@Query("SELECT dip FROM DipendentiDao dip join CorsiUtentiDao cor on dip.dipendenteId = cor.dipendente where dip.matricola like ?1 and dip.nome like ?2 and dip.cognome like ?3 and dip.dataNascita like ?4 and dip.tipoDipendente like ?5 and cor.corso = ?6")
	Page<DipendentiDao> findCorsisti(String matricola,String nome,String cognome, String dataNascita,String tipoDip, Long corso,Pageable page);
	
	@Query("SELECT dip FROM DipendentiDao dip join CorsiUtentiDao cor on dip.dipendenteId = cor.dipendente where cor.corso = ?1")
	List<DipendentiDao> findCorsisti(Long corso);
	
	@Query("SELECT cor FROM CorsiUtentiDao cor where cor.corso = ?1 and cor.dipendente = ?2")
	Optional<CorsiUtentiDao> findByCorDip(Long corso, Long dipendente);

}
