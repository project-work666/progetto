package it.its.projectwork.bbfs.dao;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.CittaDto;
import lombok.Data;

@Entity
@Table(name = "citta")
@Data
public class CittaDao 
{
	@Id
	@Column(name = "id_city")
	private Long idCity;
	
	@Basic(optional = false)
	@Column(name = "description")
	private String name;
	
	@Column(name = "id_prov")
	private String idProv;
	public void parseDto(CittaDto d) {	
		if(d.getIdCity() != null)
			this.idCity= d.getIdCity();
		if(d.getName() != null)
			this.name= d.getName();
		if(d.getIdProv() != null)
			this.idProv= d.getIdProv();
	}
	
}