package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import it.its.projectwork.bbfs.dto.TipiDto;
import lombok.Data;

@Entity
@Table(name = "tipi")
@Data
public class TipiDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="tipo_id", unique=true, nullable=false) 
	private Long tipoId;
	@Column(name="descrizione") 
	private String descrizione;
	public void parseDto(TipiDto d) {
		if(d.getTipo_id() != null)
			this.tipoId= d.getTipo_id();
		if(d.getDescrizione() != null)
			this.descrizione= d.getDescrizione();

	}
}
