package it.its.projectwork.bbfs.dto;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class DipendentiDto {
 
	private Long dipendente_id;
	private String matricola;
	private String nome;
	private String cognome;
	private String sesso;
	private String luogo_nascita;
	private String stato_civile;
	private String titolo_studio;
	private String conseguito_presso;
	private String anno_conseguimento;
	private String tipo_dipendente;
	private String qualifica;
	private String livello;
	private String data_assunzione;
	private String data_nascita;
	private String data_fine_rapporto;
	private String data_scadenza_contratto;
	private Long societa;
	public void parseDao(DipendentiDao d) {
		if(d.getDipendenteId() != null)
			this.dipendente_id = d.getDipendenteId();
		if(d.getMatricola() != null)
			this.matricola = d.getMatricola();
		if(d.getNome() != null)
			this.nome = d.getNome();
		if(d.getCognome() != null)
			this.cognome = d.getCognome();
		if(d.getSesso() != null)
			this.sesso = d.getSesso();
		if(d.getLuogoNascita() != null)
			this.luogo_nascita = d.getLuogoNascita();
		if(d.getStatoCivile() != null)
			this.stato_civile = d.getStatoCivile();
		if(d.getTitoloStudio() != null)
			this.titolo_studio = d.getTitoloStudio();
		if(d.getConseguitoPresso() != null)
			this.conseguito_presso = d.getConseguitoPresso();
		if(d.getAnnoConseguimento() != null)
			this.anno_conseguimento = d.getAnnoConseguimento();
		if(d.getTipoDipendente() != null)
			this.tipo_dipendente = d.getTipoDipendente();
		if(d.getQualifica() != null)
			this.qualifica = d.getQualifica();
		if(d.getLivello() != null)
			this.livello = d.getLivello();
		if(d.getSocieta() != null)
			this.societa = d.getSocieta();
		if(d.getDataAssunzione() != null)
			this.data_assunzione = d.getDataAssunzione();
		if(d.getDataNascita() != null)
			this.data_nascita = d.getDataNascita();
		if(d.getDataFineRapporto() != null)
			this.data_fine_rapporto = d.getDataFineRapporto();
		if(d.getDataScadenzaContratto() != null)
			this.data_scadenza_contratto = d.getDataScadenzaContratto();
	}
	
}
