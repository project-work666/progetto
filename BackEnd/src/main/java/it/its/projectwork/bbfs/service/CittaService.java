package it.its.projectwork.bbfs.service;

import java.util.ArrayList;

import it.its.projectwork.bbfs.dto.CittaDto;



public interface CittaService {

	public ArrayList<CittaDto> selByIdProv(String id);
	public ArrayList<CittaDto> selTuttiIds();
	public ArrayList<CittaDto> selTutti();
	public CittaDto selById(Long id);
}