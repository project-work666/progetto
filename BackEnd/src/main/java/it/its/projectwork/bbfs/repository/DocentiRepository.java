package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;

public interface DocentiRepository extends PagingAndSortingRepository<DocentiDao, Long>{ 
	void saveAndFlush(DocentiDao d);
	List<DocentiDao> findAll();
	Page<DocentiDao> findAll(Pageable page);
	Page<DocentiDao> findByRagioneSocialeLikeIgnoreCaseAndTelefonoLikeIgnoreCaseAndTitoloLikeIgnoreCaseAndReferenteLikeIgnoreCase(String ragioneSociale,String telefono,String titolo, String referente, Pageable page);
	
	@Query("SELECT dip FROM DipendentiDao dip join CorsiDocentiDao cor on dip.dipendenteId = cor.docente where cor.interno=1 and dip.matricola like ?1 and dip.nome like ?2 and dip.cognome like ?3 and dip.dataNascita like ?4 and dip.tipoDipendente like ?5")
	Page<DipendentiDao> findAllDocentiInterni(String matricola, String nome, String cognome, String dataNascita, String tipoDip, Pageable page);

	@Query("SELECT dip FROM DipendentiDao dip join CorsiDocentiDao cor on dip.dipendenteId = cor.docente where cor.interno=1")
	List<DipendentiDao> findAllDocentiInterni();

}
