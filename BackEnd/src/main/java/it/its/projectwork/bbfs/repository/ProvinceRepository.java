package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.projectwork.bbfs.dao.ProvinceDao;

@Repository
public interface ProvinceRepository  extends JpaRepository<ProvinceDao, String> {
	List<ProvinceDao> findByIdRegione(String idRegione);
}