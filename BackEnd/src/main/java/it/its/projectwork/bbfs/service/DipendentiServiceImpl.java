package it.its.projectwork.bbfs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.repository.DipendentiRepository;

@Service
@Transactional

public class DipendentiServiceImpl implements DipendentiService {

	@Autowired
	DipendentiRepository dipendentiRepository;
	
	@Override
	public List<DipendentiDao> selTutti() {
		return dipendentiRepository.findAllByOrderByCognome();
	}

	@Override
	public List<DipendentiDao> selTutti(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<DipendentiDao> resP = dipendentiRepository.findAllByOrderByCognome(p);
		ArrayList<DipendentiDao> res = new ArrayList<DipendentiDao>();
		for(DipendentiDao d : resP) {
			res.add(d);
		}
		return res;
		
	}

	@Override
	public Optional<DipendentiDao> selById(Long id) {
		return dipendentiRepository.findById(id);
	}

	@Override
	public List<DipendentiDao> selDipendenti(int pagina, int quantita, String sortBy, int dir, String matricola,
			String nome, String cognome, String dataNascita, String tipoDip) {
		Pageable p;
		if(sortBy.length() <= 0)
			sortBy = "dipendenteId";
		if(dir > 0)
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).ascending());
		else{
			p = PageRequest.of(pagina, quantita, Sort.by(sortBy).descending());
		}
		Page<DipendentiDao> resP = dipendentiRepository.findDipendenti("%"+matricola+"%","%"+nome+"%","%"+cognome+"%","%"+dataNascita+"%","%"+tipoDip+"%",p);
		ArrayList<DipendentiDao> res = new ArrayList<DipendentiDao>();
		for(DipendentiDao d : resP) {
			res.add(d);
		}
		return res;
	}

}
