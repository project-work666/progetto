package it.its.projectwork.bbfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		for(String arg: args) {
			System.err.println(arg);
		}
		SpringApplication.run(Application.class, args);
	}

}
