package it.its.projectwork.bbfs.service;

import java.util.List;
import java.util.Optional;

import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;
public interface DocentiService {

	public List <DocentiDao> selTutti();
	public List <DocentiDao> selTutti(int pagina, int quantita);
	public List<DocentiDao> selAllOrdinato(int pagina,int quantita, String sortBy, int dir, String ragioneSociale, String telefono, String titolo, String referente);
	
	public List<DipendentiDao> selTuttiInterni(int pagina,
			int quantita, String sortBy, int dir,
			String matricola, String nome, String cognome,
			String dataNascita,String tipoDip);
	
	public List<DipendentiDao> selTuttiInterni();
	
	public Optional<DocentiDao> selById(Long id);
	
	public void delete(Long id) throws Exception;

	public void insert(DocentiDao dip);
	
	public void edit(DocentiDao dip) throws Exception;
	
}
