package it.its.projectwork.bbfs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import it.its.projectwork.bbfs.dao.ValutazioniUtentiDao;

public interface ValutazioneUtentiRepository extends PagingAndSortingRepository<ValutazioniUtentiDao, Long>{
	void saveAndFlush( ValutazioniUtentiDao d);
	List< ValutazioniUtentiDao> findAll();
	List<ValutazioniUtentiDao> findByDipendente(Long i);
	@Query("SELECT val FROM ValutazioniUtentiDao val where val.dipendente = ?1 and val.corso = ?2")
	List<ValutazioniUtentiDao> findByDipendenteAndCorso(Long docente, Long corso);
}
