package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dao.CorsiUtentiDao;
import it.its.projectwork.bbfs.dao.DipendentiDao;
import it.its.projectwork.bbfs.dao.DocentiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.DipendentiDto;
import it.its.projectwork.bbfs.service.CorsiUtenteService;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/alunni")
public class AlunniController {
	@Autowired
	private CorsiUtenteService dipendentiService; 
	private static final Logger LOGGER = LoggerFactory.getLogger(AlunniController.class);
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<DipendentiDto>> fetchAll() {

		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DipendentiDao> dipendentiDao = dipendentiService.selCorsisti();
		ArrayList<DipendentiDto> dipendenti =  new ArrayList<DipendentiDto>();
		for(DipendentiDao d: dipendentiDao) {
			DipendentiDto doc = new DipendentiDto();
			doc.parseDao(d);
			dipendenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + dipendenti.size());

		response.setResponse(dipendenti);
		return response;

	}
	
	@GetMapping(value = "/getPagina" , produces = "application/json")
	public BaseResponseDto<List<DipendentiDto>> fetchAllPagina(@RequestParam(required = false)  String filters) {

		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
		
		
		LOGGER.info("******Otteniamo tutto******");
		
		JSONParser parser = new JSONParser();
		System.err.println(filters);
		String matricola = "";
		String nome = "";
		String cognome = "";
		String dataNascita ="";
		String tipoDip ="";
		Long page = (long) 0;
		Long quant = (long) 10;
		String sortF ="dipendenteId";

		String sortField ="dipendenteId";
		Integer sortDir = 1;
		Integer sortD = 1;
		
		try {
			JSONObject json = (JSONObject) parser.parse(filters);
			
			JSONObject jsonFiltri = (JSONObject) json.get("filters");
			JSONObject val = (JSONObject) jsonFiltri.get("matricola");
			if(val != null)
				matricola = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("nome");
			if(val != null)
				nome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("cognome");
			if(val != null)
				cognome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("data_nascita");
			if(val != null) {
				if((String) val.get("value") != null ) {					
					dataNascita = (String) val.get("value");
					String[] temp = dataNascita.split("/");
					if(temp.length > 0 ) {
						String tempStr = "";
						if(temp.length == 3) {
							if( temp[2].length() < 4)
								while(temp[2].length() < 4) {
									System.err.println(temp[2]);
									temp[2] += "%";
								}
						}
						for(int i = temp.length; i > 0; i--) {
							if(temp[i-1].length() ==  1) {
								temp[i-1] = "0"+temp[i-1];
							}
							tempStr += temp[i-1]+"-";
						}
						
						if(tempStr.charAt(tempStr.length()-1) == '-') tempStr = tempStr.substring(0, tempStr.length()-1);
					
						dataNascita = tempStr;
					}
				}else {
					dataNascita = "";

				}
			}
			
			val = (JSONObject) jsonFiltri.get("tipo_dipendente");
			if(val != null)
				tipoDip = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			quant = (Long)json.get("rows");
			page = (Long)json.get("first")/quant;
			
			sortField = (String) json.get("sortField");
			Long sd = (Long) json.get("sortOrder");
			sortDir = sd.intValue();
			
			if(sortField != null) {
				if(sortField.equals("data_nascita")) {
					sortF = "dataNascita";
				}
				else {
					if(sortField.equals("tipo_dipendente")) {
						sortF = "tipoDipendente";
					}else {
						sortF = sortField;
					}
				}
				sortD = sortDir;
			}
			System.err.println(sortF);
			System.err.println(sortD);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch ( NullPointerException e){
			
		}
		
		
		
		List<DipendentiDao> dipendentiDao = dipendentiService.selCorsistiOrdinato(page.intValue(),quant.intValue(),sortF,sortD,matricola, nome, cognome,dataNascita,tipoDip);
		ArrayList<DipendentiDto> dipendenti =  new ArrayList<DipendentiDto>();
		for(DipendentiDao d: dipendentiDao) {
			DipendentiDto doc = new DipendentiDto();
			doc.parseDao(d);
			dipendenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + dipendenti.size());

		response.setResponse(dipendenti);
		return response;

	}
	
	
	@GetMapping(value = "/total", produces = "application/json")
	public BaseResponseDto<Integer> howMany() {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<DipendentiDao> docentiDao = dipendentiService.selCorsistiDist();
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docentiDao.size());
		return response;
	}
	
	@GetMapping(value = "/tota/{id}", produces = "application/json")
	public BaseResponseDto<Integer> howManyCorso(@PathVariable("id") Long id) {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");
		
		System.err.println(id);
		List<DipendentiDao> docentiDao = dipendentiService.selCorsisti(id);
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docentiDao.size());
		return response;
	}
	
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id, @RequestParam(required = false)  String filters)

	{
		BaseResponseDto<List<DipendentiDto>> response = new BaseResponseDto<>();
		JSONParser parser = new JSONParser();
		
		if(id == -1) {
			System.err.println("\n\n\n\nAIUTOOOO");
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("NESSUN_CORSO");
			response.setResponse(new ArrayList());
			return response;
		}
		System.err.println(filters);
		String matricola = "";
		String nome = "";
		String cognome = "";
		String dataNascita ="";
		String tipoDip ="";
		Long page = (long) 0;
		Long quant = (long) 10;
		String sortF ="dipendenteId";

		String sortField ="dipendenteId";
		Integer sortDir = 1;
		Integer sortD = 1;
		
		try {
			JSONObject json = (JSONObject) parser.parse(filters);
			
			JSONObject jsonFiltri = (JSONObject) json.get("filters");
			JSONObject val = (JSONObject) jsonFiltri.get("matricola");
			if(val != null)
				matricola = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("nome");
			if(val != null)
				nome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("cognome");
			if(val != null)
				cognome = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("data_nascita");
			if(val != null)
				dataNascita = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("tipo_dipendente");
			if(val != null)
				dataNascita = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			quant = (Long)json.get("rows");
			page = (Long)json.get("first")/quant;
			
			sortField = (String) json.get("sortField");
			Long sd = (Long) json.get("sortOrder");
			sortDir = sd.intValue();
			
			if(sortField != null) {
				if(sortField.equals("data_nascita")) {
					sortF = "dataNascita";
				}
				else {
					if(sortField.equals("tipo_dipendente")) {
						sortF = "tipoDipendente";
					}else {
						sortF = sortField;
					}
				}
				sortD = sortDir;
			}
			System.err.println(sortF);
			System.err.println(sortD);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch ( NullPointerException e){
			
		}
		
		
		
		List<DipendentiDao> dipendentiDao = dipendentiService.selCorsisti(page.intValue(),quant.intValue(),sortF,sortD,matricola, nome, cognome,dataNascita,tipoDip,id);
		ArrayList<DipendentiDto> dipendenti =  new ArrayList<DipendentiDto>();
		for(DipendentiDao d: dipendentiDao) {
			DipendentiDto doc = new DipendentiDto();
			doc.parseDao(d);
			dipendenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + dipendenti.size());

		response.setResponse(dipendenti);
		return response;
	}
	
	@GetMapping(value = "/corsi/{c}", produces = "application/json")
	public BaseResponseDto<?> DocByc(@PathVariable("c") Long c)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il dip con Id: " + c + "*******");

		List<CorsiDao> doc = dipendentiService.selCorsi(c);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;
	}
	
	@DeleteMapping(value = "/elimina/{id}")
	public BaseResponseDto<?> deleteDoc(@PathVariable("id") Long id) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("Eliminiamo il docente con id " + id);
		try {
			dipendentiService.delete(id);
			response.setResponse("{}");
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		} catch (Exception e) {
			response.setResponse(null);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.setMessage("NON_TROVO_IL_DOCENTE");
			
		}
		response.setTimestamp(new Date());
		return response;
	}
	
	@PostMapping(produces = "application/json")
	public BaseResponseDto<?> createDoc(@RequestBody CorsiUtentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			dipendentiService.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
	
	@GetMapping(value = "/trova/{c}_{d}", produces = "application/json")
	public BaseResponseDto<?> trovaByCorDip(@PathVariable("c") Long c, @PathVariable("d") Long d)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il dip con Id: " + c + "*******");

		Optional<CorsiUtentiDao> doc = dipendentiService.findCorsoAlunno(c, d);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;
	}
	
	@PutMapping(produces = "application/json")
	public BaseResponseDto<?> updateDoc(@RequestBody CorsiUtentiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("***** Modifichiamo i corsiutenti con id " + doc.getCorsi_utenti_id() + " *****");
			try {
				dipendentiService.edit(doc);
				response.setResponse(doc);
				response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.OK.value());
			} catch (Exception e) {
				response.setTimestamp(new Date());
				response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				
			}
			response.setTimestamp(new Date());
			return response;
	}
}
