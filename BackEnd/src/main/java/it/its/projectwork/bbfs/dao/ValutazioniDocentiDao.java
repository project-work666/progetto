package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.ValutazioneDocentiDto;
import lombok.Data;

@Entity
@Table(name = "valutazioni_docenti")
@Data
public class ValutazioniDocentiDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="valutazioni_docenti_id", unique=true, nullable=false) 
	private Long valutazioni_docenti_id;
	@Column(name="criterio") 
	private String criterio;
	@Column(name="valore") 
	private Double valore;
	@Column(name="corso") 
	private Long corso;
	@Column(name="docente") 
	private Long docente;
	public void parseDto(ValutazioneDocentiDto d) {
		if(d.getValutazioni_docenti_id() != null)
			this.valutazioni_docenti_id= d.getValutazioni_docenti_id();
		if(d.getCriterio() != null)
			this.criterio= d.getCriterio();
		if(d.getValore() != null)
			this.valore= d.getValore();
		if(d.getCorso() != null)
			this.corso= d.getCorso();
		if(d.getDocente() != null)
			this.docente= d.getDocente();
	
	}
}
