package it.its.projectwork.bbfs.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.bbfs.dao.CorsiDao;
import it.its.projectwork.bbfs.dto.BaseResponseDto;
import it.its.projectwork.bbfs.dto.CorsiDto;
import it.its.projectwork.bbfs.service.CorsiService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/corsi")
public class CorsiController {

	@Autowired
	private CorsiService service;
	private static final Logger LOGGER = LoggerFactory.getLogger(CorsiController.class);
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchAll() {

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		LOGGER.info("******Otteniamo tutto******");

		List<CorsiDao> docentiDao = service.selTutti();
		ArrayList<CorsiDto> docenti =  new ArrayList<CorsiDto>();
		for(CorsiDao d: docentiDao) {
			CorsiDto doc = new CorsiDto();
			doc.parseDto(d);
			docenti.add(doc);
		}
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + docenti.size());

		response.setResponse(docenti);
		return response;

	}

	
	@GetMapping(value = "/total" , produces = "application/json")
	public BaseResponseDto<Integer> total() {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();
		response.setResponse(service.selTutti().size());
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		return response;
	}
	
	
	
	@GetMapping(value = "/getPagina" , produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchAllPagina(@RequestParam(required = false)  String filters) {

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		
		
		LOGGER.info("******Otteniamo tutto******");
		
		JSONParser parser = new JSONParser();
		System.err.println(filters);
		String tipo = "";
		String tipologia = "";
		String descrizione = "";
		String luogo ="";
		String ente ="";
		
		Long page = (long) 0;
		Long quant = (long) 10;
		
		String sortF ="corsoId";
		String sortField ="corsoId";
		
		Integer sortDir = 1;
		Integer sortD = 1;
		
		try {
			JSONObject json = (JSONObject) parser.parse(filters);
			
			JSONObject jsonFiltri = (JSONObject) json.get("filters");
			JSONObject val = (JSONObject) jsonFiltri.get("tipo");
			if(val != null)
				tipo = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("tipologia");
			if(val != null)
				tipologia = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("descrizione");
			if(val != null)
				descrizione = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("luogo");
			if(val != null)
				luogo = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			val = (JSONObject) jsonFiltri.get("ente");
			if(val != null)
				ente = (String) val.get("value") == null ? "" : (String) val.get("value");
			
			quant = (Long)json.get("rows");
			page = (Long)json.get("first")/quant;
			
			sortField = (String) json.get("sortField");
			Long sd = (Long) json.get("sortOrder");
			sortDir = sd.intValue();
			
			if(sortField != null) {
				if(sortField.equals("data_nascita")) {
					sortF = "dataNascita";
				}
				else {
					if(sortField.equals("tipo_dipendente")) {
						sortF = "tipoDipendente";
					}else {
						sortF = sortField;
					}
				}
				sortD = sortDir;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch ( NullPointerException e){
			
		}
		
		
		
		List<CorsiDao> corsiDao = service.selTutti(page.intValue(), quant.intValue(), sortF, sortD, tipo, tipologia, descrizione, luogo, ente) ;
		ArrayList<CorsiDto> corsi =  new ArrayList<CorsiDto>();
		
		for(CorsiDao d: corsiDao) {
			CorsiDto cor = new CorsiDto();
			cor.parseDto(d);
			corsi.add(cor);
		}
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (corsi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		LOGGER.info("Numero dei record: " + corsi.size());

		response.setResponse(corsi);
		return response;

	}
	
	

	@GetMapping(value = "/id/{id}", produces = "application/json")
	public BaseResponseDto<?> DocById(@PathVariable("id") Long id)

	{
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il docente con Id: " + id + "*******");

		Optional<CorsiDao> doc = service.selById(id);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;


	}
	@GetMapping(value = "/tipo/{cor}", produces = "application/json")
	public BaseResponseDto<?> DocByCor(@PathVariable("cor") Long cor){
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il dip c*******");

		List<CorsiDao> doc = service.selTipo(cor);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;



	}
	@GetMapping(value = "/tipologia/{co}", produces = "application/json")
	public BaseResponseDto<?> DocByCo(@PathVariable("co") Long co){
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("****** Otteniamo il dip c*******");

		List<CorsiDao> doc = service.selTipologia(co);

		if (doc == null) {
			response.setResponse(null);
			response.setMessage("NON_TROVO_IL_DOCENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(doc);
		return response;



	}

	@DeleteMapping(value = "/elimina/{id}")
	public BaseResponseDto<?> deleteDoc(@PathVariable("id") Long id) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		LOGGER.info("Eliminiamo il docente con id " + id);
		try {
			service.delete(id);
			response.setResponse("{}");
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		} catch (Exception e) {
			response.setResponse(null);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			response.setMessage("NON_TROVO_IL_DOCENTE");
			
		}
		response.setTimestamp(new Date());
		return response;
	}

	@PutMapping(produces = "application/json")
	public BaseResponseDto<?> updateDoc(@RequestBody CorsiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		System.err.println(doc);
			try {
				service.edit(doc);
				response.setResponse(doc);
				response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.OK.value());
			} catch (Exception e) {
				System.err.println(e);
				response.setTimestamp(new Date());
				response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				
			}
			response.setTimestamp(new Date());
			return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<?> createDoc(@RequestBody CorsiDao doc) {
		BaseResponseDto<?> response = new BaseResponseDto<>();
		try {			
			System.err.println(doc.toString());
			service.insert(doc);
			response.setResponse(doc);
			response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.OK.value());
		} catch (Exception e) {
			System.err.println(e);
			response.setTimestamp(new Date());
			response.setMessage("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			
		}
		response.setTimestamp(new Date());
		
		return response;
	}
}
