package it.its.projectwork.bbfs.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import it.its.projectwork.bbfs.dto.SocietaDto;
import lombok.Data;

@Entity
@Table(name = "societa")
@Data
public class SocietaDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="societa_id", unique=true, nullable=false) 
	private Long societa_id;
	@Column(name = "ragione_sociale")
	private String ragione_sociale;
	@Column(name = "indirizzo")
	private String indirizzo;
	@Column(name = "localita")
	private String localita;
	@Column(name = "provincia")
	private String provincia;
	@Column(name = "nazione")
	private String nazione;
	@Column(name = "telefono")
	private String telefono;
	@Column(name = "fax")
	private String fax;
	@Column(name = "partita_iva")
	private String partita_iva;
	public Long getIdSocieta() {
		return societa_id;
	}
	public void parseDao(SocietaDto d) {
		if(d.getSocieta_id() != null)
			this.societa_id= d.getSocieta_id();
		if(d.getRagione_sociale() != null)
			this.ragione_sociale= d.getRagione_sociale();
		if(d.getIndirizzo() != null)
			this.indirizzo= d.getIndirizzo();
		if(d.getLocalita() != null)
			this.localita= d.getLocalita();
		if(d.getProvincia() != null)
			this.provincia= d.getProvincia();
		if(d.getNazione() != null)
			this.nazione= d.getNazione();
		if(d.getTelefono() != null)
			this.telefono= d.getTelefono();	
		if(d.getFax() != null)
			this.fax= d.getFax();
		if(d.getPartita_iva() != null)
			this.partita_iva= d.getPartita_iva();
	}
}
