package it.its.projectwork.bbfs.dao;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import it.its.projectwork.bbfs.dto.RegioniDto;
import lombok.Data;

@Entity
@Table(name = "regioni")
@Data
public class RegioniDao 
{

	@Id
	@Column(name = "id_region")
	private Long idRegion;
	
	@Basic(optional = false)
	@Column(name = "description")
	private String description;
	 
	@Column(name = "iso_coun")
	private String isoCountry;
    
	public void parseDto(RegioniDto d) {
		if(d.getIsoCountry() != null)
			this.isoCountry= d.getIsoCountry();
		if(d.getName() != null)
			this.description= d.getName();
		if(d.getIdRegion() != null)
			this.idRegion= d.getIdRegion();

	}
}
