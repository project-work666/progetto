# Gestione Corsi

Web app per la gestione dei corsi interni a corvallis, backend sviluppato in Java (Springboot), frontend sviluppato in angular e completo di webserver, sviluppato in Java (Springboot) e database mysql su docker

### Come compilare

#### Backend e webserver frontend

###### Pre-requisiti:

- Java JDK
- Eclipse (Versione 2019-12 preferibilmente)
- Maven

Una volta clonato il progetto, aprire la cartella del progetto desiderato nel terminale (ad esempio progetto\BackEnd ) e utilizzare il seguente comando per compilare il codice

```
C:\Users\User\progetto\BackEnd> mvn clean package
```

All'interno della cartella target del progetto si troverà il file .jar appena compilato

#### Frontend

###### Pre-requisiti:

- NPM
- Angular CLI

Una volta clonato il progetto recarsi nella cartella del frontend e digitare:

```
C:\Users\User\progetto\FrontEnd> npm install
```

per installare tutti i pacchetti necessari.

Dopo di che basterà far partire lo script di build:

```
C:\Users\User\progetto\FrontEnd> npm run-script build
```

All'interno della cartella FrontEnd\dist si troveranno tutti i file compilati, e pronti per essere copiati/spostati all'interno della cartella _progetto\WebServer FrontEnd\src\main\resources\static_ per essere hostati dal webserver

### Console Args

#### Backend

per utilizzare ip diversi del db, porte diverse del db, username e password diverse e per avviare il server su una porta diversa da quelle di default
utilizzare i seguenti comandi all'avvio del file jar:

- -port=PORTA_DEL_SERVER

- -dbIp=INDIRIZZO_IP_DATABASE

- -dbPort=PORTA_DATABASE

- -dbUsername=USERNAME_DB

- -dbPassowrd=PASSWORD_DB

Esempio :

```
java jar BackEnd-0.0.1.jar -port=8093 -dbIp=8.8.8.8 -dbPort=1337 -dbUsername=pippo       -dbPassword=PassWord!
```
