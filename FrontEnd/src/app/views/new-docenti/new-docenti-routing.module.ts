import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NewDocentiComponent } from "./new-docenti.component";

const routes: Routes = [
  {
    path: "",
    component: NewDocentiComponent,
    data: {
      title: "Nuovo docente",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewDocentiRoutingModule {}
