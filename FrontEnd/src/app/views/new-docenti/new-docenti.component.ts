import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../api/core/services/api.service";
import { DataTableOptions } from "../../api/datatable-option";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-new-docenti",
  templateUrl: "./new-docenti.component.html",
  styleUrls: ["./new-docenti.component.scss"],
})
export class NewDocentiComponent implements OnInit {
  public newFromGroup: FormGroup;
  public subitIndicator = false;
  province: any;
  nazioni: any;
  localita: any;
  selectedDip: any;
  constructor(
    private api: ApiService,
    public fb: FormBuilder,
    public router: Router
  ) {}

  ngOnInit() {
    this.api.get("nazioni").subscribe((res) => {
      this.nazioni = res.response;

      this.api
        .get("province/nazione/" + this.nazioni[0].idNazione)
        .subscribe((resPro) => {
          this.province = resPro.response;
          this.api
            .get("citta/idProv/" + this.province[0].idProvincia)
            .subscribe((resLoc) => {
              this.localita = resLoc.response;
              console.log(this.localita);

              this.newFromGroup.controls.nazione.setValue(this.nazioni[0]);

              this.newFromGroup.controls.provincia.setValue(this.province[0]);

              this.newFromGroup.controls.localita.setValue(this.localita[0]);

              this.newFromGroup.controls.nazione.valueChanges.subscribe(
                (newV) => {
                  this.updateDropDownNazione(newV);
                }
              );
              this.newFromGroup.controls.provincia.valueChanges.subscribe(
                (newV) => {
                  this.updateDropDownProv(newV);
                }
              );
            });
        });
    });

    this.newFromGroup = this.fb.group({
      titolo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      ragione_sociale: new FormControl(
        {
          value: "",
          disabled: false,
        },
        Validators.required
      ),
      indirizzo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      localita: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      provincia: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      nazione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      telefono: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      fax: new FormControl({ value: "", disabled: false }, Validators.required),
      piva: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      referente: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
    });
  }

  subit() {
    this.subitIndicator = true;

    if (!this.newFromGroup.invalid) {
      this.api
        .post("docenti", {
          ...this.newFromGroup.value,
          ragioneSociale: this.newFromGroup.value.ragione_sociale,
          partita_iva: this.newFromGroup.value.piva,
          localita: this.newFromGroup.value.localita.idCity,
          provincia: this.newFromGroup.value.provincia.idProvincia,
          nazione: this.newFromGroup.value.nazione.idNazione,
        })
        .subscribe((res) => {
          this.router.navigateByUrl("docenti");
        });
    }
  }

  updateDropDownNazione(nazione: any) {
    this.api
      .get("province/nazione/" + nazione.idNazione)
      .subscribe((resPro) => {
        this.province = resPro.response;

        this.newFromGroup.controls.provincia.setValue(this.province[0]);
      });
  }

  updateDropDownProv(prov: any) {
    this.api
      .get(
        "citta/idProv/" + (prov.idProvincia == null ? prov : prov.idProvincia)
      )
      .subscribe((resLoc) => {
        this.localita = resLoc.response;
        console.log(this.localita);

        this.newFromGroup.controls.localita.setValue(this.localita[0]);
      });
  }
}
