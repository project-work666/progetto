import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NewDocentiComponent } from "./new-docenti.component";
import { NewDocentiRoutingModule } from "./new-docenti-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { DialogModule } from "primeng/dialog";
import { ButtonModule } from "primeng/button";

@NgModule({
  imports: [
    CommonModule,
    NewDocentiRoutingModule,
    SharedModule,
    DialogModule,
    ButtonModule,
  ],
  declarations: [NewDocentiComponent],
})
export class NewDocentiModule {}
