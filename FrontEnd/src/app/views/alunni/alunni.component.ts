import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../api/core/services/api.service";
import { Router } from "@angular/router";
import { AlunniService } from "../../api/core/services/alunni.service";
import { DataTableOptions } from "../../api/datatable-option";
import {
  trigger,
  style,
  state,
  transition,
  animate,
} from "@angular/animations";
import { ReplaceSource } from "webpack-sources";

@Component({
  selector: "app-alunni",
  templateUrl: "./alunni.component.html",
  styleUrls: ["./alunni.component.scss"],
  animations: [
    trigger("rowExpansionTrigger", [
      state(
        "void",
        style({
          transform: "translateX(-10%)",
          opacity: 0,
        })
      ),
      state(
        "active",
        style({
          transform: "translateX(0)",
          opacity: 1,
        })
      ),
      transition("* <=> *", animate("400ms cubic-bezier(0.86, 0, 0.07, 1)")),
    ]),
  ],
})
export class AlunniComponent implements OnInit {
  public options: DataTableOptions = {
    colsOptions: [
      { label: "Matricola", name: "matricola" },
      { label: "Nome", name: "nome" },
      { label: "Cognome", name: "cognome" },
      { label: "Data di nascita", name: "data_nascita" },
      { label: "Tipo di dipendente", name: "tipo_dipendente" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };
  public optionsCorsi: DataTableOptions = {
    colsOptions: [
      { label: "Tipo", name: "tipo" },
      { label: "Tipologia", name: "tipologia" },
      { label: "Descrizione", name: "descrizione" },
      { label: "Luogo", name: "luogo" },
    ],
  };
  public colonne: string[] = [
    "Impegno",
    "Logica",
    "Apprendimento",
    "Velocità",
    "Ordine e Precisione",
  ];
  public lista: any[];
  public tipoId = "dipendente_id";
  displayModal: boolean;
  listaModal: any;
  disableEdit = true;
  disableDelete = true;
  tr;
  paginatore;
  cols: any[];
  public voti: number[] = [0];
  public indiceDipendente: number;
  public expanded: boolean[];
  public mostraTabella: boolean[] = [false];
  public mostraScritta: boolean[] = [false];
  public frase: string;
  public appoggio: any[];
  public scrittaVoto: string[];
  public criteri: any[];
  public displayValutazione: boolean;
  public trCorsi;
  public listaCorsi: any[] = [0];
  public paginatoreCorsi;
  public tipi: any;
  public tipologie: any;
  public mostraValutazioni: boolean;
  titolo = "Alunni";
  public indiceDipValuta: number;
  public valore: number[] = [0, 0, 0, 0];
  public indiceCorso: number;
  public nomeCorso: string;

  constructor(
    public router: Router,
    private api: ApiService,
    public alunniService: AlunniService
  ) {}

  ngOnInit() {
    console.log(this.api);
    this.api.get("alunni").subscribe((resp) => {
      this.lista = resp.response;
      console.log("ALUNNI LISTA", this.lista);
    });

    this.cols = [
      { field: "tipo", header: "Tipo Corso" },
      { field: "media", header: "Media voti" },
    ];

    this.api.get("corsi").subscribe((resp) => {
      this.listaCorsi = resp.response;
    });

    this.api.get("tipo").subscribe((resp) => {
      this.tipi = resp.response;
    });

    this.api.get("tipologie").subscribe((resp) => {
      this.tipologie = resp.response;
    });
  }

  select(event) {
    if (event.column_name !== "operazioni") {
      this.alunniService.getById(event.item).subscribe((resp) => {
        this.listaModal = resp.response;
        console.log("LISTA", this.listaModal);
        this.displayModal = true;
      });
      this.getAppoggio(event.item);
      this.appoggio.splice(0, 1);
      this.expanded = [false];
      for (let i = 0; i < this.appoggio.length - 1; i++) {
        this.expanded.push(false);
        this.mostraTabella.push(false);
        this.mostraScritta.push(false);
      }
    }
  }

  getAppoggio(index) {
    this.indiceDipendente = index;
    this.appoggio = [9];
    let elenco;
    this.api.get("alunni/corsi/" + this.indiceDipendente).subscribe((resp) => {
      elenco = resp.response;
      console.log(
        "corsi dell'alunno con id " + this.indiceDipendente + " sono: ",
        resp.response
      );
      elenco.forEach((e) => {
        this.api.get("tipo/" + e.tipo).subscribe((re) => {
          this.api
            .get(
              "valutazioni/alunni/media/" +
                this.indiceDipendente +
                "_" +
                e.corsoId
            )
            .subscribe((ress) => {
              this.appoggio.push({
                indice: e.corsoId,
                descrizione: re.response.descrizione,
                media: ress.response,
              });
            });
        });
      });
    });
  }

  getExpanded(event) {
    this.api
      .get(
        "valutazioni/alunni/trova/" + this.indiceDipendente + "_" + event.indice
      )
      .subscribe((respo) => {
        console.log("val alunno: ", respo.response);
        if (respo.response != null) {
          this.criteri = respo.response;
          this.criteri.forEach((e) => {
            switch (e.valore) {
              case 1:
                e.valore = "Totalmente insufficiente ( " + e.valore + " )";
                break;
              case 2:
                e.valore = "Insufficiente ( " + e.valore + " )";
                break;
              case 3:
                e.valore = "Sufficiente ( " + e.valore + " )";
                break;
              case 4:
                e.valore = "Discreto ( " + e.valore + " )";
                break;
              case 5:
                e.valore = "Buono ( " + e.valore + " )";
                break;
              case 6:
                e.valore = "Ottimo ( " + e.valore + " )";
                break;
            }
          });
          this.mostraTabella[event.i] = true;
          this.mostraScritta[event.i] = false;
        } else {
          this.mostraScritta[event.i] = true;
          this.mostraTabella[event.i] = false;
          this.frase = "Nessuna valutazione è stata effettuata";
        }
      });
    this.expanded[event.i] = !this.expanded[event.i];
  }

  load(event) {
    this.paginatore = event;
    console.log("Load", event);
    this.updateLista();
  }

  updateLista() {
    let hasProp: boolean = false;
    for (var prop in this.paginatore.filters) {
      if (this.paginatore.filters.hasOwnProperty(prop)) {
        hasProp = true;
        break;
      }
    }
    let filtri = JSON.stringify(this.paginatore);
    let filterQuery: String = encodeURI("filters=" + filtri + "");

    this.api.get("alunni/getPagina" + "?" + filterQuery).subscribe((resp) => {
      let temp = resp.response;
      this.lista = [];
      for (let item of temp) {
        let dataStr: string = item.data_nascita;
        let dataStrArr = dataStr.split("-");
        let data = dataStrArr[2] + "/" + dataStrArr[1] + "/" + dataStrArr[0];
        delete item.data_nascita;
        this.lista.push({ data_nascita: data, ...item });
        console.log({ data_nascita: data, ...item });
      }
      this.api.get("alunni/total").subscribe((n) => {
        this.tr = n.response;
      });
    });
  }

  valuta(index) {
    this.indiceDipValuta = index;
    this.mostraValutazioni = false;
    this.displayValutazione = true;
    this.api.get("alunni/corsi/" + index).subscribe((resp) => {
      this.listaCorsi = resp.response;
      for (let corso of this.listaCorsi) {
        let x = this.tipi.find((tipo) => {
          return tipo.tipo_id == corso.tipo;
        });
        let y = this.tipologie.find((tipologia) => {
          return tipologia.tipologia_id == corso.tipologia;
        });
        corso.tipo = x.descrizione;
        corso.tipologia = y.descrizione;
        this.trCorsi = this.listaCorsi.length;
      }
      this.trCorsi = this.listaCorsi.length;
      console.log(this.listaCorsi);
    });
    for (let i = 0; i < this.colonne.length; i++) {
      this.voti[i] = 0;
    }
    this.colonne.forEach((e) => {});
  }

  indietro() {
    this.mostraValutazioni = false;
  }

  loadCorsi(event) {
    this.paginatoreCorsi = event;
    this.updateListaCorsi();
  }

  updateListaCorsi() {
    let filtri = JSON.stringify(this.paginatoreCorsi);
    let filterQuery: String = encodeURI("filters=" + filtri + "");
    this.api.get("corsi/getPagina" + "?" + filterQuery).subscribe((resp) => {
      for (let corso of this.listaCorsi) {
        let x = this.tipi.find((tipo) => {
          return tipo.tipo_id == corso.tipo;
        });
        let y = this.tipologie.find((tipologia) => {
          return tipologia.tipologia_id == corso.tipologia;
        });
        corso.tipo = x.descrizione;
        corso.tipologia = y.descrizione;
      }
      console.log(this.listaCorsi);
      this.api.get("corsi/total").subscribe((n) => {
        console.log(n);
        this.trCorsi = this.listaCorsi.length;
      });
    });
  }

  getValore(val: number, i: number){
    switch (val) {
      case 1:
        this.scrittaVoto[i] = "Totalmente insufficiente ( " + val + " )";
        break;
      case 2:
        this.scrittaVoto[i] = "Insufficiente ( " + val + " )";
        break;
      case 3:
        this.scrittaVoto[i] = "Sufficiente ( " + val + " )";
        break;
      case 4:
        this.scrittaVoto[i] = "Discreto ( " + val + " )";
        break;
      case 5:
        this.scrittaVoto[i] = "Buono ( " + val + " )";
        break;
      case 6:
        this.scrittaVoto[i] = "Ottimo ( " + val + " )";
        break;
    }
  }

  selectCorsi(event) {
    this.valore = [0, 0, 0, 0];
    this.scrittaVoto = ['','' , '', ''];
    this.indiceCorso = event.item;
    this.mostraValutazioni = true;
    let nome;
    this.api.get("valutazioni/alunni/trova/" + this.indiceDipValuta + "_" + this.indiceCorso).subscribe((resp) => {
      for (let i = 0; i < resp.response.length; i++) {
        this.valore[i] = resp.response[i].valore;
        this.getValore(resp.response[i].valore, i);
      }
    });
    this.api.get("corsi/id/" + event.item).subscribe((resp) => {
      nome = resp.response;
      let x = this.tipi.find((tipo) => {
        return tipo.tipo_id == nome.tipo;
      });
      let y = this.tipologie.find((tipologia) => {
        return tipologia.tipologia_id == nome.tipologia;
      });
      this.nomeCorso = x.descrizione + " ( tipologia:  " + y.descrizione + " )";
    });
  }

  handleRate(event) {
    this.voti[event.i] = event.value;
    this.getValore(event.value, event.i);
    console.log("voto: " + event.value);
    console.log("array: ", this.voti);
  }

  handleCancel(i) {
    this.voti[i] = 0;
    console.log("array con voto cancellato: ", this.voti);
  }

  controllaArrayVoti(): boolean {
    let t = true;
    for (let i = 0; i < this.voti.length; i++) {
      if (this.voti[i] == 0) t = false;
    }
    return t;
  }
  conferma() {
    if (this.controllaArrayVoti()) {
      this.api
        .get(
          "valutazioni/alunni/trova/" +
            this.indiceDipValuta +
            "_" +
            this.indiceCorso
        )
        .subscribe((resp) => {
          if (resp.response != null) {
            console.log("resp: ", resp.response);
            for (let i = 0; i < resp.response.length; i++) {
              this.api
                .put("valutazioni/alunni", {
                  valutazioni_utenti_id: resp.response[i].valutazioni_utenti_id,
                  criterio: this.colonne[i],
                  dipendente: this.indiceDipValuta,
                  valore: this.voti[i],
                  corso: this.indiceCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiornamento voti effettuato");
                  this.displayValutazione = false;
                });
            }
          } else {
            let i = 0;
            this.colonne.forEach((e) => {
              this.api
                .post("valutazioni/alunni", {
                  valutazioni_utenti_id: null,
                  criterio: e,
                  dipendente: this.indiceDipValuta,
                  valore: this.voti[i],
                  corso: this.indiceCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiunta effettuata");
                  this.displayValutazione = false;
                });
              i++;
            });
          }
        });
    }
  }
}
