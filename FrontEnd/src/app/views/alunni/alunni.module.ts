import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlunniComponent } from './alunni.component';
import { AlunniRoutingModule } from './alunni-routing.module';
import { DialogModule } from 'primeng/dialog';
import { SharedModule } from '../../shared/shared.module';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import {RatingModule} from 'primeng/rating';

@NgModule({
  declarations: [AlunniComponent],
  imports: [
    CommonModule, AlunniRoutingModule, SharedModule, DialogModule, TabViewModule, TableModule, RatingModule
  ]
})
export class AlunniModule { }
