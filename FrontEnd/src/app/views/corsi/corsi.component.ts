import { Component, OnInit } from "@angular/core";
import { DataTableOptions } from "../../api/datatable-option";
import { ApiService } from "../../api/core/services/api.service";
import { MessageService } from "primeng/api";
import { Router, NavigationExtras } from "@angular/router";

@Component({
  selector: "app-corsi",
  templateUrl: "./corsi.component.html",
  styleUrls: ["./corsi.component.scss"],
  providers: [MessageService],
})
export class CorsiComponent implements OnInit {
  public options: DataTableOptions = {
    colsOptions: [
      { label: "Tipo", name: "tipo" },
      { label: "Tipologia", name: "tipologia" },
      { label: "Descrizione", name: "descrizione" },
      { label: "Luogo", name: "luogo" },
      { label: "Ente", name: "ente" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };
  public corsi: any;
  public corsiPage = true;
  public tipi: any;
  public tipologie: any;
  public tipoId = "corsi_id";

  paginatore;
  tr;

  indice: number;
  titolo = "Corsi";

  constructor(
    public api: ApiService,
    private messageService: MessageService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.api.get("tipo").subscribe((resp) => {
      this.tipi = resp.response;
    });

    this.api.get("tipologie").subscribe((resp) => {
      this.tipologie = resp.response;
    });

    this.api.get("corsi").subscribe((resp) => {
      this.corsi = resp.response;
      for (let corso of this.corsi) {
        let x = this.tipi.find((tipo) => {
          return tipo.tipo_id == corso.tipo;
        });
        let y = this.tipologie.find((tipologia) => {
          return tipologia.tipologia_id == corso.tipologia;
        });
        corso.tipo = x.descrizione;
        corso.tipologia = y.descrizione;
      }
    });
  }

  edit(event: any) {
    let navEx: NavigationExtras = {
      state: { id: event },
    };
    this.router.navigateByUrl("corsi/edit", navEx);
  }

  valuta(event: any) {
    let navEx: NavigationExtras = {
      state: { id: event },
    };
    this.router.navigateByUrl("corsi/valutazione", navEx);
  }

  delete(index: number) {
    console.log("id corso", index);
    this.indice = index;
    this.messageService.clear();
    this.messageService.add({
      key: "c",
      sticky: true,
      severity: "warn",
      summary: "Sei sicuro?",
      detail: "Conferma per eliminare il corso",
    });
  }

  onConfirm() {
    this.messageService.clear("c");
    console.log("id corso", this.indice);
    this.api.delete("corsi/elimina/" + this.indice).subscribe((resp) => {
      this.api.get("corsi").subscribe((resp) => {
        this.corsi = resp.response;
        console.log(this.corsi);
        this.messageService.add({
          severity: "success",
          summary: "Successo",
          detail: "Corso eliminato",
        });
      });
    });
  }

  onReject() {
    this.messageService.clear("c");
    this.messageService.add({
      severity: "error",
      summary: "Errore",
      detail: "Eliminazione annullata",
    });
  }

  dettaglioCorsi(event) {
    if (event.column_name !== "operazioni") {
      let navEx: NavigationExtras = {
        state: { id: event.item },
      };
      this.router.navigateByUrl("corsi/dettaglio", navEx);
    }
  }

  nuovoCorso() {
    this.router.navigateByUrl("corsi/new");
  }

  load(event) {
    this.paginatore = event;
    this.updateLista();
  }

  updateLista() {
    let filtri = JSON.stringify(this.paginatore);
    let filterQuery: String = encodeURI("filters=" + filtri + "");
    this.api.get("corsi/getPagina" + "?" + filterQuery).subscribe((resp) => {
      this.corsi = resp.response;
      for (let corso of this.corsi) {
        let x = this.tipi.find((tipo) => {
          return tipo.tipo_id == corso.tipo;
        });
        let y = this.tipologie.find((tipologia) => {
          return tipologia.tipologia_id == corso.tipologia;
        });
        corso.tipo = x.descrizione;
        corso.tipologia = y.descrizione;
      }
      console.log(this.corsi);
      this.api.get("corsi/total").subscribe((n) => {
        console.log(n);
        this.tr = n.response;
      });
    });
  }
}
