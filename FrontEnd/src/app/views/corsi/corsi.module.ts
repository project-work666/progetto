import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CorsiComponent } from './corsi.component';
import { CorsiRoutingModule } from './corsi-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import {ToastModule} from 'primeng/toast';
import { SharedModule } from '../../shared/shared.module';



@NgModule({
  declarations: [CorsiComponent],
  imports: [
    CommonModule, 
    CorsiRoutingModule,
    DropdownModule,
    InputTextModule,
    CardModule,
    CarouselModule,
    ToastModule,
    SharedModule
  ]
})
export class CorsiModule { }
