import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { ApiService } from "../../api/core/services/api.service";
import { DataTableOptions } from "../../api/datatable-option";
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from "@angular/router";
import { AppSidebarNavTitleComponent } from "@coreui/angular/lib/sidebar/app-sidebar-nav/app-sidebar-nav-title.component";

@Component({
  selector: "app-edit-docenti",
  templateUrl: "./edit-docenti.component.html",
  styleUrls: ["./edit-docenti.component.scss"],
})
export class EditDocentiComponent implements OnInit {
  province: any;
  nazioni: any;
  localita: any;

  public newFromGroup: FormGroup;
  public subitIndicator = false;
  public id;
  selectedDip: any;
  constructor(
    private api: ApiService,
    public fb: FormBuilder,
    public router: Router
  ) {
    this.id = history.state.id;
    if (this.id) {
    } else {
      this.router.navigateByUrl("docenti");
    }
  }

  ngOnInit() {
    this.newFromGroup = this.fb.group({
      titolo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      ragione_sociale: new FormControl(
        {
          value: "",
          disabled: false,
        },
        Validators.required
      ),
      indirizzo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      localita: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      provincia: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      nazione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      telefono: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      fax: new FormControl({ value: "", disabled: false }, Validators.required),
      piva: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      referente: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
    });

    this.api.get("docenti/id/" + history.state.id).subscribe((res) => {
      let docente = res.response;
      delete docente.docente_id;

      let iva = docente.partita_iva;
      delete docente.partita_iva;

      let rag = docente.ragioneSociale;
      delete docente.ragioneSociale;

      delete docente.idDocente;
      delete docente.docenteId;
      console.log(docente);

      this.api.get("nazioni").subscribe((resNazio) => {
        this.nazioni = resNazio.response;
        this.api
          .get("province/nazione/" + res.response.nazione)
          .subscribe((resPro) => {
            this.province = resPro.response;
            this.api
              .get("citta/idProv/" + res.response.provincia)
              .subscribe((resLoc) => {
                this.localita = resLoc.response;
                this.newFromGroup.setValue({
                  ...docente,
                  piva: iva,
                  ragione_sociale: rag,
                });
                this.newFromGroup.controls.nazione.setValue(
                  this.nazioni.find((nazione) => {
                    return nazione.idNazione == docente.nazione;
                  })
                );
                this.newFromGroup.controls.provincia.setValue(
                  this.province.find((province) => {
                    return province.idProvincia == docente.provincia;
                  })
                );
                this.newFromGroup.controls.localita.setValue(
                  this.localita.find((localita) => {
                    return localita.idCity == docente.localita;
                  })
                );
                this.newFromGroup.controls.nazione.valueChanges.subscribe(
                  (newV) => {
                    this.updateDropDownNazione(newV);
                  }
                );
                this.newFromGroup.controls.provincia.valueChanges.subscribe(
                  (newV) => {
                    this.updateDropDownProv(newV);
                  }
                );
              });
          });
      });
    });
    this.newFromGroup.valueChanges.subscribe((newV) => {
      console.log(this.newFromGroup);
    });
  }

  subit() {
    this.subitIndicator = true;
    if (!this.newFromGroup.invalid) {
      this.api
        .put("/docenti", {
          docenteId: this.id,
          ...this.newFromGroup.value,
          localita: this.newFromGroup.value.localita.idCity,
          provincia: this.newFromGroup.value.provincia.idProvincia,
          nazione: this.newFromGroup.value.nazione.idNazione,
          partita_iva: this.newFromGroup.value.piva,
          ragioneSociale: this.newFromGroup.value.ragione_sociale,
        })
        .subscribe((res) => {
          this.router.navigateByUrl("docenti");
        });
    }
  }
  updateDropDownNazione(nazione: any) {
    this.api
      .get("province/nazione/" + nazione.idNazione)
      .subscribe((resPro) => {
        this.province = resPro.response;

        this.newFromGroup.controls.provincia.setValue(this.province[0]);
      });
  }

  updateDropDownProv(prov: any) {
    console.log("updateDropDownNazione");

    this.api
      .get(
        "citta/idProv/" + (prov.idProvincia == null ? prov : prov.idProvincia)
      )
      .subscribe((resLoc) => {
        this.localita = resLoc.response;
        console.log(this.localita);

        this.newFromGroup.controls.localita.setValue(this.localita[0]);
      });
  }
}
