import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EditDocentiComponent } from "./edit-docenti.component";

const routes: Routes = [
  {
    path: "",
    component: EditDocentiComponent,
    data: {
      title: "Modifica docente",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditDocentiRoutingModule {}
