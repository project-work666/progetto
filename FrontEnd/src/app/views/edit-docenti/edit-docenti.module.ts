import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EditDocentiComponent } from "./edit-docenti.component";
import { EditDocentiRoutingModule } from "./edit-docenti-routing.module";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  imports: [CommonModule, EditDocentiRoutingModule, SharedModule],
  declarations: [EditDocentiComponent],
})
export class EditDocentiModule {}
