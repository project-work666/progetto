import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { EditCorsiComponent } from './edit-corsi.component';
import { EditCorsiRoutingModule } from './edit-corsi-routing.module';



@NgModule({
  imports: [CommonModule, SharedModule, EditCorsiRoutingModule],
  declarations: [EditCorsiComponent],
})
export class EditCorsiModule { }
