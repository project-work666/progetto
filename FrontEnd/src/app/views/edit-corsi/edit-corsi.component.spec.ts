import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCorsiComponent } from './edit-corsi.component';

describe('EditCorsiComponent', () => {
  let component: EditCorsiComponent;
  let fixture: ComponentFixture<EditCorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
