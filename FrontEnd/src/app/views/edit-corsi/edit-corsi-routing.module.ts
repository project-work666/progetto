import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EditCorsiComponent } from "./edit-corsi.component";

const routes: Routes = [
  {
    path: "",
    component: EditCorsiComponent,
    data: {
      title: "Modifica corsi",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCorsiRoutingModule {}
