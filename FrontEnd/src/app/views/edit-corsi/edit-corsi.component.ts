import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { ApiService } from "../../api/core/services/api.service";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { AlunniService } from "../../api/core/services/alunni.service";
import { DataTableOptions } from "../../api/datatable-option";

@Component({
  selector: "app-edit-corsi",
  templateUrl: "./edit-corsi.component.html",
  styleUrls: ["./edit-corsi.component.scss"],
  providers: [MessageService],
})
export class EditCorsiComponent implements OnInit {
  public formEdit: FormGroup;
  public aggiungiForm: FormGroup;
  public corsiDocentiForm: FormGroup;
  public subitIndicator = false;
  public indice: number;
  public primoDelete: boolean = true;
  public cambio: boolean = true;
  public indiceDocente: any;
  public indiceDocenteVecchio: number;
  public id;
  public idCorso;
  public risolutore = true;
  public abe = true;
  public durata: number = 0;
  public posizione: number = -1;
  public nuovo = false;
  public internoBoo: boolean;
  public listaAlunni: any[];
  public arrayDelete: any[] = [2];
  public ragioneSociale: any;
  public options: DataTableOptions = {
    colsOptions: [
      { label: "Matricola", name: "matricola" },
      { label: "Nome", name: "nome" },
      { label: "Cognome", name: "cognome" },
      { label: "Durata", name: "durata" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };
  elencoTipi;
  elencoTipologie;
  elencoMisure;
  elencoStati;

  constructor(
    private api: ApiService,
    public fb: FormBuilder,
    public router: Router,
    private messageService: MessageService,
    private alunniService: AlunniService
  ) {
    this.id = history.state.id;
    if (this.id) {
    } else {
      this.router.navigateByUrl("corsi");
    }
  }

  ngOnInit(): void {
    this.formEdit = this.fb.group({
      tipo: new FormControl(
        {
          value: "",
          disabled: false,
        },
        Validators.required
      ),
      tipologia: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      durata: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      descrizione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      note: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      data_erogazione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      data_chiusura: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      edizione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      stato: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      misura: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      data_censimento: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      ente: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      luogo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      internoBo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
    });

    this.aggiungiForm = this.fb.group({
      durata: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
    });

    this.corsiDocentiForm = this.fb.group({
      internoBo: new FormControl(
        { value: false, disabled: false },
        Validators.required
      ),
      docente: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
    });

    this.api.get("corsidocenti/id/" + history.state.id).subscribe((resp) => {
      if (resp.response != null) {
        this.indiceDocenteVecchio = resp.response.docente;
        if (resp.response.interno) {
          this.alunniService.getById(resp.response.docente).subscribe((res) => {
            this.ragioneSociale = res.response.nome;
            this.ragioneSociale += " ";
            this.ragioneSociale += res.response.cognome;
            this.corsiDocentiForm.setValue({
              internoBo: true,
              docente: this.ragioneSociale,
            });
          });
        } else {
          this.api
            .get("docenti/id/" + resp.response.docente)
            .subscribe((respo) => {
              this.ragioneSociale = respo.response.ragioneSociale;
              this.corsiDocentiForm.setValue({
                internoBo: false,
                docente: this.ragioneSociale,
              });
            });
        }
      }
    });

    this.api.get("tipo").subscribe((resp) => {
      this.elencoTipi = resp.response;
      this.api.get("tipologie").subscribe((resp) => {
        this.elencoTipologie = resp.response;
        this.api.get("misure").subscribe((resp) => {
          this.elencoMisure = resp.response;
          this.elencoStati = [
            { label: "Previsto", value: "Previsto" },
            { label: "Erogato", value: "Erogato" },
          ];
          this.api.get("corsi/id/" + history.state.id).subscribe((res) => {
            let corso = res.response;

            let intBo;
            if (corso.interno == 0) intBo = false;
            else intBo = true;
            delete corso.interno;
            delete corso.previsto;
            delete corso.corso_id;
            delete corso.idCorso;
            delete corso.corsoId;
            let d_ero = new Date(corso.data_erogazione);
            let d_cen = new Date(corso.data_censimento);
            let d_chi = new Date(corso.data_chiusura);

            let tipo = this.elencoTipi.find((t) => {
              return t.tipo_id === corso.tipo;
            });

            let tipologia = this.elencoTipologie.find((t) => {
              return t.tipologia_id === corso.tipologia;
            });

            let misura = this.elencoMisure.find((t) => {
              return t.misura_id === corso.misura;
            });

            let sta = this.elencoStati[corso.erogato];
            delete corso.erogato;
            this.formEdit.setValue({
              ...corso,
              stato: sta,
              internoBo: intBo,
              data_erogazione: d_ero,
              data_censimento: d_cen,
              data_chiusura: d_chi,
              misura: misura,
              tipo: tipo,
              tipologia: tipologia,
            });
          });
        });
      });
    });
    this.idCorso = history.state.id;
    this.api.get("alunni/" + history.state.id).subscribe((resp) => {
      this.listaAlunni = resp.response;
    });
  }

  getCorsiDocenti() {
    let inte;
    if (this.corsiDocentiForm.value.internoBo) inte = 1;
    else inte = 0;
    if (this.indiceDocenteVecchio != null && this.indiceDocente != null) {
      this.api.get("corsidocenti/id/" + this.id).subscribe((res) => {
        this.api
          .put("corsidocenti", {
            corsi_docenti_id: res.response.corsi_docenti_id,
            ...this.corsiDocentiForm.value,
            interno: inte,
            docente: this.indiceDocente,
            corso: this.id,
          })
          .subscribe((res) => {
            console.log("docente aggiornato");
          });
      });
    } else if (
      this.indiceDocenteVecchio != null &&
      this.indiceDocente == null
    ) {
    } else {
      this.api
        .post("corsidocenti", {
          corsi_docenti_id: null,
          ...this.corsiDocentiForm.value,
          interno: inte,
          docente: this.indiceDocente,
          corso: this.id,
        })
        .subscribe((res) => {
          console.log("docente inserito");
        });
    }
  }

  getCorsi() {
    let pre;
    let ero;
    let censimentoStr =
      this.formEdit.controls.data_censimento.value.getFullYear() +
      "-" +
      (this.formEdit.controls.data_censimento.value.getMonth() + 1) +
      "-" +
      this.formEdit.controls.data_censimento.value.getDate();
    let erogazioneStr =
      this.formEdit.controls.data_erogazione.value.getFullYear() +
      "-" +
      (this.formEdit.controls.data_erogazione.value.getMonth() + 1) +
      "-" +
      this.formEdit.controls.data_erogazione.value.getDate();
    let chiusuraStr =
      this.formEdit.controls.data_chiusura.value.getFullYear() +
      "-" +
      (this.formEdit.controls.data_chiusura.value.getMonth() + 1) +
      "-" +
      this.formEdit.controls.data_chiusura.value.getDate();

    if (this.formEdit.value.stato.value == "Previsto") {
      pre = 1;
      ero = 0;
    } else {
      pre = 0;
      ero = 1;
    }
    let int;
    if (this.formEdit.value.internoBo) int = 1;
    else int = 0;
    this.api
      .put("corsi", {
        corsoId: this.id,
        ...this.formEdit.value,
        tipo: this.formEdit.value.tipo.tipo_id,
        tipologia: this.formEdit.value.tipologia.tipologia_id,
        misura: this.formEdit.value.misura.misura_id,
        previsto: pre,
        erogato: ero,
        interno: int,
        data_censimento: censimentoStr,
        data_erogazione: erogazioneStr,
        data_chiusura: chiusuraStr,
      })
      .subscribe((res) => {
        this.router.navigateByUrl("corsi");
      });
  }

  getCorsiAlunni() {
    if (this.listaAlunni != null) {
      this.listaAlunni.forEach((e) => {
        if (e.nuovo) {
          this.api
            .post("alunni", {
              corsi_utenti_id: null,
              durata: e.durata,
              corso: this.id,
              dipendente: e.dipendente_id,
            })
            .subscribe((res) => {
              console.log("alunni aggiunti al db");
            });
        }
        if (e.modificato) {
          this.alunniService
            .getByCorAlu(this.id, e.dipendente_id)
            .subscribe((resp) => {
              this.api
                .put("alunni", {
                  corsi_utenti_id: resp.response.corsi_utenti_id,
                  durata: e.durata,
                  corso: this.id,
                  dipendente: e.dipendente_id,
                })
                .subscribe((res) => {
                  console.log("alunno modificato nel db", e);
                });
            });
        }
      });
    }
    if (!this.primoDelete) {
      this.arrayDelete.forEach((ele) => {
        this.alunniService.getByCorAlu(this.id, ele).subscribe((resp) => {
          this.alunniService.deleteById(resp.response.corsi_utenti_id).subscribe((resp) => {
            console.log("alunno eliminato");
          });
        });
      });
    }
  }

  subit() {
    this.subitIndicator = true;
    if (
      !this.formEdit.invalid &&
      !this.corsiDocentiForm.invalid &&
      this.cambio
    ) {
      this.getCorsiDocenti();
      this.getCorsiAlunni();
      this.getCorsi();
    } else {
      console.log("sono uscito");
    }
  }

  delete(index: any) {
    this.indice = index;
    this.messageService.clear();
    this.messageService.add({
      key: "c",
      sticky: true,
      severity: "warn",
      summary: "Sei sicuro?",
      detail: "Conferma per rimuovere l'alunno",
    });
  }

  onConfirm() {
    this.risolutore = false;
    this.messageService.clear("c");
    this.arrayDelete.push(this.indice);
    if (this.primoDelete) {
      this.primoDelete = false;
      this.arrayDelete.splice(0, 1);
    }
    for (let i = 0; i < this.listaAlunni.length; i++) {
      if (this.listaAlunni[i].dipendente_id == this.indice) {
        if (this.listaAlunni[i].nuovo) {
          this.arrayDelete.pop();
          this.listaAlunni.splice(i, 1);
        }
        else{
          let t = false;
          this.listaAlunni.forEach(e => {
            if(e.nuovo) t=true;
          });
          if(!t){
            let cancellato = false;
            this.api.get("alunni/" + history.state.id).subscribe((res) => {
              this.listaAlunni = [2];
              res.response.forEach(e => {
                this.arrayDelete.forEach(d => {
                  if(e.dipendente_id == d) cancellato = true;
                });
                if(!cancellato){
                  this.listaAlunni.push(e);
                } 
                cancellato = false;
              });
              this.listaAlunni.splice(0, 1);
                for (let i = 0; i < this.listaAlunni.length; i++) {
                  this.api.get("alunni/trova/" + this.idCorso + "_" + this.listaAlunni[i].dipendente_id).subscribe((resp) => {
                    this.listaAlunni[i].durata = resp.response.durata;
                  });
                }
            });
          }
          else this.listaAlunni.splice(i, 1);
        }
        this.messageService.add({
          severity: "success",
          summary: "Successo",
          detail: "Alunno rimosso",
        });
      }
    }
  }

  onReject() {
    this.messageService.clear("c");
    this.messageService.add({
      severity: "error",
      summary: "Errore",
      detail: "Rimozione annullata",
    });
  }

  aggDoc(event) {
    this.indiceDocente = event.id;
    this.corsiDocentiForm.setValue({
      ...this.corsiDocentiForm.value,
      docente: event.ragSoc,
    });
    this.cambio = true;
  }


  modifica(index: number): boolean {
    let trovato = false;
    let j = 0;
    this.listaAlunni.forEach((e) => {
      if (e.dipendente_id == index) {
        this.posizione = j;
        if (e.nuovo) this.nuovo = true;
        trovato = true;
      }
      j++;
    });
    return trovato;
  }

  aggAlunno(index: any) {
    this.api.get("dipendenti/id/" + index).subscribe((resp) => {
      this.api.get("alunni/" + history.state.id).subscribe((res) => {
        if(this.risolutore){
          this.listaAlunni = res.response;
          for (let i = 0; i < this.listaAlunni.length; i++) {
            this.api.get("alunni/trova/" + this.idCorso + "_" + this.listaAlunni[i].dipendente_id).subscribe((resp) => {
              if (this.posizione == i) this.listaAlunni[i].durata = this.durata;
              else this.listaAlunni[i].durata = resp.response.durata;
            });
          }
          this.risolutore = false;
          this.abe = false;
        }
        if (this.modifica(index)) {
          this.listaAlunni[this.posizione] = {
            dipendente_id: index,
            matricola: resp.response.matricola,
            nome: resp.response.nome,
            cognome: resp.response.cognome,
            durata: this.aggiungiForm.value.durata,
            nuovo: this.nuovo,
            modificato: true,
          };
          this.durata =  this.listaAlunni[this.posizione].durata;
        } else {
          if (res.response != null) {
            if(this.abe){
              for (let i = 0; i < this.listaAlunni.length; i++) {
                this.api.get("alunni/trova/" + this.idCorso + "_" + this.listaAlunni[i].dipendente_id).subscribe((ele) => {
                  let cancellato = false;
                  res.response.forEach(e => {
                    this.arrayDelete.forEach(d => {
                      if(e.dipendente_id == d) cancellato = true;
                    });
                    if(!cancellato){
                      e.durata = ele.response.durata;
                    } 
                    cancellato = false;
                  });
                });
              }
            }
            this.abe=true;
            this.listaAlunni.push({
              dipendente_id: index,
              matricola: resp.response.matricola,
              nome: resp.response.nome,
              cognome: resp.response.cognome,
              durata: this.aggiungiForm.value.durata,
              nuovo: true,
            });
          } else if (this.listaAlunni != null) {
            this.listaAlunni.push({
              dipendente_id: index,
              matricola: resp.response.matricola,
              nome: resp.response.nome,
              cognome: resp.response.cognome,
              durata: this.aggiungiForm.value.durata,
              nuovo: true,
            });
          } else {
            this.listaAlunni = ["cancellare"];
            this.listaAlunni.push({
              dipendente_id: index,
              matricola: resp.response.matricola,
              nome: resp.response.nome,
              durata: this.aggiungiForm.value.durata,
              cognome: resp.response.cognome,
              nuovo: true,
            });
            this.listaAlunni.splice(0, 1);
          }
        }
      });
    });
  }

  annullaInput(event) {
    this.corsiDocentiForm.setValue({
      ...this.corsiDocentiForm.value,
      docente: null,
    });
    this.cambio = false;
  }
}
