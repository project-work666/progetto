import { Component, OnInit, Input } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "../../api/core/services/api.service";
import { DataTableOptions } from "../../api/datatable-option";
import { AlunniService } from "../../api/core/services/alunni.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-dettaglio-corsi",
  templateUrl: "./dettaglio-corsi.component.html",
  styleUrls: ["./dettaglio-corsi.component.scss"],
  providers: [MessageService],
})
export class DettaglioCorsiComponent implements OnInit {
  constructor(
    public fb: FormBuilder,
    public routeActive: ActivatedRoute,
    public api: ApiService,
    public router: Router,
    private alunniService: AlunniService
  ) {
    this.idCorso = history.state.id;
    if (this.idCorso) {
    } else {
      this.router.navigateByUrl("corsi");
    }
  }

  listaVoti: any[] = [];

  public formGroup: FormGroup;
  public corsiDocentiForm: FormGroup;
  public aggiungiForm: FormGroup = this.fb.group({
    durata: new FormControl({ value: "", disabled: true }),
  });
  public elencoTipi: any;
  public ragioneSociale: any;
  public nomeCorso: string;
  public nomeAlunni: string[];
  public criteriDoc: any[] = [0];
  public idCorso;
  public tipoCorso;
  public internoBoo: boolean;
  public valutazioneCorso: boolean;
  public valutazioneDocente: boolean;
  public mostraValutazioneDocente: boolean;
  public displayValutazione: boolean;
  public mostraValutazioni: boolean;
  public mostraValutazioniDoc: boolean;
  public mostraValutazioneCorso: boolean;
  public listaAlunni: any;
  public indiceAlunno: number;
  public valore: number[] = [0, 0, 0, 0];
  public scrittaVoto: string[];
  public indiceDocente: number;
  public criteri: any[];
  public voti: number[] = [0];
  public expanded: boolean[] = [false];
  public mostraTabella: boolean[] = [false];
  public mostraScritta: boolean[] = [false];
  public options: DataTableOptions = {
    colsOptions: [
      { label: "Matricola", name: "matricola" },
      { label: "Nome", name: "nome" },
      { label: "Cognome", name: "cognome" },
      { label: "Durata", name: "durata" },
      { label: "Valuta", name: "operazioni" },
    ],
  };
  public colonne: string[] = [
    "Impegno",
    "Logica",
    "Apprendimento",
    "Velocità",
    "Ordine e Precisione",
  ];
  public colonneDoc: string[] = [
    "Completezza",
    "Chiarezza nell'esposizione",
    "Disponibilità",
  ];

  valutazioni = [
    "",
    "Insufficente",
    "Sufficente  ",
    "Buono       ",
    "Molto buono ",
  ];

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      tipo: new FormControl({ value: "", disabled: true }, Validators.required),
      tipologia: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      durata: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      descrizione: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      note: new FormControl({ value: "", disabled: true }, Validators.required),
      data_erogazione: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      data_chiusura: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      edizione: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      stato: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      misura: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      data_censimento: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      ente: new FormControl({ value: "", disabled: true }, Validators.required),
      luogo: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      internoBo: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
    });

    this.corsiDocentiForm = this.fb.group({
      internoBo: new FormControl(
        { value: false, disabled: true },
        Validators.required
      ),
      docente: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
    });

    this.api.get("corsidocenti/id/" + this.idCorso).subscribe((resp) => {
      console.log("Corso Docente by corso ", resp.response);
      this.indiceDocente = resp.response.docente;
      if (resp.response != null) {
        if (resp.response.interno) {
          this.alunniService.getById(resp.response.docente).subscribe((res) => {
            this.ragioneSociale = res.response.nome;
            this.ragioneSociale += " ";
            this.ragioneSociale += res.response.cognome;
            this.corsiDocentiForm.setValue({
              internoBo: true,
              docente: this.ragioneSociale,
            });
          });
        } else {
          this.api
            .get("docenti/id/" + resp.response.docente)
            .subscribe((respo) => {
              console.log(respo.response);
              this.ragioneSociale = respo.response.ragioneSociale;
              this.corsiDocentiForm.setValue({
                internoBo: false,
                docente: this.ragioneSociale,
              });
            });
        }
      }
    });

    this.setIndiceDoc();

    this.getVoti();

    this.api.get("tipo").subscribe((resp) => {
      this.elencoTipi = resp.response;
    });

    this.api.get("corsi/id/" + this.idCorso).subscribe((resp) => {
      let corso = resp.response;
      this.getTipo(corso.tipo);
      let sta;
      if (corso.erogato == 0) sta = "Previsto";
      else sta = "Erogato";
      let intBo;
      if (corso.interno == 0) intBo = false;
      else intBo = true;
      let tgia = this.getTipologia(corso.tipologia);
      let tpo = this.tipoCorso;
      let mis = this.getMisura(corso.misura);
      this.nomeCorso = this.tipoCorso + " ( tipologia: " + tgia + " )";
      delete corso.interno;
      delete corso.erogato;
      delete corso.previsto;
      delete corso.tipologia;
      delete corso.misura;
      delete corso.tipo;
      delete corso.corso_id;
      delete corso.idCorso;
      delete corso.corsoId;
      console.log(corso);

      let data_iniStrArr = corso.data_erogazione.split("-");
      let data_fineStrArr = corso.data_chiusura.split("-");
      let data_cenStrArr = corso.data_censimento.split("-");
      let data_erogazione =
        data_iniStrArr[2] + "/" + data_iniStrArr[1] + "/" + data_iniStrArr[0];
      let data_chiusura =
        data_fineStrArr[2] +
        "/" +
        data_fineStrArr[1] +
        "/" +
        data_fineStrArr[0];
      let data_censimento =
        data_cenStrArr[2] + "/" + data_cenStrArr[1] + "/" + data_cenStrArr[0];

      this.formGroup.setValue({
        ...corso,
        misura: mis,
        tipo: tpo,
        stato: sta,
        internoBo: intBo,
        tipologia: tgia,
        data_erogazione: data_erogazione,
        data_chiusura: data_chiusura,
        data_censimento: data_censimento,
      });
    });

    this.api.get("alunni/" + this.idCorso).subscribe((resp) => {
      if(resp.response != null){
        this.listaAlunni = resp.response;
        this.setExpanded();
      }
    });
  }

  getTipo(tipo) {
    if (this.elencoTipi) {
      this.elencoTipi.forEach((e) => {
        if (e.tipo_id == tipo) {
          console.log("Sono qui" + e.descrizione);
          this.tipoCorso = e.descrizione;
        }
      });
    }
  }

  getTipologia(tipologia): String {
    if (tipologia == 1) return "Ingresso";
    else if (tipologia == 2) return "Aggiornamento";
    else return "Seminario";
  }

  getMisura(misura): String {
    if (misura == 1) return "Ore";
    else return "Griorni";
  }

  getValore(val: number, i: number) {
    switch (val) {
      case 1:
        this.scrittaVoto[i] = "Totalmente insufficiente ( " + val + " )";
        break;
      case 2:
        this.scrittaVoto[i] = "Insufficiente ( " + val + " )";
        break;
      case 3:
        this.scrittaVoto[i] = "Sufficiente ( " + val + " )";
        break;
      case 4:
        this.scrittaVoto[i] = "Discreto ( " + val + " )";
        break;
      case 5:
        this.scrittaVoto[i] = "Buono ( " + val + " )";
        break;
      case 6:
        this.scrittaVoto[i] = "Ottimo ( " + val + " )";
        break;
    }
  }

  valutaAlu(index) {
    this.valore = [0, 0, 0, 0];
    this.scrittaVoto = ["", "", "", ""];
    this.indiceAlunno = index;
    this.displayValutazione = true;
    this.mostraValutazioni = true;
    this.mostraValutazioniDoc = false;
    for (let i = 0; i < this.colonne.length; i++) {
      this.voti[i] = 0;
    }
    this.api
      .get("valutazioni/alunni/trova/" + this.indiceAlunno + "_" + this.idCorso)
      .subscribe((resp) => {
        if (resp.response != null) {
          for (let i = 0; i < resp.response.length; i++) {
            this.valore[i] = resp.response[i].valore;
            this.getValore(resp.response[i].valore, i);
          }
        }
      });
  }

  handleRate(event) {
    this.voti[event.i] = event.value;
    this.getValore(event.value, event.i);
    console.log("voto: " + event.value);
    console.log("array: ", this.voti);
  }

  handleCancel(i) {
    this.voti[i] = 0;
    console.log("array con voto cancellato: ", this.voti);
  }

  controllaArrayVoti(): boolean {
    let t = true;
    for (let i = 0; i < this.voti.length; i++) {
      if (this.voti[i] == 0) t = false;
    }
    return t;
  }
  conferma() {
    if (this.controllaArrayVoti()) {
      this.api
        .get(
          "valutazioni/alunni/trova/" + this.indiceAlunno + "_" + this.idCorso
        )
        .subscribe((resp) => {
          if (resp.response != null) {
            console.log("resp: ", resp.response);
            for (let i = 0; i < resp.response.length; i++) {
              this.api
                .put("valutazioni/alunni", {
                  valutazioni_utenti_id: resp.response[i].valutazioni_utenti_id,
                  criterio: this.colonne[i],
                  dipendente: this.indiceAlunno,
                  valore: this.voti[i],
                  corso: this.idCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiornamento voti effettuato");
                  this.displayValutazione = false;
                });
            }
          } else {
            let i = 0;
            this.colonne.forEach((e) => {
              this.api
                .post("valutazioni/alunni", {
                  valutazioni_utenti_id: null,
                  criterio: e,
                  dipendente: this.indiceAlunno,
                  valore: this.voti[i],
                  corso: this.idCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiunta effettuata");
                  this.displayValutazione = false;
                });
              i++;
            });
          }
        });
    }
    this.mostraValutazioni = false;
  }

  confermaDoc() {
    if (this.controllaArrayVoti()) {
      this.api
        .get(
          "valutazioni/docenti/trova/" + this.indiceDocente + "_" + this.idCorso
        )
        .subscribe((resp) => {
          if (resp.response != null) {
            console.log("resp: ", resp.response);
            for (let i = 0; i < resp.response.length; i++) {
              this.api
                .put("valutazioni/docenti", {
                  valutazioni_docenti_id:
                    resp.response[i].valutazioni_docenti_id,
                  criterio: this.colonneDoc[i],
                  docente: this.indiceDocente,
                  valore: this.voti[i],
                  corso: this.idCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiornamento voti effettuato");
                  this.displayValutazione = false;
                });
            }
          } else {
            let i = 0;
            this.colonneDoc.forEach((e) => {
              this.api
                .post("valutazioni/docenti", {
                  valutazioni_utenti_id: null,
                  criterio: e,
                  docente: this.indiceDocente,
                  valore: this.voti[i],
                  corso: this.idCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiunta effettuata");
                  this.displayValutazione = false;
                });
              i++;
            });
          }
        });
    }
    this.mostraValutazioniDoc = false;
  }

  nuovaMedia(media, index) {
    this.voti[index] = media;
  }

  convertiValore(val): string {
    let res;
    res =
      this.valutazioni[Math.round(val)] + (val !== 0 ? " (" + val + ")" : "");
    return res;
  }

  getVoti() {
    this.mostraValutazioneCorso = !this.mostraValutazioneCorso;
    if(this.mostraValutazioneCorso){
      this.api
      .get("valutazioni/corso/corso/" + history.state.id)
      .subscribe((resp) => {
        if(resp.response.length == 0) this.valutazioneCorso = false;
        else this.valutazioneCorso = true;
        for (let valutazione of resp.response) {
          valutazione.valore = this.convertiValore(valutazione.valore);
          this.listaVoti.push(valutazione);
        }
      });
    }
  }

  valutaDoc() {
    
    this.displayValutazione = true;
    this.mostraValutazioniDoc = true;
    this.mostraValutazioni = false;
    for (let i = 0; i < this.colonneDoc.length; i++) {
      this.voti[i] = 0;
    }
  }

  setIndiceDoc(){
    this.api.get("corsidocenti/id/" + history.state.id).subscribe((resp) => {
      if(resp.response != null) this.indiceDocente = resp.response.docente;
    });
  }
  getValoreDoc(val: any, i: number): string{
    if(val >= 1 && val < 1.5) return "Totalmente insufficiente ( " + val + " )"
    if(val >= 1.5 && val < 2.5) return "Insufficiente ( " + val + " )"
    if(val >= 2.5 && val < 3.5) return "Buono ( " + val + " )"
    if(val >= 3.5 && val <= 4) return "Molto buono ( " + val + " )"
  }

  espandiDocente(){
    this.mostraValutazioneDocente = !this.mostraValutazioneDocente;
    if(this.mostraValutazioneDocente){
      this.api.get("valutazioni/docenti/trova/" + this.indiceDocente + "_" + this.idCorso).subscribe((resp) => {
      if(resp.response == null) this.valutazioneDocente = false;
      else{
        this.valutazioneDocente = true;
        let i = 0;
        resp.response.forEach(e => {
          this.criteriDoc.push({criterio: e.criterio, valore: this.getValoreDoc(e.valore, i)});
          i++;
        });
        this.criteriDoc.splice(0,1);
      } 
    });
    }
  }

  setExpanded(){
    let j = 0;
    this.listaAlunni.forEach(e => {
      this.expanded[j] = false;
      this.mostraTabella[j] = false;
      this.mostraScritta[j] = false;
      j++;
    });
  }

  getExpanded(indice, i) {
    console.log("expanded", this.expanded);
    console.log("mostraTabella", this.mostraTabella);
    console.log("mostraScritta", this.mostraScritta);
    this.api
      .get(
        "valutazioni/alunni/trova/" + indice + "_" + this.idCorso
      )
      .subscribe((respo) => {
        console.log("val alunno: ", respo.response);
        if (respo.response != null) {
          this.criteri = respo.response;
          this.criteri.forEach((e) => {
            switch (e.valore) {
              case 1:
                e.valore = "Totalmente insufficiente ( " + e.valore + " )";
                break;
              case 2:
                e.valore = "Insufficiente ( " + e.valore + " )";
                break;
              case 3:
                e.valore = "Sufficiente ( " + e.valore + " )";
                break;
              case 4:
                e.valore = "Discreto ( " + e.valore + " )";
                break;
              case 5:
                e.valore = "Buono ( " + e.valore + " )";
                break;
              case 6:
                e.valore = "Ottimo ( " + e.valore + " )";
                break;
            }
          });
          this.mostraTabella[i] = true;
          this.mostraScritta[i] = false;
        } else {
          this.mostraScritta[i] = true;
          this.mostraTabella[i] = false;
        }
      });
    this.expanded[i] = !this.expanded[i];
  }
}
