import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DettaglioCorsiComponent } from "./dettaglio-corsi.component";
import { DettaglioCorsiRoutingModule } from "./dettaglio-corsi-routing.module";
import { DropdownModule } from "primeng/dropdown";
import { InputTextModule } from "primeng/inputtext";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { InputTextareaModule } from "primeng/inputtextarea";
import { SharedModule } from "../../shared/shared.module";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";

@NgModule({
  declarations: [DettaglioCorsiComponent],
  imports: [
    CommonModule,
    DettaglioCorsiRoutingModule,
    DropdownModule,
    InputTextModule,
    ReactiveFormsModule,
    FormsModule,
    InputTextareaModule,
    SharedModule,
    TabViewModule,
    DialogModule,
  ],
})
export class DettaglioCorsiModule {}
