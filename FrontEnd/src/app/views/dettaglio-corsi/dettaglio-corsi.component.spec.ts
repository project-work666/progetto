import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DettaglioCorsiComponent } from './dettaglio-corsi.component';

describe('DettaglioCorsiComponent', () => {
  let component: DettaglioCorsiComponent;
  let fixture: ComponentFixture<DettaglioCorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DettaglioCorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DettaglioCorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
