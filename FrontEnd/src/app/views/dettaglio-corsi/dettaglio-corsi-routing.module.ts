import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DettaglioCorsiComponent } from './dettaglio-corsi.component';

const routes: Routes = [
  {
    path: "",
    component: DettaglioCorsiComponent,
    data: {
      title: "Dettaglio Corso",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DettaglioCorsiRoutingModule {}
