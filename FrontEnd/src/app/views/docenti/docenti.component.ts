import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../api/core/services/api.service";
import { Router, NavigationExtras } from "@angular/router";
import { DocentiService } from "../../api/core/services/docenti.service";
import { DataTableOptions } from "../../api/datatable-option";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { MessageService } from "primeng/api";
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from "@angular/animations";
import { AlunniService } from "../../api/core/services/alunni.service";

@Component({
  selector: "app-docenti",
  templateUrl: "./docenti.component.html",
  styleUrls: ["./docenti.component.scss"],
  providers: [MessageService],
  animations: [
    trigger("rowExpansionTrigger", [
      state(
        "void",
        style({
          transform: "translateX(-10%)",
          opacity: 0,
        })
      ),
      state(
        "active",
        style({
          transform: "translateX(0)",
          opacity: 1,
        })
      ),
      transition("* <=> *", animate("400ms cubic-bezier(0.86, 0, 0.07, 1)")),
    ]),
  ],
})
export class DocentiComponent implements OnInit {
  public options: DataTableOptions = {
    colsOptions: [
      { label: "Ragione Sociale", name: "ragione_sociale" },
      { label: "Telefono", name: "telefono" },
      { label: "Titolo", name: "titolo" },
      { label: "Referente", name: "referente" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };

  public optionsInterni: DataTableOptions = {
    colsOptions: [
      { label: "Matricola", name: "matricola" },
      { label: "Nome", name: "nome" },
      { label: "Cognome", name: "cognome" },
      { label: "Data di nascita", name: "data_nascita" },
      { label: "Tipo di dipendente", name: "tipo_dipendente" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };

  province: any;
  nazioni: any;
  localita: any;

  public lista: any[];
  public listaInterni: any[];
  cols: any[];
  displayModal: boolean;
  displayModalInterni: boolean;
  listaModal: FormGroup;
  listaModalInterni: FormGroup;
  indice: any;
  public mostraNiente: boolean;
  public mostraScrittaNoCorsoVal: boolean = false;
  public expanded: boolean[];
  public expandedInterni: boolean[];
  public corsi: any[];
  public tipoId = "docente_id";
  public docentiPage = true;
  public indiceDocente: number;
  public mostraTabella: boolean[] = [false];
  public mostraScritta: boolean[] = [false];
  public frase: string = "Nessuna valutazione è stata effettuata";
  public appoggio: any[];
  public criteri: any[];
  public appoggioInterni: any[];
  public interno: number = 0;
  tr: number;
  trInterni: number;
  titolo = "Docenti esterni";
  paginatore;
  paginatoreInterni;

  tipi;
  tipologie;

  public displayValutazione: boolean;
  public trCorsi;
  public listaCorsi: any[] = [0];
  public voti: number[] = [0];
  public mostraValutazioni: boolean;
  public paginatoreCorsi;
  public indiceCorso: number;
  public nomeCorso: string;
  public indiceDipValuta: number;

  mostraTabCorsiEValutazioni = true;
  public colonne: string[] = [
    "Completezza",
    "Chiarezza nell'esposizione",
    "Disponibilità",
  ];

  public optionsCorsi: DataTableOptions = {
    colsOptions: [
      { label: "Tipo", name: "tipo" },
      { label: "Tipologia", name: "tipologia" },
      { label: "Descrizione", name: "descrizione" },
      { label: "Luogo", name: "luogo" },
    ],
  };

  constructor(
    public router: Router,
    private api: ApiService,
    public docenteService: DocentiService,
    public alunniService: AlunniService,
    public fb: FormBuilder,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.cols = [
      { field: "tipo", header: "Tipo Corso" },
      { field: "media", header: "Media voti" },
    ];
    this.api.get("tipo").subscribe((resp) => {
      this.tipi = resp.response;
    });

    this.api.get("tipologie").subscribe((resp) => {
      this.tipologie = resp.response;
    });
  }

  setInterno(n) {
    this.interno = n;
  }

  updateLista() {
    let hasProp: boolean = false;
    for (var prop in this.paginatore.filters) {
      if (this.paginatore.filters.hasOwnProperty(prop)) {
        hasProp = true;
        break;
      }
    }
    let filtri = JSON.stringify(this.paginatore.filters);
    let filterQuery: String = encodeURI("filters=" + filtri + "");
    this.api
      .get(
        "docenti/" +
          this.paginatore.first / this.paginatore.rows +
          "_" +
          this.paginatore.rows +
          "?" +
          (this.paginatore.sortField != null
            ? "sortField=" +
              this.paginatore.sortField +
              "&sortOrder=" +
              this.paginatore.sortOrder +
              "&"
            : "") +
          (hasProp ? filterQuery : "")
      )
      .subscribe((resp) => {
        this.lista = resp.response;
        this.api.get("docenti/total").subscribe((n) => {
          this.tr = n.response;
        });
      });
  }

  load(event) {
    console.log("OnLoad", event);
    this.paginatore = event;
    this.updateLista();
  }

  delete(index: any) {
    console.log("id docente", index);
    this.indice = index;
    this.messageService.clear();
    this.messageService.add({
      key: "c",
      sticky: true,
      severity: "warn",
      summary: "Sei sicuro?",
      detail: "Conferma per eliminare il docente",
    });
  }

  onConfirm() {
    this.messageService.clear("c");
    console.log("id docente", this.indice);
    this.docenteService.deleteById(this.indice).subscribe((resp) => {
      this.updateLista();
    });
    this.messageService.add({
      severity: "success",
      summary: "Successo",
      detail: "Docente eliminato",
    });
  }

  onReject() {
    this.messageService.clear("c");
    this.messageService.add({
      severity: "error",
      summary: "Errore",
      detail: "Eliminazione annullata",
    });
  }

  edit(event: any) {
    let navEx: NavigationExtras = {
      state: { id: event },
    };
    this.router.navigateByUrl("docenti/edit", navEx);
  }

  nuovoDoc() {
    this.router.navigateByUrl("docenti/new");
  }

  select(event) {
    if (event.column_name !== "operazioni") {
      this.docenteService.getById(event.item).subscribe((resp) => {
        this.api.get("nazioni/id/" + resp.response.nazione).subscribe((res) => {
          this.nazioni = res.response.name;
          console.log("Nazione", this.nazioni);
          this.api
            .get("province/id/" + resp.response.provincia)
            .subscribe((resPro) => {
              this.province = resPro.response.description;
              console.log("Provincia", this.province);
              this.api
                .get("citta/id/" + resp.response.localita)
                .subscribe((resLoc) => {
                  this.localita = resLoc.response.name;
                  console.log("LOC", this.localita);
                  this.listaModal = this.fb.group({
                    nome: new FormControl(
                      {
                        value: [resp.response.nome],
                        disabled: true,
                      },
                      Validators.required
                    ),
                    cognome: new FormControl(
                      { value: [resp.response.cognome], disabled: true },
                      Validators.required
                    ),
                    titolo: new FormControl(
                      { value: [resp.response.titolo], disabled: true },
                      Validators.required
                    ),
                    ragione_sociale: new FormControl(
                      {
                        value: [resp.response.ragioneSociale],
                        disabled: true,
                      },
                      Validators.required
                    ),
                    indirizzo: new FormControl(
                      { value: [resp.response.indirizzo], disabled: true },
                      Validators.required
                    ),
                    localita: new FormControl(
                      { value: [this.localita], disabled: true },
                      Validators.required
                    ),
                    provincia: new FormControl(
                      { value: [this.province], disabled: true },
                      Validators.required
                    ),
                    nazione: new FormControl(
                      { value: [this.nazioni], disabled: true },
                      Validators.required
                    ),
                    telefono: new FormControl(
                      { value: [resp.response.telefono], disabled: true },
                      Validators.required
                    ),
                    fax: new FormControl(
                      { value: [resp.response.fax], disabled: true },
                      Validators.required
                    ),
                    piva: new FormControl(
                      { value: [resp.response.partita_iva], disabled: true },
                      Validators.required
                    ),
                    referente: new FormControl(
                      { value: [resp.response.referente], disabled: true },
                      Validators.required
                    ),
                  });
                  this.displayModal = true;
                });
            });
        });
      });

      this.getAppoggio(event.item);
      this.appoggio.splice(0, 1);
      this.expanded = [false];
      for (let i = 0; i < this.appoggio.length - 1; i++) {
        this.expanded.push(false);
        this.mostraTabella.push(false);
        this.mostraScritta.push(false);
      }
    }
  }

  selectInterni(event) {
    if (event.column_name !== "operazioni") {
      this.alunniService.getById(event.item).subscribe((resp) => {
        this.listaModalInterni = resp.response;
        console.log("LISTA", this.listaModalInterni);
        this.displayModalInterni = true;
      });
      this.getAppoggio(event.item);
      this.appoggioInterni.splice(0, 1);
      this.expandedInterni = [false];
      for (let i = 0; i < this.appoggioInterni.length - 1; i++) {
        this.expandedInterni.push(false);
        this.mostraTabella.push(false);
        this.mostraScritta.push(false);
      }
      this.interno = 1;
    }
  }

  getAppoggio(index) {
    this.indiceDocente = index;
    this.appoggio = [9];
    this.appoggioInterni = [9];
    let elenco;
    this.api
      .get("corsidocenti/trovaByDoc/" + this.indiceDocente)
      .subscribe((resp) => {
        elenco = resp.response;
        if (resp.response != null) {
          this.mostraTabCorsiEValutazioni = true;
          elenco.forEach((e) => {
            this.api.get("corsi/id/" + e.corso).subscribe((res) => {
              this.api.get("tipo/" + res.response.tipo).subscribe((re) => {
                this.api
                  .get(
                    "valutazioni/docenti/media/" +
                      this.indiceDocente +
                      "_" +
                      e.corso
                  )
                  .subscribe((ress) => {
                    if (e.interno == 0 && this.interno == 0) {
                      this.appoggio.push({
                        indice: e.corso,
                        descrizione: re.response.descrizione,
                        media: ress.response,
                      });
                    } else if (e.interno == 1 && this.interno == 1) {
                      this.appoggioInterni.push({
                        indice: e.corso,
                        descrizione: re.response.descrizione,
                        media: ress.response,
                      });
                    }
                    if (this.appoggio.length == 0) {
                      this.mostraNiente = true;
                    } else this.mostraNiente = false;
                  });
              });
            });
          });
          if (elenco.length == 0) {
            this.mostraNiente = true;
          }
        }
      });
  }

  getExpanded(event) {
    this.api
      .get(
        "valutazioni/docenti/trova/" + this.indiceDocente + "_" + event.indice
      )
      .subscribe((respo) => {
        if (respo.response != null) {
          this.criteri = respo.response;
          this.criteri.forEach((e) => {
            if (e.valore >= 1 && e.valore < 2)
              e.valore = "Insufficiente ( " + e.valore + " )";
            else if (e.valore >= 2 && e.valore < 3)
              e.valore = "Sufficiente ( " + e.valore + " )";
            else if (e.valore >= 3 && e.valore < 4)
              e.valore = "Buono ( " + e.valore + " )";
            else if (e.valore >= 4 && e.valore <= 5)
              e.valore = "Molto buono ( " + e.valore + " )";
            else if (e.valore == 5) e.valore = "Ottimo ( " + e.valore + " )";
            else e.valore = "Non applicabile";
          });
          this.mostraTabella[event.i] = true;
          this.mostraScritta[event.i] = false;
        } else {
          this.mostraScritta[event.i] = true;
          this.mostraTabella[event.i] = false;
        }
      });
    if (this.interno == 0) this.expanded[event.i] = !this.expanded[event.i];
    else if (this.interno == 1)
      this.expandedInterni[event.i] = !this.expandedInterni[event.i];
  }

  updateListaInterni() {
    let filtri = JSON.stringify(this.paginatoreInterni.filters);
    let filterQuery: String = encodeURI("filters=" + filtri + "");
    this.api
      .get(
        "docenti/interni/" +
          this.paginatoreInterni.first / this.paginatoreInterni.rows +
          "_" +
          this.paginatoreInterni.rows +
          "?" +
          (this.paginatoreInterni.sortField != null
            ? "sortField=" +
              this.paginatoreInterni.sortField +
              "&sortOrder=" +
              this.paginatoreInterni.sortOrder +
              "&"
            : "") +
          filterQuery
      )
      .subscribe((resp) => {
        let temp = resp.response;
        this.listaInterni = [];
        for (let item of temp) {
          let dataStr: string = item.data_nascita;
          let dataStrArr = dataStr.split("-");
          let data = dataStrArr[2] + "/" + dataStrArr[1] + "/" + dataStrArr[0];
          delete item.data_nascita;
          this.listaInterni.push({ data_nascita: data, ...item });
          console.log({ data_nascita: data, ...item });
        }
        this.api.get("docenti/interni/total").subscribe((n) => {
          this.trInterni = n.response;
        });
      });
  }

  loadInterni(event) {
    console.log("OnLoad", event);
    this.paginatoreInterni = event;
    this.updateListaInterni();
  }

  selectCorsi(event) {
    this.indiceCorso = event.item;
    this.mostraValutazioni = true;
    let nome;
    console.log("id corso ", event);
    this.api.get("corsi/id/" + event.item).subscribe((resp) => {
      nome = resp.response;
      let x = this.tipi.find((tipo) => {
        return tipo.tipo_id == nome.tipo;
      });
      let y = this.tipologie.find((tipologia) => {
        return tipologia.tipologia_id == nome.tipologia;
      });
      this.nomeCorso = x.descrizione + " ( tipologia:  " + y.descrizione + " )";
    });
  }

  handleRate(event, i) {
    this.voti[i] = event.value;
    console.log("voto: " + event.value);
    console.log("array: ", this.voti);
  }

  handleCancel(event, i) {
    this.voti[i] = 0;
    console.log("array con voto cancellato: ", this.voti);
  }

  indietro() {
    this.mostraValutazioni = false;
  }
  controllaArrayVoti(): boolean {
    let t = true;
    for (let i = 0; i < this.voti.length; i++) {
      if (this.voti[i] == 0) t = false;
    }
    return t;
  }
  conferma() {
    if (this.controllaArrayVoti()) {
      console.log(
        "valutazioni/docenti/trova/" +
          this.indiceDipValuta +
          "_" +
          this.indiceCorso
      );
      this.api
        .get(
          "valutazioni/docenti/trova/" +
            this.indiceDipValuta +
            "_" +
            this.indiceCorso
        )
        .subscribe((resp) => {
          if (resp.response != null) {
            console.log("resp: ", resp.response);
            for (let i = 0; i < resp.response.length; i++) {
              this.api
                .put("valutazioni/docenti", {
                  valutazioni_docenti_id:
                    resp.response[i].valutazioni_docenti_id,
                  criterio: this.colonne[i],
                  docente: this.indiceDipValuta,
                  valore: this.voti[i],
                  corso: this.indiceCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiornamento voti effettuato");
                  this.displayValutazione = false;
                });
            }
          } else {
            let i = 0;
            this.colonne.forEach((e) => {
              this.api
                .post("valutazioni/docenti", {
                  valutazioni_utenti_id: null,
                  criterio: e,
                  docente: this.indiceDipValuta,
                  valore: this.voti[i],
                  corso: this.indiceCorso,
                })
                .subscribe((resp) => {
                  console.log("Aggiunta effettuata", resp);
                  this.displayValutazione = false;
                });
              i++;
            });
          }
        });
    }
  }

  valuta(index) {
    this.indiceDipValuta = index;
    this.mostraValutazioni = false;
    this.displayValutazione = true;
    this.api
      .get(
        (this.interno === 0
          ? "corsidocenti/trovaByDocEsterno/"
          : "corsidocenti/trovaByDocInterno/") + index
      )
      .subscribe((resp) => {
        this.listaCorsi = resp.response;
        for (let corso of this.listaCorsi) {
          let x = this.tipi.find((tipo) => {
            return tipo.tipo_id == corso.tipo;
          });
          let y = this.tipologie.find((tipologia) => {
            return tipologia.tipologia_id == corso.tipologia;
          });
          corso.tipo = x.descrizione;
          corso.tipologia = y.descrizione;
          this.trCorsi = this.listaCorsi.length;
        }
        if (resp.response.length == 0) this.mostraScrittaNoCorsoVal = true;
      });
    for (let i = 0; i < this.colonne.length; i++) {
      this.voti[i] = 0;
    }
    this.colonne.forEach((e) => {});
  }

  nuovaMedia(media, index) {
    this.voti[index] = media;
  }
}
