import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DocentiComponent } from "./docenti.component";
import { DocentiRoutingModule } from "./docenti-routing.module";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { SharedModule } from "../../shared/shared.module";
import { ToastModule } from "primeng/toast";
import { TableModule } from "primeng/table";
import { MessagesModule } from "primeng/messages";

import { RatingModule } from "primeng/rating";

@NgModule({
  imports: [
    CommonModule,
    DocentiRoutingModule,
    SharedModule,
    DialogModule,
    TabViewModule,
    ToastModule,
    TableModule,
    RatingModule,
    MessagesModule,
  ],
  declarations: [DocentiComponent],
})
export class DocentiModule {}
