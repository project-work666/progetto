import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../api/core/services/api.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-valutazione-corso",
  templateUrl: "./valutazione-corso.component.html",
  styleUrls: ["./valutazione-corso.component.scss"],
})
export class ValutazioneCorsoComponent implements OnInit {
  private id: any;
  public val: number;
  risultato1 = "Buono";
  public medie: number[] = [0, 0, 0, 0, 0];
  showError: boolean = false;

  public criteri: string[] = [
    "Utilità del corso per la crescita personale",
    "Utilità del corso in ambito lavorativo attuale",
    "Scelta argomenti specifici",
    "Livello di appofondimento degli argomenti specifici",
    "Durata del corso",
    "Corretta suddivisione tra teoria e pratica",
    "Efficacia delle esercitazioni",
    "Qualità del materiale didattico",
  ];

  constructor(
    private api: ApiService,
    public fb: FormBuilder,
    public router: Router
  ) {
    this.val = 0;
    this.id = history.state.id;
    console.log(this.id);
    if (this.id) {
    } else {
      this.router.navigateByUrl("corsi");
    }
  }

  handleChange(event) {
    console.log(event);
  }

  nuovaMedia(media, index) {
    this.medie[index] = media;
  }

  conferma() {
    let i = 0;
    let arr = [];
    for (let item of this.criteri) {
      arr.push({
        criterio: item,
        valutazione: this.medie[i],
      });
      i++;
    }
    let final = {
      corso: this.id,
      valutazioni: arr,
    };

    this.api.post("valutazioni/corso/batchInsert", final).subscribe((res) => {
      if (res.status === 200) {
        this.router.navigateByUrl("/corsi");
      } else {
        this.showError = true;
      }
    });
  }

  ngOnInit() {}
}
