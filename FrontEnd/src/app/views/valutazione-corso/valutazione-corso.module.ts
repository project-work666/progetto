import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ValutazioneCorsoComponent } from "./valutazione-corso.component";
import { ValutazioneCorsoRoutingModule } from "./new-docenti-routing.module";
import { SharedModule } from "../../shared/shared.module";

import { InputTextModule } from "primeng/inputtext";

@NgModule({
  imports: [
    CommonModule,
    ValutazioneCorsoRoutingModule,
    InputTextModule,
    SharedModule,
  ],
  declarations: [ValutazioneCorsoComponent],
})
export class ValutazioneCorsoModule {}
