import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ValutazioneCorsoComponent } from "./valutazione-corso.component";

const routes: Routes = [
  {
    path: "",
    component: ValutazioneCorsoComponent,
    data: {
      title: "Valuta il corso",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValutazioneCorsoRoutingModule {}
