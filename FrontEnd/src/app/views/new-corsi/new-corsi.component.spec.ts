import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCorsiComponent } from './new-corsi.component';

describe('NewCorsiComponent', () => {
  let component: NewCorsiComponent;
  let fixture: ComponentFixture<NewCorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
