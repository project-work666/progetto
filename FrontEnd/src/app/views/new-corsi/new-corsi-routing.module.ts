import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NewCorsiComponent } from "./new-corsi.component";

const routes: Routes = [
  {
    path: "",
    component: NewCorsiComponent,
    data: {
      title: "Nuovo corso",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewCorsiRoutingModule {}
