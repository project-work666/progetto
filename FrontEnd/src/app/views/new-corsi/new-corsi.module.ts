import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewCorsiComponent } from './new-corsi.component';
import { NewCorsiRoutingModule } from './new-corsi-routing.module';
import { SharedModule } from '../../shared/shared.module';



@NgModule({
  declarations: [NewCorsiComponent],
  imports: [
    CommonModule, SharedModule, NewCorsiRoutingModule
  ]
})
export class NewCorsiModule { }
