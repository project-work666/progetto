import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../api/core/services/api.service";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { DataTableOptions } from "../../api/datatable-option";
import { AlunniService } from "../../api/core/services/alunni.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-new-corsi",
  templateUrl: "./new-corsi.component.html",
  styleUrls: ["./new-corsi.component.scss"],
  providers: [MessageService],
})
export class NewCorsiComponent implements OnInit {
  public formNew: FormGroup;
  public aggiungiForm: FormGroup;
  public corsiDocentiForm: FormGroup;
  public subitIndicator = false;
  public internoBoo: boolean = false;
  public mostraPart: boolean = true;
  public primoIndex: boolean = true;
  public listaAlunni: any[];
  public indice: any;
  public indiceCorso: number;
  public indiceDocente: any;
  public totCorsi: any[];
  public options: DataTableOptions = {
    colsOptions: [
      { label: "Matricola", name: "matricola" },
      { label: "Nome", name: "nome" },
      { label: "Cognome", name: "cognome" },
      { label: "Durata", name: "durata" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };

  constructor(
    private api: ApiService,
    public fb: FormBuilder,
    public router: Router,
    private messageService: MessageService,
    private alunniService: AlunniService
  ) {}

  ngOnInit(): void {
    this.formNew = this.fb.group({
      tipo: new FormControl(
        {
          value: "",
          disabled: false,
        },
        Validators.required
      ),
      tipologia: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      durata: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      descrizione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      note: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      data_erogazione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      data_chiusura: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      edizione: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      stato: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      misura: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      data_censimento: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      ente: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      luogo: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
      internoBo: new FormControl(
        { value: false, disabled: false },
        Validators.required
      ),
    });

    this.aggiungiForm = this.fb.group({
      durata: new FormControl(
        { value: "", disabled: false },
        Validators.required
      ),
    });

    this.corsiDocentiForm = this.fb.group({
      internoBo: new FormControl(
        { value: false, disabled: false },
        Validators.required
      ),
      docente: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
    });
  }

  aggDoc(event) {
    this.indiceDocente = event.id;
    this.corsiDocentiForm.setValue({
      ...this.corsiDocentiForm.value,
      docente: event.ragSoc,
    });
  }

  public posizione: number = 0;

  modifica(index: number): boolean {
    let trovato = false;
    let j = 0;
    this.listaAlunni.forEach((e) => {
      if (e.dipendente_id == index) {
        console.log("trovato un alunno già in lista", e);
        this.posizione = j;
        trovato = true;
      }
      j++;
    });
    return trovato;
  }

  aggAlunno(index: any) {
    if (this.listaAlunni != null && this.listaAlunni.length == 0)
      this.primoIndex = true;
    if (this.primoIndex) this.listaAlunni = ["eliminaquesto"];
    this.api.get("dipendenti/id/" + index).subscribe((resp) => {
      if (this.modifica(index)) {
        console.log("sono dentro all'if di modifica");
        this.listaAlunni[this.posizione] = {
          dipendente_id: index,
          matricola: resp.response.matricola,
          nome: resp.response.nome,
          cognome: resp.response.cognome,
          durata: this.aggiungiForm.value.durata,
          modificato: true,
        };
      } else {
        this.listaAlunni.push({
          dipendente_id: index,
          matricola: resp.response.matricola,
          nome: resp.response.nome,
          cognome: resp.response.cognome,
          durata: this.aggiungiForm.value.durata,
        });
        if (this.primoIndex) {
          this.listaAlunni.splice(0, 1);
          this.primoIndex = false;
          console.log("lista togliedo il primo", this.listaAlunni);
        }
      }
    });
  }

  annullaInput(event) {
    this.corsiDocentiForm.setValue({
      ...this.corsiDocentiForm.value,
      docente: null,
    });
  }

  delete(index: any) {
    this.indice = index;
    this.messageService.clear();
    this.messageService.add({
      key: "c",
      sticky: true,
      severity: "warn",
      summary: "Sei sicuro?",
      detail: "Conferma per rimuovere l'alunno",
    });
  }

  onConfirm() {
    this.messageService.clear("c");
    for (let i = 0; i < this.listaAlunni.length; i++) {
      if (this.listaAlunni[i].dipendente_id == this.indice) {
        this.listaAlunni.splice(i, 1);
        console.log(this.listaAlunni);
        this.messageService.add({
          severity: "success",
          summary: "Successo",
          detail: "Alunno rimosso",
        });
      }
    }
  }

  onReject() {
    this.messageService.clear("c");
    this.messageService.add({
      severity: "error",
      summary: "Errore",
      detail: "Rimozione annullata",
    });
  }

  salva() {
    let data_censimento = new Date(this.formNew.controls.data_censimento.value);
    let data_erogazione = new Date(this.formNew.controls.data_erogazione.value);
    let data_chiusura = new Date(this.formNew.controls.data_chiusura.value);

    let censimentoStr =
      data_censimento.getFullYear() +
      "-" +
      (data_censimento.getMonth() + 1) +
      "-" +
      data_censimento.getDate();
    let erogazioneStr =
      data_erogazione.getFullYear() +
      "-" +
      (data_erogazione.getMonth() + 1) +
      "-" +
      data_erogazione.getDate();
    let chiusuraStr =
      data_chiusura.getFullYear() +
      "-" +
      (data_chiusura.getMonth() + 1) +
      "-" +
      data_chiusura.getDate();

    console.log("CORSO");
    if (!this.formNew.invalid) {
      let pre;
      let ero;
      if (this.formNew.value.stato.value == "Previsto") {
        pre = 1;
        ero = 0;
      } else {
        pre = 0;
        ero = 1;
      }
      let int;
      if (this.formNew.value.interno) int = 1;
      else int = 0;

      this.api
        .post("corsi", {
          ...this.formNew.value,
          tipo: this.formNew.value.tipo.tipo_id,
          tipologia: this.formNew.value.tipologia.tipologia_id,
          misura: this.formNew.value.misura.misura_id,
          previsto: pre,
          erogato: ero,
          interno: int,
          data_chiusura: chiusuraStr,
          data_censimento: censimentoStr,
          data_erogazione: erogazioneStr,
        })
        .subscribe((res) => {
          this.api.get("corsi").subscribe((resp) => {
            this.totCorsi = resp.response;
            this.getUltimoId();
          });
        });
      this.mostraPart = false;
    }
  }

  getUltimoId() {
    this.indiceCorso = this.totCorsi[this.totCorsi.length - 1].corsi_id;
    console.log("indice nuovo corso creato ora " + this.indiceCorso);
  }

  getCorsiDocenti() {
    if (this.indiceDocente != null) {
      let inte;
      if (this.corsiDocentiForm.value.internoBo) inte = 1;
      else inte = 0;
      this.api
        .post("corsidocenti", {
          ...this.corsiDocentiForm.value,
          interno: inte,
          docente: this.indiceDocente,
          corso: this.indiceCorso,
        })
        .subscribe((res) => {
          console.log("docente inserito");
        });
    }
  }

  getCorsi() {
    let pre;
    let ero;
    if (this.formNew.value.stato.value == "Previsto") {
      pre = 1;
      ero = 0;
    } else {
      pre = 0;
      ero = 1;
    }
    let int;
    if (this.formNew.value.interno) int = 1;
    else int = 0;
    this.api
      .put("corsi", {
        corsi_id: this.indiceCorso,
        ...this.formNew.value,
        tipo: this.formNew.value.tipo.tipo_id,
        tipologia: this.formNew.value.tipologia.tipologia_id,
        misura: this.formNew.value.misura.misura_id,
        previsto: pre,
        erogato: ero,
        interno: int,
      })
      .subscribe((res) => {
        this.router.navigateByUrl("corsi");
      });
  }

  getCorsiAlunni() {
    if (!this.primoIndex) {
      this.listaAlunni.forEach((e) => {
        this.api
          .post("alunni", {
            corsi_utenti_id: null,
            durata: e.durata,
            corso: this.indiceCorso,
            dipendente: e.dipendente_id,
          })
          .subscribe((res) => {
            console.log("alunni aggiunti al db", e);
          });
      });
    }
  }

  subit() {
    this.subitIndicator = true;
    if (!this.formNew.invalid && !this.corsiDocentiForm.invalid) {
      this.getCorsiDocenti();
      this.getCorsiAlunni();
      this.getCorsi();
    } else console.log("Sono uscito");
  }
}
