/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { InputDocentiComponent } from './input-docenti.component';

describe('InputDocentiComponent', () => {
  let component: InputDocentiComponent;
  let fixture: ComponentFixture<InputDocentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputDocentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDocentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
