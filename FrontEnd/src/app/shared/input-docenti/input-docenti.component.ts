import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { ApiService } from "../../api/core/services/api.service";

@Component({
  selector: "app-input-docenti",
  templateUrl: "./input-docenti.component.html",
  styleUrls: ["./input-docenti.component.scss"],
})
export class InputDocentiComponent implements OnInit {
  public formgroup: FormGroup;

  @Input()
  public submit: boolean;

  @Input()
  public paginaDettaglio: boolean;

  @Input()
  public parentForm: any;

  @Input()
  public disableButton: boolean;

  @Output()
  submitForm: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  nazioni: any;

  @Input()
  provincia: any;

  @Input()
  localita: any;

  constructor(private api: ApiService) {}

  selecedNazione = {
    iso: null,
    description: null,
  };
  ngOnInit() {}

  conferma() {
    this.submitForm.emit();
  }
}
