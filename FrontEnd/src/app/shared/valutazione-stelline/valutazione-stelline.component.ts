import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataTableOptions } from '../../api/datatable-option';
import { ApiService } from '../../api/core/services/api.service';

@Component({
  selector: 'app-valutazione-stelline',
  templateUrl: './valutazione-stelline.component.html',
  styleUrls: ['./valutazione-stelline.component.scss']
})
export class ValutazioneStellineComponent implements OnInit {
  @Input()
  public colonne: string[];

  @Input()
  public mostraValutazioni: boolean;

  @Input()
  public paginaDettCorso: boolean;

  @Input()
  public nomeCorso: string;

  @Input()
  public valore: number[];

  @Input()
  public scrittaVoto: string[];

  @Output()
  onConferma: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onIndietro: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onHandleRate: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onHandleCancel: EventEmitter<any> = new EventEmitter<any>();

  constructor(private api: ApiService,) { }

  ngOnInit(): void {}

  indietro() {
    this.onIndietro.emit();
  }

  handleRate(event, i) {
    this.onHandleRate.emit({ value: event.value, i: i });
  }

  handleCancel(event, i) {
    this.onHandleCancel.emit(i);
  }

  conferma() {
    this.onConferma.emit();
  }
}
