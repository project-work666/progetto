import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValutazioneStellineComponent } from './valutazione-stelline.component';

describe('ValutazioneStellineComponent', () => {
  let component: ValutazioneStellineComponent;
  let fixture: ComponentFixture<ValutazioneStellineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValutazioneStellineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValutazioneStellineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
