import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { DataTableOptions } from "../../api/datatable-option";
import { Router } from "@angular/router";

@Component({
  selector: "app-datatable",
  templateUrl: "./datatable.component.html",
  styleUrls: ["./datatable.component.scss"],
})
export class DatatableComponent implements OnInit {
  @Input()
  public lista: any[];

  @Input()
  public totalRecords: number;

  @Input()
  public options: DataTableOptions;

  @Input()
  public secondToLastDisableSearch: Boolean;

  @Input()
  public buttonText: String;

  @Input()
  public title: String;

  @Input()
  public testo_mancante: String;

  @Input()
  public tipoId: String;

  @Input()
  public disableButtons: Boolean;

  @Input()
  public disableValuta: Boolean;

  @Input()
  public disableEdit: Boolean;

  @Input()
  public disableDelete: Boolean;

  @Input()
  public disableTopButton: Boolean;

  @Input()
  public showAggiungi: Boolean;

  @Input()
  public showAggiungiDoc: Boolean;

  @Output()
  onSelect: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onEdit: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onAgg: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onDelete: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onLoad: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onPlus: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onValuta: EventEmitter<any> = new EventEmitter<any>();

  public currentIndex: boolean[] = [false];
  selectedLista: any[];
  constructor(public router: Router) {}

  ngOnInit() {
    console.log(this.lista);
    console.log(this.options.colsOptions);
  }

  delete(index: number) {
    this.onDelete.emit(index);
  }
  edit(index: number) {
    this.onEdit.emit(index);
  }

  valuta(index: number) {
    this.onValuta.emit(index);
  }

  select(item: any, col: any) {
    this.onSelect.emit({ item: item, column_name: col.name });
  }

  aggiungi(index: number) {
    this.onAgg.emit(index);
  }

  btnClick() {
    this.onPlus.emit();
  }

  loadLazy(event: any) {
    this.onLoad.emit(event);
  }
}
