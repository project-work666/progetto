import { Component, OnInit, Input } from "@angular/core";
import { AnonymousSubject } from "rxjs/internal/Subject";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-dettaglio-alunni",
  templateUrl: "./dettaglio-alunni.component.html",
  styleUrls: ["./dettaglio-alunni.component.scss"],
})
export class DettaglioAlunniComponent implements OnInit {
  @Input()
  listaAlunno: any;

  constructor(public fb: FormBuilder) {}

  public formGroup: FormGroup;

  ngOnInit(): void {
    let dataStrArr = this.listaAlunno.dataNascita.split("-");

    let data = dataStrArr[2] + "/" + dataStrArr[1] + "/" + dataStrArr[0];

    this.formGroup = this.fb.group({
      matricola: new FormControl(
        {
          value: this.listaAlunno.matricola,
          disabled: true,
        },
        Validators.required
      ),
      nome: new FormControl(
        {
          value: this.listaAlunno.nome,
          disabled: true,
        },
        Validators.required
      ),
      cognome: new FormControl(
        {
          value: this.listaAlunno.cognome,
          disabled: true,
        },
        Validators.required
      ),
      sesso: new FormControl(
        {
          value: this.listaAlunno.sesso,
          disabled: true,
        },
        Validators.required
      ),
      data_nascita: new FormControl(
        {
          value: data,
          disabled: true,
        },
        Validators.required
      ),
      luogo_nascita: new FormControl(
        {
          value: this.listaAlunno.luogoNascita,
          disabled: true,
        },
        Validators.required
      ),
      stato_civile: new FormControl(
        {
          value: this.listaAlunno.statoCivile,
          disabled: true,
        },
        Validators.required
      ),
      titolo_studio: new FormControl(
        {
          value: this.listaAlunno.titoloStudio,
          disabled: true,
        },
        Validators.required
      ),
      conseguito_presso: new FormControl(
        {
          value: this.listaAlunno.conseguitoPresso,
          disabled: true,
        },
        Validators.required
      ),
      anno_conseguimento: new FormControl(
        {
          value: this.listaAlunno.annoConseguimento,
          disabled: true,
        },
        Validators.required
      ),
      tipo_dipendente: new FormControl(
        {
          value: this.listaAlunno.tipoDipendente,
          disabled: true,
        },
        Validators.required
      ),
      qualifica: new FormControl(
        {
          value: this.listaAlunno.qualifica,
          disabled: true,
        },
        Validators.required
      ),
      livello: new FormControl(
        {
          value: this.listaAlunno.livello,
          disabled: true,
        },
        Validators.required
      ),
    });
  }
}
