import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DettaglioAlunniComponent } from './dettaglio-alunni.component';

describe('DettaglioAlunniComponent', () => {
  let component: DettaglioAlunniComponent;
  let fixture: ComponentFixture<DettaglioAlunniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DettaglioAlunniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DettaglioAlunniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
