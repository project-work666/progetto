import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputCorsiComponent } from './input-corsi.component';

describe('InputCorsiComponent', () => {
  let component: InputCorsiComponent;
  let fixture: ComponentFixture<InputCorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputCorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputCorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
