import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { DataTableOptions } from "../../api/datatable-option";
import { ApiService } from "../../api/core/services/api.service";
import { AlunniService } from "../../api/core/services/alunni.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-input-corsi",
  templateUrl: "./input-corsi.component.html",
  styleUrls: ["./input-corsi.component.scss"],
})
export class InputCorsiComponent implements OnInit {
  @Input()
  public submit: boolean;

  @Input()
  public parentForm: FormGroup;

  @Input()
  public corsiDocentiForm: any;

  @Input()
  public testoBottone: string;

  @Input()
  public aggiungiForm: any;

  @Input()
  public disableButton: boolean;

  @Input()
  public disableEdit: boolean;

  @Input()
  public disableTopButton: boolean;

  @Input()
  public disableDelete: boolean;

  @Input()
  public disableValuta: boolean;

  @Input()
  public internoBoolean: boolean;

  @Input()
  public dettaglioCorsoPage: boolean;

  @Input()
  public mostraPart: boolean;

  @Input()
  public idCorso: number;

  @Input()
  public listaAlunni: any[];

  @Input()
  public options: DataTableOptions;

  @Output()
  onElimina: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onConferma: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onSeleziona: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onSalva: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onSelezionaDoc: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onRifiuta: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onAnnullaInput: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  submitForm: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onValutaDoc: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onValutaAlu: EventEmitter<any> = new EventEmitter<any>();

  public elencoTipi: any;
  public elencoTipologie: any;
  public elencoStati: any;
  public elencoMisure: any;
  public tipoCorrente: String;
  public nome: String;
  public tipoId: String;
  public cognome: String;
  public displayModal: boolean;
  public displayDoc: boolean;
  public internoBooleanDoc: boolean = false;
  public showConferma: boolean;
  public indiceAlunno: any;
  public optionsDocente: DataTableOptions;
  public listaAllDoc: any[];
  public listaAll: any[];
  public ragSocialeDoc: any;
  public optionsSelezionaAlunno: DataTableOptions = {
    colsOptions: [
      { label: "Matricola", name: "matricola" },
      { label: "Nome", name: "nome" },
      { label: "Cognome", name: "cognome" },
      { label: "Operazioni", name: "operazioni" },
    ],
  };

  private paginatore: any;
  public tr: any;

  private paginatoreAll: any;
  public trAll: any;

  constructor(public api: ApiService, public alunniService: AlunniService) {}

  ngOnInit() {
    this.api.get("tipo").subscribe((resp) => {
      this.elencoTipi = resp.response;
    });
    this.api.get("tipologie").subscribe((resp) => {
      this.elencoTipologie = resp.response;
    });
    this.api.get("misure").subscribe((resp) => {
      this.elencoMisure = resp.response;
    });
    this.elencoStati = [
      { label: "Previsto", value: "Previsto" },
      { label: "Erogato", value: "Erogato" },
    ];

    this.api.get("dipendenti").subscribe((resp) => {
      this.listaAll = resp.response;
    });
  }

  test(event) {
    console.log("test", this.parentForm.controls.tipo.value);
  }

  salva() {
    this.onSalva.emit();
  }

  conferma() {
    this.submitForm.emit("ok");
  }

  delete(index: any) {
    this.onElimina.emit(index);
    this.api.get("alunni/tota/" + this.idCorso).subscribe((n) => {
      this.tr = n.response;
    });
  }

  select(index: any) {
    if (!this.aggiungiForm.invalid) {
      this.onSeleziona.emit(index);
      this.displayModal = false;
    }
  }

  valutaDoc() {
    this.onValutaDoc.emit();
  }

  valutaAlu(index: number) {
    this.onValutaAlu.emit(index);
  }

  showAggiunta() {
    this.displayModal = true;
  }

  selectDoc(index: any) {
    if (this.corsiDocentiForm.value.internoBo) {
      this.alunniService.getById(index).subscribe((resp) => {
        this.ragSocialeDoc = resp.response.nome;
        this.ragSocialeDoc += " ";
        this.ragSocialeDoc += resp.response.cognome;
        this.onSelezionaDoc.emit({ ragSoc: this.ragSocialeDoc, id: index });
      });
    } else {
      this.api.get("docenti/id/" + index).subscribe((resp) => {
        this.ragSocialeDoc = resp.response.ragioneSociale;
        console.log("indice docente esterno ", index);
        this.onSelezionaDoc.emit({ ragSoc: this.ragSocialeDoc, id: index });
      });
    }
    this.displayDoc = false;
  }

  showDocente() {
    if (this.corsiDocentiForm.value.internoBo) {
      this.tipoId = "dipendente_id";
      this.optionsDocente = this.optionsSelezionaAlunno;
      this.api.get("dipendenti").subscribe((resp) => {
        this.listaAllDoc = resp.response;
      });
    } else {
      this.tipoId = "docente_id";
      this.optionsDocente = {
        colsOptions: [
          { label: "Ragione Sociale", name: "ragione_sociale" },
          { label: "Telefono", name: "telefono" },
          { label: "Operazioni", name: "operazioni" },
        ],
      };
      this.api.get("docenti").subscribe((resp) => {
        this.listaAllDoc = resp.response;
      });
    }
    this.displayDoc = true;
  }

  onConfirm() {
    this.onConferma.emit();
  }

  onReject() {
    this.onRifiuta.emit();
  }

  load(event) {
    this.paginatore = event;
    this.updateLista();
  }

  updateLista() {
    let hasProp: boolean = false;
    for (var prop in this.paginatore.filters) {
      if (this.paginatore.filters.hasOwnProperty(prop)) {
        hasProp = true;
        break;
      }
    }
    let filtri = JSON.stringify(this.paginatore);
    let filterQuery: String = encodeURI("filters=" + filtri + "");

    this.api
      .get("alunni/" + (this.idCorso ? this.idCorso : -1) + "?" + filterQuery)
      .subscribe((resp) => {
        if(resp.response != null){
          this.listaAlunni = resp.response;
          for (let i = 0; i < this.listaAlunni.length; i++) {
            this.api
              .get(
                "alunni/trova/" +
                  this.idCorso +
                  "_" +
                  this.listaAlunni[i].dipendente_id
              )
              .subscribe((resp) => {
                this.listaAlunni[i].durata = resp.response.durata;
              });
          }
          if (this.listaAlunni.length > 0) {
            this.api.get("alunni/tota/" + this.idCorso).subscribe((n) => {
              this.tr = n.response;
            });
          } else {
            this.tr = this.listaAlunni.length;
          }
        }
        
      });
  }

  updateListaDoc() {
    if (this.corsiDocentiForm.value.internoBo) {
      let filtri = JSON.stringify(this.paginatore);
      let filterQuery: String = encodeURI("filters=" + filtri + "");

      this.api
        .get("dipendenti/getPaginato" + "?" + filterQuery)
        .subscribe((resp) => {
          this.listaAllDoc = resp.response;
          console.log(this.listaAllDoc);
          this.api.get("dipendenti/total").subscribe((n) => {
            this.tr = n.response;
          });
        });
    } else {
      let hasProp: boolean = false;
      for (var prop in this.paginatore.filters) {
        if (this.paginatore.filters.hasOwnProperty(prop)) {
          hasProp = true;
          break;
        }
      }
      let filtri = JSON.stringify(this.paginatore.filters);
      let filterQuery: String = encodeURI("filters=" + filtri + "");

      this.api
        .get(
          "docenti/" +
            this.paginatore.first / this.paginatore.rows +
            "_" +
            this.paginatore.rows +
            "?" +
            (this.paginatore.sortField != null
              ? "sortField=" +
                this.paginatore.sortField +
                "&sortOrder=" +
                this.paginatore.sortOrder +
                "&"
              : "") +
            (hasProp ? filterQuery : "")
        )
        .subscribe((resp) => {
          this.listaAllDoc = resp.response;
          this.api.get("docenti/total").subscribe((n) => {
            this.tr = n.response;
          });
        });
    }
  }

  loadDoc(event) {
    console.log("OnLoad", event);
    this.paginatore = event;
    this.updateListaDoc();
  }

  annulloInput(event) {
    this.onAnnullaInput.emit(event);
  }

  loadAll(event) {
    this.paginatoreAll = event;
    this.updateListaAll();
  }

  updateListaAll() {
    let filtri = JSON.stringify(this.paginatoreAll);
    let filterQuery: String = encodeURI("filters=" + filtri + "");

    this.api
      .get("dipendenti/getPaginato" + "?" + filterQuery)
      .subscribe((resp) => {
        this.listaAll = resp.response;
        console.log("AAAA", this.listaAll);
        this.api.get("dipendenti/total").subscribe((n) => {
          this.trAll = n.response;
        });
      });
  }
}
