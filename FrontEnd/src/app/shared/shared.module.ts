import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DatatableComponent } from "./datatable/datatable.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { TableModule } from "primeng/table";
import { InputTextModule } from "primeng/inputtext";
import { CheckboxModule } from "primeng/checkbox";
import { RadioButtonModule } from "primeng/radiobutton";
import { InputDocentiComponent } from "./input-docenti/input-docenti.component";
import { ButtonModule } from "primeng/button";
import { DropdownModule } from "primeng/dropdown";
import { DettaglioAlunniComponent } from "./dettaglio-alunni/dettaglio-alunni.component";
import { TabViewModule } from "primeng/tabview";
import { CalendarModule } from "primeng/calendar";
import { InputCorsiComponent } from "./input-corsi/input-corsi.component";
import { ToastModule } from "primeng/toast";
import { InputMaskModule } from "primeng/inputmask";
import { InputSwitchModule } from "primeng/inputswitch";
import { DialogModule } from "primeng/dialog";
import { SpinnerModule } from "primeng/spinner";
import { RatingModule } from 'primeng/rating';
import { InputValutazioneComponent } from "./input-valutazione/input-valutazione.component";
import { TabCorsiValComponent } from "./tab-corsi-val/tab-corsi-val.component";
import { ValutazioneStellineComponent } from './valutazione-stelline/valutazione-stelline.component';
@NgModule({
  declarations: [
    DatatableComponent,
    InputDocentiComponent,
    DettaglioAlunniComponent,
    InputCorsiComponent,
    InputValutazioneComponent,
    TabCorsiValComponent,
    ValutazioneStellineComponent,
  ],
  exports: [
    DatatableComponent,
    InputDocentiComponent,
    DettaglioAlunniComponent,
    InputCorsiComponent,
    InputValutazioneComponent,
    TabCorsiValComponent,
    ValutazioneStellineComponent,
  ],
  imports: [
    SpinnerModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TableModule,
    InputTextModule,
    CheckboxModule,
    RadioButtonModule,
    ButtonModule,
    DropdownModule,
    TabViewModule,
    CalendarModule,
    ToastModule,
    InputMaskModule,
    InputSwitchModule,
    DialogModule,
    RatingModule
  ],
})
export class SharedModule {}
