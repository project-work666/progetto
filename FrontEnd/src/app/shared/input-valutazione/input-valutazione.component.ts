import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-input-valutazione",
  templateUrl: "./input-valutazione.component.html",
  styleUrls: ["./input-valutazione.component.scss"],
})
export class InputValutazioneComponent implements OnInit {
  risultato = "";

  valutazioni = ["", "Insufficente", "Sufficente", "Buono", "Molto buono"];

  @Input()
  public titolo: string;

  @Output()
  onChangeMedia: EventEmitter<any> = new EventEmitter<any>();

  public media = 0;

  public form: FormGroup;

  constructor(public fb: FormBuilder) {
    this.form = fb.group({
      insufficente: [0],
      sufficente: [0],
      buono: [0],
      moltoBuono: [0],
    });
  }

  ngOnInit() {}

  handleChange(event) {
    console.log(event);
    let nInsuff = this.form.controls.insufficente.value;
    let nSuff = this.form.controls.sufficente.value;
    let nBuono = this.form.controls.buono.value;
    let nMoltoBuono = this.form.controls.moltoBuono.value;

    let insufficente = 0;
    if (nInsuff > 0) insufficente = nInsuff;
    let sufficente = 0;
    if (nSuff > 0) sufficente = nSuff * 2;
    let buono = 0;
    if (nBuono > 0) buono = nBuono * 3;
    let moltoBuono = 0;
    if (nMoltoBuono > 0) moltoBuono = nMoltoBuono * 4;

    let sommaVoti = insufficente + sufficente + buono + moltoBuono;
    let sommaQuantiVoti = nInsuff + nSuff + nBuono + nMoltoBuono;

    this.media = sommaVoti / sommaQuantiVoti;

    this.media = Math.round(this.media * 100) / 100;

    if (Number.isNaN(this.media)) {
      this.media = 0;
    }
    this.onChangeMedia.emit(this.media + "");
    this.risultato =
      this.valutazioni[Math.round(this.media)] +
      (this.media !== 0 ? "(" + this.media + ")" : "");
  }
}
