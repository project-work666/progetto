import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabCorsiValComponent } from './tab-corsi-val.component';

describe('TabCorsiValComponent', () => {
  let component: TabCorsiValComponent;
  let fixture: ComponentFixture<TabCorsiValComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabCorsiValComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabCorsiValComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
