import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tab-corsi-val',
  templateUrl: './tab-corsi-val.component.html',
  styleUrls: ['./tab-corsi-val.component.scss']
})
export class TabCorsiValComponent implements OnInit {

  @Input()
  public frase: string;

  @Input()
  public mostraNiente: boolean;
  
  @Input()
  public expanded: boolean[];

  @Input()
  public mostraTabella: boolean[];

  @Input()
  public mostraScritta: boolean[];

  @Input()
  public appoggio: any[];

  @Input()
  public criteri: any[];

  @Output()
  onGetExpanded: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  getExpanded(indice, i){
    this.onGetExpanded.emit({indice: indice, i: i});
  }

}
