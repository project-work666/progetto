import { TestBed } from '@angular/core/testing';
import { DocentiService } from './docenti.service';


describe('DocentiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocentiService = TestBed.get(DocentiService);
    expect(service).toBeTruthy();
  });
});
