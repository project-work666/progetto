import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';


@Injectable({
  providedIn: 'root'
})
export class AlunniService {
  private readonly path = 'alunni';
  private readonly path2 = 'dipendenti';

  public listaSoggetti:any[] = [  ];

  constructor(private api:ApiService) { }
  
  public getAll():Observable<any>{ 
    return this.api.get(this.path);
  }
  public getById(id:string):Observable<any>{
    return this.api.get(this.path2 + '/id/' + id);
  }
  public getByCorAlu(id:any, id2: any):Observable<any>{
    return this.api.get(this.path + '/trova/' + id + "_" + id2);
  }
  public deleteById(id:string):Observable<any>{
    return this.api.delete(this.path +'/elimina/' + id);
  }

}