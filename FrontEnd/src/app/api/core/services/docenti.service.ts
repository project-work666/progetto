import { Injectable } from '@angular/core';
import * as uuid from 'uuid';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class DocentiService {
  private readonly path = 'docenti';

  public listaSoggetti:any[] = [  ];

  constructor(private api:ApiService) { }
  
  public getAll():Observable<any>{ 
    return this.api.get(this.path);
  }
  public getById(id:string):Observable<any>{
    return this.api.get(this.path + '/id/' + id);
  }
  public deleteById(id:string):Observable<any>{
    return this.api.delete(this.path +'/elimina'+'/' + id);
    }

}
