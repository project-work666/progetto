import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
  {
    name: "Corsi",
    url: "/corsi",
    icon: "icon-bell", //bell envelope
    children: [
      {
        name: "Lista Corsi",
        url: "/corsi",
        icon: "icon-bell",
      },
      {
        name: "Nuovo corso",
        url: "/corsi/new",
        icon: "icon-plus",
      },
    ],
  },
  {
    name: "Docenti",
    url: "/docenti",
    icon: "icon-user",
    children: [
      {
        name: "Lista docenti",
        url: "/docenti",
        icon: "icon-user",
      },
      {
        name: "Nuovo docente esterno",
        url: "/docenti/new",
        icon: "icon-plus",
      },
    ],
  },
  {
    name: "Alunni",
    url: "/dipendenti",
    icon: "icon-user",
  },
];
