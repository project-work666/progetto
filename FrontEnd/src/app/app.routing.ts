import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Import Containers
import { DefaultLayoutComponent } from "./containers";

import { P404Component } from "./views/error/404.component";
import { P500Component } from "./views/error/500.component";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "docenti",
    pathMatch: "full",
  },
  {
    path: "404",
    component: P404Component,
    data: {
      title: "Page 404",
    },
  },
  {
    path: "500",
    component: P500Component,
    data: {
      title: "Page 500",
    },
  },

  {
    path: "",
    component: DefaultLayoutComponent,
    data: {
      title: "Home",
    },
    children: [
      {
        path: "corsi",
        loadChildren: () =>
          import("./views/corsi/corsi.module").then((m) => m.CorsiModule),
      },
      {
        path: "docenti",
        loadChildren: () =>
          import("./views/docenti/docenti.module").then((m) => m.DocentiModule),
      },
      {
        path: "dipendenti",
        loadChildren: () =>
          import("./views/alunni/alunni.module").then((m) => m.AlunniModule),
      },
      {
        path: "docenti/new",
        loadChildren: () =>
          import("./views/new-docenti/new-docenti.module").then(
            (m) => m.NewDocentiModule
          ),
      },
      {
        path: "corsi/new",
        loadChildren: () =>
          import("./views/new-corsi/new-corsi.module").then(
            (m) => m.NewCorsiModule
          ),
      },
      {
        path: "corsi/dettaglio",
        loadChildren: () =>
          import("./views/dettaglio-corsi/dettaglio-corsi.module").then(
            (m) => m.DettaglioCorsiModule
          ),
      },
      {
        path: "docenti/edit",
        loadChildren: () =>
          import("./views/edit-docenti/edit-docenti.module").then(
            (m) => m.EditDocentiModule
          ),
      },
      {
        path: "corsi/edit",
        loadChildren: () =>
          import("./views/edit-corsi/edit-corsi.module").then(
            (m) => m.EditCorsiModule
          ),
      },
      {
        path: "corsi/valutazione",
        loadChildren: () =>
          import("./views/valutazione-corso/valutazione-corso.module").then(
            (m) => m.ValutazioneCorsoModule
          ),
      },
    ],
  },
  { path: "**", component: P404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
