import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { MessageService } from "primeng/api";

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(private messageService: MessageService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!request.headers.has("Content-Type")) {
      request = request.clone({
        headers: request.headers.set("Content-Type", "application/json"),
      });
    }
    request = request.clone({
      headers: request.headers.set("Accept", "application/json"),
    });

    return next.handle(request).pipe(
      catchError((error) => {
        let titolo;
        let descirzione;

        console.log("/n/n/n Grave errore", error);
        if (error.status === 0) {
          titolo = "Impossibile contattare il server";
          descirzione = "Controlla di essere connesso ad internet";
        } else {
          titolo = "Errore";
          descirzione = "C'è stato un errore";
        }

        this.messageService.add({
          key: "generalError",
          severity: "error",
          summary: titolo,
          detail: descirzione,
          life: 30000,
        });

        //console.log("error is intercept", error);
        return throwError(error.message);
      })
    );
  }
}
