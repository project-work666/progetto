-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: projectwork
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `citta`
--

DROP TABLE IF EXISTS `citta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `citta` (
  `id_city` int(7) NOT NULL,
  `description` varchar(20) NOT NULL,
  `id_prov` varchar(10) NOT NULL,
  PRIMARY KEY (`id_city`),
  KEY `id_prov` (`id_prov`),
  CONSTRAINT `citta_ibfk_1` FOREIGN KEY (`id_prov`) REFERENCES `province` (`id_prov`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `citta`
--

LOCK TABLES `citta` WRITE;
/*!40000 ALTER TABLE `citta` DISABLE KEYS */;
INSERT INTO `citta` VALUES (1000,'Padova','PD'),(1001,'Cittadella','PD'),(1002,'Camposampiero','PD'),(1003,'Campodarsego','PD'),(35000,'Rennes','FR-35'),(35001,'Saint-Malo','FR-35'),(35002,'Vitré','FR-35'),(35003,'Fougères','FR-35'),(35004,'Bruz','FR-35'),(35043,'Monselice','PD'),(36100,'Vicenza','VI'),(36101,'Bassano delGrappa','VI'),(36102,'Arzignano','VI'),(36103,'Laghi','VI'),(37000,'Verona','VR'),(37001,'Belfiore','VR'),(37002,'Garda','VR'),(37003,'Nogara','VR'),(37004,'Vigasio','VR'),(75800,'Palestine','48-001'),(80121,'Napoli','NA');
/*!40000 ALTER TABLE `citta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corsi`
--

DROP TABLE IF EXISTS `corsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `corsi` (
  `corso_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `tipologia` int(11) NOT NULL,
  `descrizione` varchar(100) NOT NULL,
  `edizione` varchar(100) NOT NULL,
  `previsto` int(11) NOT NULL,
  `erogato` int(11) NOT NULL,
  `durata` int(11) NOT NULL,
  `misura` int(11) NOT NULL,
  `note` varchar(100) NOT NULL,
  `luogo` varchar(100) NOT NULL,
  `ente` varchar(100) NOT NULL,
  `data_erogazione` date NOT NULL,
  `data_chiusura` date NOT NULL,
  `data_censimento` date NOT NULL,
  `interno` int(11) NOT NULL,
  PRIMARY KEY (`corso_id`),
  KEY `tipo` (`tipo`),
  KEY `tipologia` (`tipologia`),
  KEY `misura` (`misura`),
  CONSTRAINT `corsi_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipi` (`tipo_id`) ON UPDATE CASCADE,
  CONSTRAINT `corsi_ibfk_2` FOREIGN KEY (`tipologia`) REFERENCES `tipologie` (`tipologia_id`) ON UPDATE CASCADE,
  CONSTRAINT `corsi_ibfk_3` FOREIGN KEY (`misura`) REFERENCES `misure` (`misura_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corsi`
--

LOCK TABLES `corsi` WRITE;
/*!40000 ALTER TABLE `corsi` DISABLE KEYS */;
INSERT INTO `corsi` VALUES (1,5,1,'Corso fine java ','1',1,0,38,1,'Varie note d\'esempio','Crovallis ufficio','Corvallis sede','2010-12-01','2021-01-08','2020-12-01',1),(2,4,1,'Corso2 edit','1',0,1,38,2,'Varie note d\'esempio','Crovallis ufficiiiiiii','Corvallis','2020-12-01','2021-01-08','2020-12-01',1),(3,1,3,'Corso 3','1',1,0,38,1,'Varie note d\'esempio','Crovallis uffici','Corvallis','2020-12-01','2021-01-08','2020-12-01',0),(5,1,1,'Corso su come fare java','1',1,0,38,1,'Varie note d\'esempio','Crovallis uffici','Corvallis','2020-12-01','2021-01-08','2020-12-01',0),(6,1,2,'Corso 2','1',1,0,38,1,'Varie note d\'esempio','Crovallis uffici','Corvallis','2020-12-20','2021-01-08','2020-12-01',0),(8,3,3,'descrizione nuovo corso','3',1,0,10,1,'prima nota nuovo corso','Padova','Corvallis','2000-12-20','2001-01-15','2000-12-05',1),(9,16,2,'Descrizione corso nuovo','5',1,0,34,2,'Note corso nuovo','Ciao','interno e 0','2000-02-02','2000-03-03','2000-04-04',0),(14,20,1,'Corso nuovo con docenti','3',0,1,33,1,'Note interessanti','Londra','Corvallis','2020-02-15','2020-03-20','2020-02-09',0),(15,22,3,'Proviamo gestione','1',0,1,13,2,'Proviamo gestione alunni e docenti','Roma','Corvallis','2020-03-20','2020-04-20','2020-03-03',1),(17,4,3,'corso per conoscenze basilari dell\'argomento','2',1,0,12,1,'nessuna nota','Londra','Corvallis','2000-04-04','2000-03-03','2010-02-02',0),(18,2,2,'corso intensivo','5',1,0,3,1,'nessuna nota','Napoli','Corvallis','2000-05-05','2000-05-05','2000-05-05',0),(21,5,2,'conoscenze basi','2',0,1,2,2,'nessuna nota','Vicenza','Corvallis','2000-02-02','2000-02-02','2000-02-02',0),(22,5,2,'corso per conoscenze basilari dell\'argomento','2',1,0,1,1,'nessuna nota','Verona','Corvallis','2000-02-02','2000-01-01','2000-10-10',0),(24,2,3,'corso per conoscenze basilari dell\'argomento','4',1,0,3,1,'nessuna nota','Napoli','Corvallis','2000-10-10','2000-10-10','2000-10-10',0),(25,4,1,'corso per conoscenze basilari dell\'argomento','6',0,1,3,1,'nessuna nota','Verona','Corvallis','1200-10-10','1010-10-10','1010-10-10',0),(26,4,1,'nuovo corso','3',0,1,3,1,'nessuna nota ancora','sss','Corvallis','2020-05-06','2020-05-06','2020-05-03',0),(28,2,3,'nuovo corso','5',1,0,45,2,'nessuna nota ora','Padova','Corvallis','2020-05-19','2020-05-27','2020-05-11',0),(29,5,1,'Corso inizio java ','1',1,0,38,1,'Varie note d\'esempio','Crovallis ufficio','Corvallis sede','2010-12-01','2021-01-08','2020-12-01',1),(30,4,1,'Corso10','1',0,1,38,2,'Varie note d\'esempio','Crovallis ufficiiiiiii','Corvallis','2020-12-01','2021-01-08','2020-12-01',1),(31,1,3,'Corso 31','1',1,0,38,1,'Varie note d\'esempio','Crovallis uffici','Corvallis','2020-12-01','2021-01-08','2020-12-01',0),(32,1,1,'Corso su come fare java','1',1,0,38,1,'Varie note d\'esempio','Crovallis uffici','Corvallis','2020-12-01','2021-01-08','2020-12-01',0),(33,1,2,'Corso 22','1',1,0,38,1,'Varie note d\'esempio','Crovallis uffici','Corvallis','2020-12-20','2021-01-08','2020-12-01',0),(34,3,3,'descrizione nuovo corso','3',1,0,10,1,'prima nota nuovo corso','Padova','Corvallis','2000-12-20','2001-01-15','2000-12-05',1),(35,16,2,'Descrizione corso nuovo','5',1,0,34,2,'Note corso nuovo','Ciao','interno e 0','2000-02-02','2000-03-03','2000-04-04',0),(36,20,1,'Corso nuovo con docenti','3',0,1,33,1,'Note interessanti','Londra','Corvallis','2020-02-15','2020-03-20','2020-02-09',0),(37,22,3,'Proviamo gestione','1',0,1,13,2,'Proviamo gestione alunni e docenti','Roma','Corvallis','2020-03-20','2020-04-20','2020-03-03',1),(38,4,3,'corso per conoscenze basilari dell\'argomento','2',1,0,12,1,'nessuna nota','Londra','Corvallis','2000-04-04','2000-03-03','2010-02-02',0),(39,2,2,'corso intensivo','5',1,0,3,1,'nessuna nota','Napoli','Corvallis','2000-05-05','2000-05-05','2000-05-05',0),(40,5,2,'conoscenze basi','2',0,1,2,2,'nessuna nota','Vicenza','Corvallis','2000-02-02','2000-02-02','2000-02-02',0),(41,5,2,'corso per conoscenze basilari dell\'argomento','2',1,0,1,1,'nessuna nota','Verona','Corvallis','2000-02-02','2000-01-01','2000-10-10',0),(42,2,3,'corso per conoscenze basilari dell\'argomento','4',1,0,3,1,'nessuna nota','Napoli','Corvallis','2000-10-10','2000-10-10','2000-10-10',0),(43,4,1,'corso per conoscenze basilari dell\'argomento','6',0,1,3,1,'nessuna nota','Verona','Corvallis','1200-10-10','1010-10-10','1010-10-10',0),(44,4,1,'nuovo corso','3',0,1,3,1,'nessuna nota ancora','sss','Corvallis','2020-05-06','2020-05-06','2020-05-03',0),(45,2,3,'nuovo corso','5',1,0,45,2,'nessuna nota ora','Padova','Corvallis','2020-05-19','2020-05-27','2020-05-11',0);
/*!40000 ALTER TABLE `corsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corsi_docenti`
--

DROP TABLE IF EXISTS `corsi_docenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `corsi_docenti` (
  `corsi_docenti_id` int(11) NOT NULL AUTO_INCREMENT,
  `corso` int(11) NOT NULL,
  `docente` int(11) DEFAULT NULL,
  `interno` int(11) DEFAULT NULL,
  PRIMARY KEY (`corsi_docenti_id`),
  KEY `corso` (`corso`),
  CONSTRAINT `corsi_docenti_ibfk_1` FOREIGN KEY (`corso`) REFERENCES `corsi` (`corso_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corsi_docenti`
--

LOCK TABLES `corsi_docenti` WRITE;
/*!40000 ALTER TABLE `corsi_docenti` DISABLE KEYS */;
INSERT INTO `corsi_docenti` VALUES (2,14,6,0),(4,2,1,0),(6,15,5,1),(9,5,1,0),(12,17,1,1),(13,18,3,0),(14,3,3,1),(15,1,32,1),(16,9,3,0),(20,25,4,0),(21,28,15,1),(22,28,18,1),(23,29,19,1),(24,30,20,1),(25,31,22,1),(26,32,23,1),(27,33,25,1),(28,34,26,1),(29,35,28,1),(30,36,27,1),(31,37,29,1),(32,38,30,1),(33,39,31,1),(34,40,32,1),(35,41,33,1),(36,42,21,1);
/*!40000 ALTER TABLE `corsi_docenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corsi_utenti`
--

DROP TABLE IF EXISTS `corsi_utenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `corsi_utenti` (
  `corsi_utenti_id` int(11) NOT NULL AUTO_INCREMENT,
  `corso` int(11) NOT NULL,
  `dipendente` int(11) NOT NULL,
  `durata` int(11) NOT NULL,
  PRIMARY KEY (`corsi_utenti_id`),
  KEY `corso` (`corso`),
  KEY `dipendente` (`dipendente`),
  CONSTRAINT `corsi_utenti_ibfk_1` FOREIGN KEY (`corso`) REFERENCES `corsi` (`corso_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `corsi_utenti_ibfk_2` FOREIGN KEY (`dipendente`) REFERENCES `dipendenti` (`dipendente_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corsi_utenti`
--

LOCK TABLES `corsi_utenti` WRITE;
/*!40000 ALTER TABLE `corsi_utenti` DISABLE KEYS */;
INSERT INTO `corsi_utenti` VALUES (2,5,2,20),(4,3,10,35),(12,1,1,200),(21,1,7,5),(23,3,1,4),(24,3,2,5),(28,1,9,34),(30,17,1,62),(31,18,1,4),(32,3,9,3),(33,5,4,5),(57,15,1,1),(61,1,4,2),(62,17,2,3),(63,25,1,76),(64,26,4,6),(67,1,5,200),(68,1,2,555),(69,28,6,88),(70,1,20,5),(71,3,21,4),(72,3,22,5),(73,1,23,34),(74,17,24,62),(75,18,25,4),(76,3,26,3),(77,5,27,5),(78,15,28,1),(79,1,29,2),(80,17,30,3),(81,17,31,3),(82,17,32,3),(83,25,33,76),(84,25,17,76),(85,26,16,6),(86,26,15,6),(87,28,14,88),(88,28,13,88);
/*!40000 ALTER TABLE `corsi_utenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dipendenti`
--

DROP TABLE IF EXISTS `dipendenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dipendenti` (
  `dipendente_id` int(11) NOT NULL AUTO_INCREMENT,
  `matricola` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `sesso` varchar(100) NOT NULL,
  `luogo_nascita` varchar(100) NOT NULL,
  `stato_civile` varchar(100) NOT NULL,
  `titolo_studio` varchar(100) NOT NULL,
  `conseguito_presso` varchar(100) NOT NULL,
  `anno_conseguimento` int(11) NOT NULL,
  `tipo_dipendente` varchar(100) NOT NULL,
  `qualifica` varchar(100) NOT NULL,
  `livello` varchar(100) NOT NULL,
  `data_nascita` date NOT NULL,
  `data_assunzione` date NOT NULL,
  `data_fine_rapporto` date NOT NULL,
  `data_scadenza_contratto` date NOT NULL,
  `societa` int(11) NOT NULL,
  PRIMARY KEY (`dipendente_id`),
  KEY `societa` (`societa`),
  CONSTRAINT `dipendenti_ibfk_1` FOREIGN KEY (`societa`) REFERENCES `societa` (`societa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dipendenti`
--

LOCK TABLES `dipendenti` WRITE;
/*!40000 ALTER TABLE `dipendenti` DISABLE KEYS */;
INSERT INTO `dipendenti` VALUES (1,1337,'Mario','Azzurri','M','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(2,1338,'Luigi','Rossi','M','Padova','Sposato','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(3,1339,'Anna','Blu','F','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(4,1340,'Maria','Rossi','F','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(5,1341,'Marta','Gialli','F','Padova','Sposata','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(6,1342,'Leonardo','Rossi','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(7,1343,'Paolo','Verdi','M','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(8,1344,'Nicola','Rossi','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(9,1345,'Tommaso','Bianchi','M','Padova','Celibe','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(10,1346,'Roberto','Rossi','M','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(11,1346,'Roberto','Verdi','M','Padova','Sposato','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(12,1346,'Giacomo','Blu','M','Padova','Sposato','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(13,1346,'Gianfranco','Rossi','M','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(14,1346,'Giulia','Blu','F','Padova','Sposata','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(15,1346,'Carla','Azzurri','F','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(16,1346,'Emanuela','Rossi','F','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(17,1346,'Edoardo','Bianchi','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(18,1337,'Mario','Rossi','M','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(19,1338,'Luigi','Verdi','M','Padova','Sposato','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(20,1339,'Anna','Bianchi','F','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(21,1340,'Maria','Blu','F','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(22,1341,'Marta','Blu','F','Padova','Sposata','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(23,1342,'Leonardo','Blu','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(24,1343,'Paolo','Azzurri','M','Padova','Celibe','Terza Media','Scuola Media di Napoli',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(25,1344,'Nicola','Blu','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(26,1345,'Tommaso','Blu','M','Padova','Celibe','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(27,1346,'Roberto','Blu','M','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',1999,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',2),(28,1346,'Giacomo','Blu','M','Padova','Sposato','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(29,1346,'Giacomo','Azzurri','M','Padova','Sposato','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(30,1346,'Giulia','Azzurri','F','Padova','Sposata','Terza Media','Scuola Media di Napoli',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(31,1346,'Carla','Verdi','F','Padova','Celibe','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',3),(32,1346,'Edoardo','Azzurri','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1),(33,1346,'Edoardo','Blu','M','Padova','Sposato','Diploma scuola secondaria secondo grado','ITS Girardi',2008,'Developer','Programmatore Qualificato','Livello 8','1988-06-05','1998-01-05','3000-01-01','2020-04-05',1);
/*!40000 ALTER TABLE `dipendenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docenti`
--

DROP TABLE IF EXISTS `docenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `docenti` (
  `docente_id` int(11) NOT NULL AUTO_INCREMENT,
  `titolo` varchar(100) NOT NULL,
  `ragione_sociale` varchar(100) NOT NULL,
  `indirizzo` varchar(100) NOT NULL,
  `localita` varchar(100) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `nazione` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `partita_iva` varchar(100) NOT NULL,
  `referente` varchar(100) NOT NULL,
  PRIMARY KEY (`docente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docenti`
--

LOCK TABLES `docenti` WRITE;
/*!40000 ALTER TABLE `docenti` DISABLE KEYS */;
INSERT INTO `docenti` VALUES (1,'tutti','Beppe Verdi','Via da qua 99','1000','PD','IT','0495796302','0495796302','92894561238','Giovanni Muciaccia'),(3,'tutti','Beppe Vessicchio','Via da qua 99','1000','PD','IT','0147852369','0147852369','92894561238','Carlo Conti'),(4,'tutti','Giuseppe Verdi','Via da qua 99','1000','PD','IT','0366998521','0366998521','74185296352','Giovanni Verdi'),(5,'tutti','Maria Bianchi','Via da qua 99','1000','PD','IT','0495796302','0495796302','92894561238','Giacomo Bianchi'),(6,'tutti','Carlo Bianchi','Via da qua 99','1000','PD','IT','0369852147','0369852147','92894561238','Valeria Rossi'),(7,'tutti','Luigi Bianchi','Via da qua 99','1000','PD','IT','0796359874','0796359874','74185296352','Melania Verdi'),(8,'tutti','Maddalena Bianchi','Via da qua 99','1000','PD','IT','0493698521','0493698521','74185296352','Leopoldo Casati'),(9,'tutti','Ismaele Bianchi','Via da qua 99','1000','PD','IT','0495796302','0495796302','74185296352','Pierpaolo Finiti'),(10,'tutti','Paolo Blu','Via da qua 99','1000','PD','IT','0493698521','0493698521','74185296352','Giovanni Muciaccia'),(13,'tutti','Renato Azzurro','Via da qua 99','1000','PD','IT','0495736985','0495736985','74185296352','Giovanni Muciaccia'),(14,'Nessuno','Piero Pierino','via verdi, 90','35002','FR-35','FR','0495790099','0394745286','92894561238','Giacomo Leopardi'),(15,'tutti','Giacomo Blu','Via da qua 99','1000','PD','IT','0492569802','0492569802','74185296352','Giovanni Muciaccia'),(16,'qualsiasi','Stella Bianchi','via non si sa, 80','37003','VR','IT','0236985475','5562598565','92894561238','Martino Veri'),(17,'qualsiasi','Giuliano Marittimo','Via roma, 77','1000','PD','IT','0495796302','0495796302','74185296352','Giovanni Muciaccia'),(18,'tutti','Leonardo Milani','Via boh, 55','1000','PD','IT','0366998521','0366998521','92894561238','Giovanni Verdi'),(19,'tutti','Maria Vessicchio','Via salmone, 66','1000','PD','IT','0495796302','0495796302','74185296352','Giacomo Bianchi'),(20,'tutti','Carlo Marroni','Via da li, 44','1000','PD','IT','0369852147','0369852147','74185296352','Valeria Rossi'),(21,'tutti','Luigi Blu','Via da non si sa, 99','1000','PD','IT','0796359874','0796359874','92894561238','Melania Verdi'),(22,'tutti','Maddalena Rossi','Via da qua 99','1000','PD','IT','0493698521','0493698521','74185296352','Leopoldo Casati'),(23,'tutti','Ismaele Rossi','Via da qua 99','1000','PD','IT','0495796302','0495796302','92894561238','Pierpaolo Finiti'),(24,'tutti','Paolo Azzurri','Via da qua 99','1000','PD','IT','0493698521','0493698521','74185296352','Giovanni Muciaccia'),(25,'Nessuno','Piero Pieru','via verdi, 90','35002','FR-35','FR','0495790099','0394745286','74185296352','Giacomo Leopardi'),(26,'qualsiasi','Stella Gialli','via non si sa, 80','37003','VR','IT','0236985475','5562598565','92894561238','Martino Veri'),(27,'qualsiasi','Stella Cadente','via non si sa, 80','37003','VR','IT','0236985475','5562598565','74185296352','Martino Veri'),(28,'qualsiasi','Luna Verdi','via non si sa, 80','37003','VR','IT','0236985475','5562598565','92894561238','Martino Veri');
/*!40000 ALTER TABLE `docenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misure`
--

DROP TABLE IF EXISTS `misure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `misure` (
  `misura_id` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(100) NOT NULL,
  PRIMARY KEY (`misura_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misure`
--

LOCK TABLES `misure` WRITE;
/*!40000 ALTER TABLE `misure` DISABLE KEYS */;
INSERT INTO `misure` VALUES (1,'Ore'),(2,'Giorni');
/*!40000 ALTER TABLE `misure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nazioni`
--

DROP TABLE IF EXISTS `nazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `nazioni` (
  `iso` varchar(3) NOT NULL,
  `description` varchar(20) NOT NULL,
  PRIMARY KEY (`iso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nazioni`
--

LOCK TABLES `nazioni` WRITE;
/*!40000 ALTER TABLE `nazioni` DISABLE KEYS */;
INSERT INTO `nazioni` VALUES ('FR','Francia'),('IT','Italia'),('US','USA');
/*!40000 ALTER TABLE `nazioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `province` (
  `id_prov` varchar(10) NOT NULL,
  `description` varchar(20) NOT NULL,
  `id_region` int(7) NOT NULL,
  PRIMARY KEY (`id_prov`),
  KEY `id_region` (`id_region`),
  CONSTRAINT `province_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `regioni` (`id_region`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
INSERT INTO `province` VALUES ('48-001','Anderson',1),('FR-35','Ille-et-Vilain',4),('NA','Napoli',5),('PD','Padova',6),('VI','Vicenza',6),('VR','Verona',6);
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regioni`
--

DROP TABLE IF EXISTS `regioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `regioni` (
  `id_region` int(7) NOT NULL AUTO_INCREMENT,
  `description` varchar(20) NOT NULL,
  `iso_coun` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_region`),
  KEY `iso_coun` (`iso_coun`),
  CONSTRAINT `regioni_ibfk_1` FOREIGN KEY (`iso_coun`) REFERENCES `nazioni` (`iso`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regioni`
--

LOCK TABLES `regioni` WRITE;
/*!40000 ALTER TABLE `regioni` DISABLE KEYS */;
INSERT INTO `regioni` VALUES (1,'Texas','US'),(4,'Bretagna','FR'),(5,'Campania','IT'),(6,'Veneto','IT');
/*!40000 ALTER TABLE `regioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societa`
--

DROP TABLE IF EXISTS `societa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `societa` (
  `societa_id` int(11) NOT NULL AUTO_INCREMENT,
  `ragione_sociale` varchar(100) DEFAULT NULL,
  `indirizzo` varchar(100) NOT NULL,
  `localita` varchar(100) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `nazione` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `partita_iva` varchar(100) NOT NULL,
  PRIMARY KEY (`societa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societa`
--

LOCK TABLES `societa` WRITE;
/*!40000 ALTER TABLE `societa` DISABLE KEYS */;
INSERT INTO `societa` VALUES (1,'Corvallis','Via Roma 1337','Padova','PD','Italia','0000000','0000000','IVA_ZANICCHI'),(2,'Corvallis','Via Calzoni, 1/3','Bologna','BO','Italia','0000000','0000000','IVA_ZANICCHI'),(3,'Corvallis','Via Di Novoli, 37','Firenze','FI','Italia','0000000','0000000','IVA_ZANICCHI');
/*!40000 ALTER TABLE `societa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipi`
--

DROP TABLE IF EXISTS `tipi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipi` (
  `tipo_id` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(100) NOT NULL,
  PRIMARY KEY (`tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipi`
--

LOCK TABLES `tipi` WRITE;
/*!40000 ALTER TABLE `tipi` DISABLE KEYS */;
INSERT INTO `tipi` VALUES (1,'Java'),(2,'Internet'),(3,'Client-Server'),(4,'Programmazione Mainframe'),(5,'RPG'),(6,'Cobol'),(7,'Programmazione di Sistemi EDP'),(8,'Object-Oriented'),(9,'Sistema di Gestione Qualità'),(10,'Privacy'),(11,'Analisi e Programmazione'),(12,'Programmazione PC'),(13,'Project Management'),(14,'Programmazione'),(15,'CICS'),(16,'DB2'),(17,'Linguaggio C'),(18,'Data Base'),(19,'Introduzione all\'Informatica'),(20,'Office'),(21,'JCL'),(22,'Marketing'),(23,'Oracle'),(24,'Formazione Sicurezza (626/94; 81/2008)'),(25,'Inglese'),(26,'Amministrazione/Finanza'),(27,'Windows'),(28,'Word'),(29,'Office'),(30,'Excel'),(31,'Easytrieve'),(32,'Comunicazione'),(33,'Access'),(34,'Formazione per Formatori'),(35,'Tecnica Bancaria'),(36,'Lotus Notes'),(37,'Visualage'),(38,'Websphere'),(39,'Reti/Elettronica'),(40,'SAS'),(41,'UML'),(42,'ViaSoft'),(43,'Visual Basic'),(44,'Sistemi Informativi'),(45,'Cartografia Vettoriale'),(46,'Cartografia Raster'),(47,'Gestione Elettronica Documenti-Modulistica'),(48,'Territorio e Ambiente'),(49,'Sistemi Informativi Territoriali'),(50,'Editoria'),(51,'VARIE'),(52,'AS/400'),(53,'Gestione Progetti'),(55,'Organizzazione'),(56,'Funzionale Normativo'),(57,'Funzionale'),(58,'Manageriale'),(60,'Metodologia'),(61,'Prodotti'),(62,'Business Object'),(63,'Sistemi di Business Intelligence'),(64,'Formazione Apprendisti'),(66,'Primavera'),(67,'XML'),(68,'Cattolica – Servizi in ambito Sinistri'),(69,'Ambito Assicurativo'),(70,'Gestione della Sicurezza delle Informazioni (SGSI)'),(71,'PHP'),(72,'CMS/E-commerce'),(73,'Microsoft Azure'),(74,'Middleware - Sistemi Distribuiti'),(75,'Eurotech IoT'),(76,'Blockchain'),(77,'Angular'),(78,'Soft Skill'),(79,'Infrastrutture'),(80,'Infrastrutture'),(81,'Android'),(82,'SQL'),(83,'Risorse Umane');
/*!40000 ALTER TABLE `tipi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipologie`
--

DROP TABLE IF EXISTS `tipologie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipologie` (
  `tipologia_id` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(100) NOT NULL,
  PRIMARY KEY (`tipologia_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipologie`
--

LOCK TABLES `tipologie` WRITE;
/*!40000 ALTER TABLE `tipologie` DISABLE KEYS */;
INSERT INTO `tipologie` VALUES (1,'Ingresso'),(2,'Aggiornamento'),(3,'Seminario');
/*!40000 ALTER TABLE `tipologie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valutazioni_corsi`
--

DROP TABLE IF EXISTS `valutazioni_corsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `valutazioni_corsi` (
  `valutazione_corsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `corso` int(11) NOT NULL,
  `criterio` varchar(100) NOT NULL,
  `valore` int(11) NOT NULL,
  PRIMARY KEY (`valutazione_corsi_id`),
  KEY `corso` (`corso`),
  CONSTRAINT `valutazioni_corsi_ibfk_1` FOREIGN KEY (`corso`) REFERENCES `corsi` (`corso_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valutazioni_corsi`
--

LOCK TABLES `valutazioni_corsi` WRITE;
/*!40000 ALTER TABLE `valutazioni_corsi` DISABLE KEYS */;
INSERT INTO `valutazioni_corsi` VALUES (1,1,'Utilità del corso per la crescita personale',3),(2,1,'Utilità del corso nell\'ambito lavorativo attuale',4),(3,1,'Scelta degli argomenti specifici',1),(4,1,'Livello di approfondimento degli argomenti specifici',2),(5,1,'Durata del corso',4),(6,1,'Corretta suddivisione di teoria e pratica',3),(7,1,'Efficacia delle esercitazioni',3),(8,1,'Qualità del materiale didattico',2),(9,2,'Utilità del corso per la crescita personale',2),(10,2,'Utilità del corso nell\'ambito lavorativo attuale',1),(11,2,'Scelta degli argomenti specifici',1),(12,2,'Livello di approfondimento degli argomenti specifici',3),(13,2,'Durata del corso',2),(14,2,'Corretta suddivisione di teoria e pratica',4),(15,2,'Efficacia delle esercitazioni',4),(16,2,'Qualità del materiale didattico',4);
/*!40000 ALTER TABLE `valutazioni_corsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valutazioni_docenti`
--

DROP TABLE IF EXISTS `valutazioni_docenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `valutazioni_docenti` (
  `valutazioni_docenti_id` int(11) NOT NULL AUTO_INCREMENT,
  `valore` varchar(100) NOT NULL,
  `criterio` varchar(100) NOT NULL,
  `docente` int(11) NOT NULL,
  `corso` int(11) NOT NULL,
  PRIMARY KEY (`valutazioni_docenti_id`),
  KEY `corso` (`corso`),
  CONSTRAINT `valutazioni_docenti_ibfk_2` FOREIGN KEY (`corso`) REFERENCES `corsi` (`corso_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valutazioni_docenti`
--

LOCK TABLES `valutazioni_docenti` WRITE;
/*!40000 ALTER TABLE `valutazioni_docenti` DISABLE KEYS */;
INSERT INTO `valutazioni_docenti` VALUES (2,'3','Disponibilità',1,2),(3,'3','Completezza',1,2),(5,'1','Chiarezza nell\'esposizione',1,2),(7,'3','Disponibilità',3,3),(8,'3','Completezza',3,3),(9,'1','Chiarezza nell\'esposizione',3,3);
/*!40000 ALTER TABLE `valutazioni_docenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valutazioni_utenti`
--

DROP TABLE IF EXISTS `valutazioni_utenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `valutazioni_utenti` (
  `valutazioni_utenti_id` int(11) NOT NULL AUTO_INCREMENT,
  `corso` int(11) NOT NULL,
  `dipendente` int(11) NOT NULL,
  `criterio` varchar(100) DEFAULT NULL,
  `valore` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`valutazioni_utenti_id`),
  KEY `corso` (`corso`),
  KEY `dipendente` (`dipendente`),
  CONSTRAINT `valutazioni_utenti_ibfk_1` FOREIGN KEY (`corso`) REFERENCES `corsi` (`corso_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `valutazioni_utenti_ibfk_2` FOREIGN KEY (`dipendente`) REFERENCES `dipendenti` (`dipendente_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valutazioni_utenti`
--

LOCK TABLES `valutazioni_utenti` WRITE;
/*!40000 ALTER TABLE `valutazioni_utenti` DISABLE KEYS */;
INSERT INTO `valutazioni_utenti` VALUES (1,5,2,'Impegno','5'),(2,5,2,'Logica','1'),(3,5,2,'Apprendimento','3'),(4,5,2,'Velocità','6'),(5,5,2,'Ordine e precisione','4'),(6,3,10,'Impegno','4'),(7,3,10,'Logica','5'),(8,3,10,'Apprendimento','2'),(9,3,10,'Velocità','4'),(10,3,10,'Ordine e precisione','1'),(11,1,1,'Impegno','4'),(12,1,1,'Logica','4'),(13,1,1,'Apprendimento','4'),(14,1,1,'Velocità','4'),(15,1,1,'Ordine e Precisione','4'),(16,15,1,'Apprendimento','5'),(17,15,1,'Logica','4'),(18,15,1,'Impegno','5'),(19,15,1,'Ordine e Precisione','5'),(20,15,1,'Velocità','3'),(21,3,1,'Impegno','3'),(22,3,1,'Logica','3'),(23,3,1,'Apprendimento','3'),(24,3,1,'Velocità','3'),(25,3,1,'Ordine e Precisione','3'),(26,17,1,'Impegno','1'),(27,17,1,'Logica','6'),(28,17,1,'Apprendimento','6'),(29,17,1,'Velocità','1'),(30,17,1,'Ordine e Precisione','6'),(31,1,4,'Impegno','6'),(32,1,4,'Logica','1'),(33,1,4,'Apprendimento','1'),(34,1,4,'Velocità','1'),(35,1,4,'Ordine e Precisione','6'),(36,28,6,'Logica','4'),(37,28,6,'Impegno','3'),(38,28,6,'Apprendimento','5'),(39,28,6,'Velocità','1'),(40,28,6,'Ordine e Precisione','4');
/*!40000 ALTER TABLE `valutazioni_utenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'projectwork'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-17 19:43:48
